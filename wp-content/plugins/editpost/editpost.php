<?php

/*
Plugin Name: EDITPOST
Description: Edit a post inserting CUSTOM DATA before it saves
Version: 1.0
Author: Elvis Morales
Author URI: http://www.linkedin.com/in/elvismdev
 */

define('EDITPOST_VERSION', '0.1');
define('EDITPOST_PLUGIN_URL', plugin_dir_url( __FILE__ ));

add_filter('wp_insert_post_data', 'mycustom_data_filter', 10, 2);

function mycustom_data_filter($filtered_data, $raw_data){

    if (strstr($filtered_data['post_content'], 'Hope you try this look and feel on your hair today'))
        return $filtered_data;

    $category = get_category( $raw_data['post_category'][1] );
    $randompost = get_posts ( array( 'posts_per_page'   => 1, 'orderby' => 'rand' ));

    $content = '<br>You are viewing '.$filtered_data['post_title'].'! These are great hairstyle designs in our <a href="'.get_category_link($category->term_id).'">'.$category->name.'</a> section.
                Hope you try this look and feel on your hair today. Ever considered changing it up, you might like this <a href="'.$randompost[0]->guid.'">'.$randompost[0]->post_title.'</a>';

    $filtered_data['post_content'] .= $content;

    return $filtered_data;
}

?>
