<div class="wrap">

<div id="icon-edit" class="icon32"><br></div>

<h2>Image Grabber</h2>



<?php if ($__message): ?>

<div class="updated fade below-h2" id="message" style="background-color: rgb(255, 251, 204);"><p><?php echo $__message; ?></p></div><br />

<?php endif; ?>



<br class="clear" />



<div id="poststuff">

<?php if ($data): ?>

<form action="" method="post">

<div id="post-body" class="columns-2">

<div id="post-body-content">

<!--div class="edit_c ">

	<div id="titlediv">

		<div id="titlewrap"><input id="title" placeholder="Enter Post Title" type="text" name="post_title" value="" /></div>

	</div>

	<div id="postdivrich" class="postarea">

		<?php #wp_editor('', 'post', array('media_buttons' => false, 'textarea_rows'=>6)); ?>

	</div>

</div-->

<ul class="listData">

<?php foreach($data as $row): ?>

<?php

$parts = parse_url($row->image_url);

$file_name = basename($parts['path']);

?>

	<li>

		<div class="img_t"><img src="<?php echo $row->thumb_url;?>" /></div>

		<div class="edit_t">
        <h1> Post Settings:</h1>

			<div class="row">

				<label for="title_<?php echo $row->id;?>">Post Title</label>

				<input id="title_<?php echo $row->id;?>" placeholder="Enter Post Title" type="text" name="data[<?php echo $row->id;?>][post_title]" value="<?php echo @trim($row->title, ' ... ');?>" />

			</div>
           <div class="row">

				<label for="title_<?php echo $row->id;?>">Post Tags</label>

				<input id="title_<?php echo $row->id;?>" placeholder="Enter comma seperated tags" type="text" name="data[<?php echo $row->id;?>][post_tags]" value="<?php echo @trim($row->tags, ' ... ');?>" />

			</div>
            <input id="seo-meta-title" placeholder="Enter comma seperated tags" type="hidden" name="_seo_meta[seo_meta_author]" value="<?php echo @trim($row->tags, ' ... ');?>" />
            <div class="row">

				<label for="title_<?php echo $row->id;?>">Meta keywords</label>

				<input id="title_<?php echo $row->id;?>" placeholder="Enter comma seperated keywords" type="text" name="data[<?php echo $row->id;?>][seo_meta_keyword]" value="<?php echo @trim($row->keywords, ' ... ');?>" />

			</div>
             <div class="row">

				<label for="title_<?php echo $row->id;?>">Meta Description</label>

				<textarea name="data[<?php echo $row->id;?>][seo_meta_description]" id="desc_<?php echo $row->id;?>" rows="4"><?php echo $row->description;?></textarea>
			</div>

		<h2>Image settings</h2>

<div class="row">
				<label for="filename_<?php echo $row->id;?>">File Name</label>
				<input type="text" id="filename_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][filename]" value="<?php echo $file_name;?>" />
			</div>

			<div class="row">

				<label for="title_<?php echo $row->id;?>">Image Title</label>

				<input type="text" id="title_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][img_title]" value="<?php echo @trim($row->title, ' ... ');?>" />

			</div>

			 <div class="row">

				<label for="alt_<?php echo $row->id;?>">Alternative text</label>

				<input type="text" id="alt_<?php echo $row->id;?>" name="data[<?php echo $row->id;?>][img_alt]" value="" />

			</div>

			<div class="row">

				<label for="caption_<?php echo $row->id;?>">Caption</label>

				<textarea name="data[<?php echo $row->id;?>][image_caption]" id="caption_<?php echo $row->id;?>" rows="2"></textarea>

			</div>

			<div class="row">

				<label for="desc_<?php echo $row->id;?>">Description</label>

				<textarea name="data[<?php echo $row->id;?>][image_description]" id="desc_<?php echo $row->id;?>" rows="4"><?php echo $row->description;?></textarea>

			</div>

			<input type="hidden" name="data[<?php echo $row->id;?>][id]" value="<?php echo $row->id;?>" />

			<input type="hidden" name="data[<?php echo $row->id;?>][image_url]" value="<?php echo $row->image_url;?>" />

		</div>

	</li>

<?php endforeach;?>


</ul>


</div>



<div id="postbox-container-1" class="postbox-container">

	<div class="meta-box-sortables ui-sortable">

		<div class="postbox" id="submitdiv">

<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle"><span>Bulk Posts</span></h3>

<div class="inside">

<div id="submitpost" class="submitbox">



<div id="misc-publishing-actions">



<div class="misc-pub-section"><label for="post_status">Interval:</label>

<span id="post-status-display"><input type="text" name="interval" value="<?php echo $interval;?>" size="6" /> minutes</span>



</div>



<div class="misc-pub-section curtime">

	<span id="timestamp">

	Publish Date:</span>

	<div class="hide-if-js" id="timestampdiv" style="display: block;"><div class="timestamp-wrap">



<select name="date[month]">

          <?php

            for ($i=1; $i<=12; $i++) {

              $selected = '';

              if ($i == date('m', strtotime($postDate))) $selected = ' selected="selected"';

              echo "+'<option value=\"{$i}\"{$selected}>".date('M', mktime(0, 0, 0,$i))."</option>'\n";

            }

          ?>

        </select>

        <input type="text" size="2" name="date[day]" value="<?php echo date('d', strtotime($postDate));?>" />

        ,<input type="text" size="2" name="date[year]" value="<?php echo date('Y', strtotime($postDate));?>" />

        @<input type="text" size="2" name="date[hour]" value="<?php echo date('H', strtotime($postDate));?>" />

        :<input type="text" size="2" name="date[minute]" value="<?php echo date('i', strtotime($postDate));?>" />



</div>

</div>

</div>

<div class="clear"></div>

</div>



<div id="major-publishing-actions">



<input type="submit" class="button button-highlighted" tabindex="4" value="Save Draft" id="save-post" name="save">



<div id="publishing-action">

<img alt="" id="ajax-loading" class="ajax-loading" src="<?php echo admin_url('images/wpspin_light.gif');?>" style="visibility: hidden;">

		<input type="hidden" value="Publish" id="original_publish" name="original_publish">

		<input type="submit" accesskey="p" tabindex="5" value="Schedule" class="button button-primary" id="publish" name="publish"></div>

<div class="clear"></div>

</div>

</div>



</div>

</div>



<div class="postbox" id="submitdiv">

<div title="Click to toggle" class="handlediv"><br></div><h3 class="hndle"><span>Category</span></h3>

<div class="inside">

<div id="categorypost" class="categorybox">

	<?php echo $this->_buildCategoryOptionList('category[]', unserialize(get_option('image-grab.defaultCategory')), 'category', get_option('image-grab.multipleCategory')); ?>

</div>

</div>

</div>



	</div>

</div>

</div>

<input type="hidden" name="act" value="save-post" />

</form>

<?php endif; ?>



</div>



</div>

<script type="text/javascript">

//<![CDATA[

jQuery(function($){



//$('.timestamp-wrap').html('');



$('.edit-timestamp').click(function() {

    if ($('.timestamp-wrap').html() == '') {

      $('.timestamp-wrap').html(

        '<select name="date[month]">'

          <?php

            for ($i=1; $i<=12; $i++) {

              $selected = '';

              if ($i == date('m')) $selected = ' selected="selected"';

              echo "+'<option value=\"{$i}\"{$selected}>".date('M', mktime(0, 0, 0,$i))."</option>'\n";

            }

          ?>

        +'</select>'

        +'<input type="text" size="2" name="date[day]" value="<?php echo date('d');?>" />'

        +',<input type="text" size="2" name="date[year]" value="<?php echo date('Y');?>" />'

        +'@<input type="text" size="2" name="date[hour]" value="<?php echo date('H');?>" />'

        +':<input type="text" size="2" name="date[minute]" value="<?php echo date('i');?>" />'

      );

    } else {

      $('.timestamp-wrap').html('');

    }

});



	$('form').submit(function() {

		$('.ajax-loading').css('visibility', 'visible');

		$('.button').attr('disabled', 'disabled');



		return false;

	});



	$('.button').click(function() {



		$.ajax({

			url: '<?php echo admin_url('admin.php?page=image-grab');?>',

			data: $('form').serialize() + '&' + $(this).attr('name') + '=' + $(this).attr('value'),

			type: 'POST',

			success: function(data) {

				location.href = '<?php echo admin_url('admin.php?page=image-grab');?>';

				//alert('sukses');

			}

		});



	});



});

//]]>

</script>

