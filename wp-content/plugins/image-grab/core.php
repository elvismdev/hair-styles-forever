<?php
 wp_enqueue_style('style_css',plugins_url('style.css', __FILE__ ), false, '1.0', 'all' );
 add_action('admin_init', 'seo_meta_box');
	function seo_meta_box()
	{

		foreach(array('post', 'page') as $type)
		{
			add_meta_box('SEO META BOX', 'SEO META BOX', 'seo_meta_setup', $type, 'normal', 'high');
		}
		add_action('save_post', 'save_seo_meta');
	}
	function seo_meta_setup()
	{
		global $post;

		$metakey = get_post_meta($post->ID, 'seo_meta_keyword', TRUE);
		$metadesc = get_post_meta($post->ID, 'seo_meta_description', TRUE);
		?>
		<table class="widefat">
			<tbody>
				<tr>
					<td><label for="seo-meta-keyword"><strong>Page Keywords</strong></label></td>
					<td><input id="seo-meta-keyword" class="widefat" type="text" name="seo_meta_keyword" value="<?php if(isset($metakey)){echo $metakey;} ?>" /></td>
				</tr>
				<tr>
					<td><label for="seo-meta-description"><strong>Page Description</strong></label></td>
					<td><textarea id="seo-meta-description" class="widefat" name="seo_meta_description"><?php if(isset($metadesc)){echo $metadesc;} ?></textarea></td>
				</tr>
			</tbody>
		</table>
		<?php
		echo '<input type="hidden" name="theme_seo_meta_nonce" value="'.wp_create_nonce(__FILE__).'" />';
	}

function save_seo_meta($post_id)
	{
		global $post;
		$post_id = $post->ID;
		if( !wp_verify_nonce($_POST['theme_seo_meta_nonce'], __FILE__) ) return $post_id;

		if( $_POST['post_type'] == 'page')
		{
			if( !current_user_can('edit_page', $post_id) ) return $post_id;
		}
		else
		{
			if( !current_user_can('edit_post', $post_id) ) return $post_id;
		}

        //$metakey = get_post_meta($post_id, 'seo_meta_keyword', TRUE);
		//$metadesc = get_post_meta($post_id, 'seo_meta_description', TRUE);
		if(isset($_POST['seo_meta_keyword']))
    	update_post_meta($post_id, 'seo_meta_keyword', $_POST['seo_meta_keyword']);
		if(isset($_POST['seo_meta_description']))
		update_post_meta($post_id, 'seo_meta_description', $_POST['seo_meta_description']);
		return $post_id;

	}

	if(!function_exists(clean_seo_meta)){
		function clean_seo_meta()
		{
			if(is_array($arr))
			{
				foreach($arr as $i => $v)
				{
					if(is_array($arr[$i]))
					{
						clean_seo_meta($arr[$i]);
						if(!count($arr[$i]))
						{
							unset($arr[$i]);
						}
					}else
					{
						if(trim($arr[$i] = ''))
						{
							unset($arr[$i]);
						}
					}
				}
				if(!count($arr))
				{
					$arr = NULL;
				}
			}
		}
	}
	


	function insert_meta_wp_header()
	{	
		global $post;
		
		$metakey = get_post_meta($post->ID, 'seo_meta_keyword', TRUE);
		$metadesc = get_post_meta($post->ID, 'seo_meta_description', TRUE);
		if(isset($metakey)){
			echo  '<meta name="keywords" content="'.$metakey.'" />'."\n";
		}

		if(isset($metadesc)){
			echo '<meta name="description" content="'.substr(trim($metadesc), 0,160).'" />'."\n";
		}

	}

	add_action('wp_head', 'insert_meta_wp_header', 1);
 class ImageGrabber{
	 public function __construct(){
		 register_activation_hook(__FILE__,array(&$this,'activate'));
		 add_action('admin_menu',array(&$this,'adminMenu'));add_filter("media_upload_tabs",array(&$this,"build_tab"));
		 add_action("media_upload_ImageGrabber",array(&$this,"menu_handle"));}
	function activate(){
		add_option('image-grab.defaultCategory',serialize(array(0)));
		add_option('image-grab.multipleCategory','0');
		add_option('image-grab.duplicate','0');
		add_option('image-grab.schedule_interval','60');
		add_option('image-grab.contentDesc',"New Wallpaper {TITLE} Dimesion: {DIMENSION} File Size: {FILE_SIZE}");}
  private function array_insert(&$array,$insert,$position){
	  settype($array,"array");
	  settype($insert,"array");
	  settype($position,"int");
	  if($position==0){
		  $array=array_merge($insert,$array);}
	 else{
		 if($position>=(count($array)-1)){$array=array_merge($array,$insert);}
		 else{$head=array_slice($array,0,$position);
		 $tail=array_slice($array,$position);
		 $array=array_merge($head,$insert,$tail);}
		 }
	return $array;}
 public function build_tab($tabs)
    {$newtab=array('ImageGrabber'=>__('Grab Images','ImageGrabber'));
	return $this->array_insert($tabs,$newtab,2);
	}
 public function menu_handle()
 {
	 return wp_iframe(array(&$this,"media_process"));
 }
 private function fetch_image($url){
	 if(function_exists("curl_init")){
		 return $this->curl_fetch_image($url);
		}
	elseif(ini_get("allow_url_fopen")){
		return $this->fopen_fetch_image($url);
		}
	}
private function curl_fetch_image($url)
	{
		$ch=curl_init();
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$image=curl_exec($ch);
		curl_close($ch);
		return $image;
	}
private function fopen_fetch_image($url)
{
	$image=file_get_contents($url,false,$context);
	return $image;
}
public function media_process()
{
	$apiKey=get_option('image-grab.azureAPIKey');
	if(empty($apiKey)){echo '<div class="updated below-h2" id="message"><p>You need to configuration. <a href="'.admin_url('admin.php?page=ig-options').'" target="_top">Configure</a></p></div>';return;}
	if(isset($_POST['submit'])&& $_POST['act']=='save')
	{
		$posts=$_POST['data'];
		//exit; // Esto es tremenda MARICONA en CUBANO
		foreach($posts as $post)
		{
			$filename=$post['filename'];
			$title=$post['title'];
			$alt=$post['alt'];
			$caption=$post['caption'];
			$image_url=$post['image_url'];
			$uploads=wp_upload_dir();
			$post_id=isset($_GET['post_id'])?(int) $_GET['post_id']:0;
			$filename=wp_unique_filename($uploads['path'],$filename,$unique_filename_callback=null);
			$wp_filetype=wp_check_filetype($filename,null);
			$fullpathfilename=$uploads['path']."/".$filename;
			try{if(!substr_count($wp_filetype['type'],"image"))
			{
				throw new Exception($filename.' is not a valid image. '.$wp_filetype['type'].'');
			}
			$image_string=$this->fetch_image($image_url);
			$fileSaved=file_put_contents($uploads['path']."/".$filename,$image_string);
			if(!$fileSaved){
				throw new Exception("The file cannot be saved.");
				}
			$attachment=array('post_mime_type'=>$wp_filetype['type'],'post_title'=>$title,'post_content'=>'','post_excerpt'=>$caption,'post_status'=>'inherit','guid'=>$uploads['url']."/".$filename);
			$attach_id=wp_insert_attachment($attachment,$fullpathfilename,$post_id);
			if(!$attach_id){throw new Exception("Failed to save record into database.");}
			$attach_ids[]=$attach_id;
			require_once(ABSPATH."wp-admin".'/includes/image.php');
			$attach_data=wp_generate_attachment_metadata($attach_id,$fullpathfilename);
			wp_update_attachment_metadata($attach_id,$attach_data);
			update_post_meta($attach_id,'_wp_attachment_image_alt',$alt)
			;}
			catch(Exception $e){$error='<div id="message" class="error"><p>'.$e->getMessage().'</p></div>';}
			}
			}else if(isset($_POST['submit'])&&$_POST['act']=='step2'){
				$checked=$_POST['checked'];
				$post=$_POST;
				$data=array();
				foreach($checked as $id)
				{
					$title=$post['title'][$id];
					$image_url=$post['image_url'][$id];
					$thumb_url=$post['thumb_url'][$id];
					$data[]=(object) array('id'=>$id,'title'=>$title,'image_url'=>$image_url,'thumb_url'=>$thumb_url);
				}
				include_once('display_edit.media.view.php');return;
			}
			if(isset($_REQUEST['q']))
			{
				$q=$_REQUEST['q'];
				$filter=$_REQUEST['filter'];
				$filterSize="Size:".$filter;
				$skip=$_REQUEST['skip'];
				$results=$this->bingSearch($q,$filterSize,20,$skip);$next=$skip+20;if($skip>0)$prev=$skip-20;
			}
			include_once('display.media.view.php');
			if($attach_ids){$this->media_upload_type_form("image",$errors,$attach_ids);
			}
			}
private function media_upload_type_form($type='file',$errors=null,$ids=null)
{
	$post_id=isset($_REQUEST['post_id'])?intval($_REQUEST['post_id']):0;
	$form_action_url=admin_url("media-upload.php?type=$type&tab=type&post_id=$post_id");
	$form_action_url=apply_filters('media_upload_form_url',$form_action_url,$type);
	?>
<form enctype="multipart/form-data" method="post" action="<?php echo esc_attr($form_action_url);?>" class="media-upload-form type-form validate" id="<?php echo $type;?>-form">
  <input type="submit" class="hidden" name="save" value="" />
  <input type="hidden" name="post_id" id="post_id" value="<?php echo (int) $post_id;?>" />
  <?php wp_nonce_field('media-form');?>
  <script type="text/javascript">
		//<![CDATA[
		jQuery(function($){
			var preloaded = $(".media-item.preloaded");
			if ( preloaded.length > 0 ) {
				preloaded.each(function(){prepareMediaItem({id:this.id.replace(/[^0-9]/g, '')},'');});
			}
			updateMediaForm();
		});
		//]]>
		</script>
  <div id="media-items">
    <?php
 if($ids){foreach($ids as $idx=>$id){if(!is_wp_error($id)){add_filter('attachment_fields_to_edit','media_post_single_attachment_fields_to_edit',10,2);echo get_media_items($id,$errors);}else{echo '<div id="media-upload-error">'.esc_html($id->get_error_message()).'</div>';exit;}}}?>
  </div>
  <p class="savebutton ml-submit">
    <input type="submit" class="button" name="save" value="<?php esc_attr_e('Save all changes');?>" />
  </p>
</form>
<?php
}
public function options()
{
	$data['azure_api_key']=get_option('image-grab.azureAPIKey');
	$data['default_category']=unserialize(get_option('image-grab.defaultCategory'));
	$data['multiple_category']=get_option('image-grab.multipleCategory');
	$data['duplicate']=get_option('image-grab.duplicate');
	$data['content_desc']=stripslashes(get_option('image-grab.contentDesc'));
	$data['content_pos']=get_option('image-grab.contentPos');
	$data['interval']=get_option('image-grab.schedule_interval');
	$data['post_title']=get_option('image-grab.post_title');
	$data['image_title']=get_option('image-grab.image_title');
	$data['image_alt']=get_option('image-grab.image_alt');
	$data['image_caption']=get_option('image-grab.image_caption');
	$data['image_name']=get_option('image-grab.image_name');
	if(isset($_POST['action'])&&$_POST['action']=='update')
	{
		$data=$_POST;update_option('image-grab.azureAPIKey',$data['azure_api_key']);
		update_option('image-grab.defaultCategory',serialize($data['dafault_category']));
		update_option('image-grab.multipleCategory',$data['multiple_category']);
		update_option('image-grab.duplicate',$data['duplicate']);
		update_option('image-grab.contentDesc',$data['content_desc']);
		update_option('image-grab.contentPos',$data['content_pos']);
		update_option('image-grab.schedule_interval',$data['interval']);
		update_option('image-grab.post_title',$data['post_title']);
		update_option('image-grab.image_title',$data['image_title']);
		update_option('image-grab.image_alt',$data['image_alt']);
		update_option('image-grab.image_caption',$data['image_caption']);
		update_option('image-grab.image_name',$data['image_name']);
		$data['default_category']=unserialize(get_option('image-grab.defaultCategory'));
	}
	include_once("option.view.php");
}
public function display()
{
	$apiKey=get_option('image-grab.azureAPIKey');
	if(empty($apiKey)){
		echo '<div class="updated below-h2" id="message"><p>You need to configuration. <a href="'.admin_url('admin.php?page=ig-options').'">Configure</a></p></div>';
		return;
	}
	if(isset($_POST['act'])&&$_POST['act']=='save-post')
	{
		print_r($_POST);
		//exit;
		$posts=$_POST['data'];
		if(isset($_POST['date']))
		{
			$date=$_POST['date'];
		}
		if(isset($_POST['publish']))
		{
			$status='publish';
		}else{
			$status='draft';
			}
		$date="{$date[year]}-{$date[month]}-{$date[day]} {$date[hour]}:{$date[minute]}:00";
		$interval=$_POST['interval'];
		$category=$_POST['category'];
		
		foreach($posts as $post)
		{
			$data=array('post_title'=>$post['post_title'],'post_category'=>$category,'post_status'=>$status,'tags_input'=>$post['post_tags']);
			if(isset($date)){$data['post_date']=$date;
			$date=date('Y-m-d H:i:s',strtotime("+ {$interval} minutes",strtotime($date)));
			}
			if($post_id=wp_insert_post($data))
			{
				$pmeta_desc	  = $post['seo_meta_description'];
				$pmeta_key	  = $post['seo_meta_keyword'].','.$post['post_tags'];
				$filename=$post['filename'];
				
				$title=$post['title'];
				$alt=$post['title'];
				$caption=$post['title'];
				$image_url=$post['image_url'];
				$description=$post['description'];
				$uploads=wp_upload_dir();
				$filename=wp_unique_filename($uploads['path'],$filename,$unique_filename_callback=null);
				$wp_filetype=wp_check_filetype($filename,null);
		    	add_post_meta($post_id, 'seo_meta_keyword', $pmeta_key, TRUE);
			    add_post_meta($post_id, 'seo_meta_description', $pmeta_desc, TRUE);				
				$fullpathfilename=$uploads['path']."/".$filename;
				try{
					if(!substr_count($wp_filetype['type'],"image"))
				     {
						 throw new Exception($filename.' is not a valid image. '.$wp_filetype['type'].'');
					}
					$image_string=$this->fetch_image($image_url);
					$fileSaved=file_put_contents($uploads['path']."/".$filename,$image_string);
					if(!$fileSaved)
					{
						throw new Exception("The file cannot be saved.");
					}
					$attachment=array('post_mime_type'=>$wp_filetype['type'],'post_title'=>$post['image_title'],'post_content'=>'','post_excerpt'=>$post['img_alt'],'post_status'=>'inherit','guid'=>$uploads['url']."/".$filename);
					$attach_id=wp_insert_attachment($attachment,$fullpathfilename,$post_id);
					//add_post_meta($post_id,'yoast_wpseo_focuskw',$post['post_keywords']);
					
					if(!$attach_id)
					{
						throw new Exception("Failed to save record into database.");
					}
					require_once(ABSPATH."wp-admin".'/includes/image.php');
					$attach_data=wp_generate_attachment_metadata($attach_id,$fullpathfilename);
					wp_update_attachment_metadata($attach_id,$attach_data);
					update_post_meta($attach_id,'_wp_attachment_image_alt',$alt.'fasdfasdf');
					if(get_option('image-grab.contentPos')=='before')$content=$description."\r\n\r\n<p style=\"text-align: center;\">".wp_get_attachment_image($attach_id,'full')."\r\n".$title."</p>";
					else 
					{ $imagetag=wp_get_attachment_image($attach_id,'full',$default_attr = array(
	'src'	=> $src,
));
					//$imagetag2 = str_replace('alt=""','alt="'.$alt.'thisssss"',$imagetag);
						$atturl = wp_get_attachment_image_src($attach_id);
						$cnt = '<img src="'.$atturl[0].'" alt="'.$post['img_alt'].'" title="'.$post["img_title"].'" />';
						$content="<p style=\"text-align: center;\">".$cnt."\r\n".$title."</p>\r\n\r\n".$description;    wp_update_post(array('ID'=>$post_id,'post_content'=>$content));
				 }
				 }catch(Exception $e)
				 {
					 $error='<div id="message" class="error"><p>'.$e->getMessage().'</p></div>';
			     }
				 }
				 }
				 return;
				 }else if(isset($_POST['submit'])&&$_POST['act']=='step2')
				 {
					 $checked=$_POST['checked'];
					 $post=$_POST;

					 $data=array();
					 foreach($checked as $id)
					 {
						 $title=@trim($post['title'][$id],' ... ');
						 $image_url=$post['image_url'][$id];
						 $thumb_url=$post['thumb_url'][$id];
						 $dimension=$post['dimension'][$id];
						 $file_size=$post['file_size'][$id];
						 $data[]=(object) array('id'=>$id,'title'=>$title,'image_url'=>$image_url,'thumb_url'=>$thumb_url,'description'=>stripslashes(str_replace(array("{TITLE}","{DIMENSION}","{FILE_SIZE}"),array($title,$dimension,$file_size),get_option('image-grab.contentDesc'))));}$interval=get_option('image-grab.schedule_interval');
						 $recentPost=$this->_getLastPost();
						 $postDate=$recentPost['post_date'];
						 $postDate=date('Y-m-d H:i:s',strtotime("+ {$interval} minutes",strtotime($postDate)));
						 include_once('display_edit.view.php');
						 return;
					}
					if(isset($_REQUEST['q']))
					{
						$q=$_REQUEST['q'];
						$filter=$_REQUEST['filter'];
						$filterSize="Size:".$filter;$skip=$_REQUEST['skip'];
						$results=$this->bingSearch($q,$filterSize,40,$skip);
						$next=preg_replace('/skip=(\d+)/','skip='.($skip+40),$_SERVER['REQUEST_URI']);
						if($skip>0)$prev=preg_replace('/skip=(\d+)/','skip='.($skip-40),$_SERVER['REQUEST_URI']);
					}
					include_once("display.view.php");
				}
	function _buildCategoryOptionList($name,$optionsSelected=null,$id=null,$multiple=false)
	{
		if($id)$_id=' id="'.$id.'"';
		 if($multiple)$_multiple=' size="10" multiple';
		 $options='<select name="'.$name.'"'.$_id.$_multiple.'>';
		 $categories=get_categories('get=all');
		 foreach($categories as $category):$category=sanitize_category($category);
		 if(is_array($optionsSelected)){
			 if(in_array($category->term_id,$optionsSelected))$selected=" selected='selected'";
			 else $selected='';
		}
		else
		{
			if($category->term_id==$optionsSelected)$selected=" selected='selected'";
			else $selected='';
		}
		$options.="\n\t<option value='$category->term_id' $selected>$category->name</option>";
	   endforeach;
	   $options.='</select>';
	   return $options;
	}
  function _getLastPost()
  {
	  $args=array('numberposts'=>1,'offset'=>0,'category'=>0,'orderby'=>'post_date','order'=>'DESC','post_type'=>'post','post_status'=>'draft, publish, future, pending, private','suppress_filters'=>true);
	  $res=wp_get_recent_posts($args);
	  return $res[0];
 }
public function adminMenu()
{
	if(function_exists('add_object_page'))
	{
		add_object_page('ImageGrabber','Image Grabber','administrator','image-grab',array(&$this,'display'));
	}
	else
	{
		add_menu_page('ImageGrabber','Image Grabber','administrator','image-grab',array(&$this,'display'));
		
	}
	add_submenu_page('image-grab','Grab Images','Grab Images','administrator','image-grab',array(&$this,'display'));
	add_submenu_page('image-grab','Options','Options','administrator','ig-options',array(&$this,'options'));
}
private function bingSearch($q,$filter=null,$top=20,$skip=null)
{
	$accountKey=get_option('image-grab.azureAPIKey');
	$ServiceRootURL='https://api.datamarket.azure.com/Bing/Search/';$WebSearchURL=$ServiceRootURL.'Image?$format=json&$top='.$top.'&Query=';
	$request=$WebSearchURL.urlencode('\''.$q.'\'');
	if($filter)$request=$request.'&ImageFilters='.urlencode('\''.$filter.'\'');
	if($skip)$request=$request.'&$skip='.$skip;$process=curl_init($request);
	curl_setopt($process,CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
	curl_setopt($process,CURLOPT_USERPWD,"ignored:".$accountKey);
	curl_setopt($process,CURLOPT_TIMEOUT,30);
	curl_setopt($process,CURLOPT_RETURNTRANSFER,TRUE);
	$response=curl_exec($process);
	$jsonobj=json_decode($response);
	return $jsonobj;
	}
}
$ImageGrabber=new ImageGrabber();?>
