<?php

/*
Plugin Name: NAVBUTTONSPOST
Description: Insert nav buttons redirecting to previews viewved post and random post
Version: 1.0
Author: Elvis Morales
Author URI: http://www.linkedin.com/in/elvismdev
 */

define('EDITPOST_VERSION', '1.0');
define('EDITPOST_PLUGIN_URL', plugin_dir_url( __FILE__ ));

/* Here are some parameters you may want to change: */
$zg_cookie_expire = 360; // After how many days should the cookie expire? Default is 360.
$zg_number_of_posts = 1; // How many posts should be displayed in the list? Default is 1.
$zg_recognize_pages = true; // Should pages to be recognized and listed? Default is true.

/* Do not edit after this line! */

function zg_lwp_header() { // Main function is called every time a page/post is being generated
	if (is_single()) {
		zg_lw_setcookie();
	} else if (is_page()) {
		global $zg_recognize_pages;
		if ($zg_recognize_pages === true) {
			zg_lw_setcookie();
		}
	}
}

function zg_lw_setcookie() { // Do the stuff and set cookie
	global $wp_query;
	$zg_post_ID = $wp_query->post->ID; // Read post-ID
	if (! isset($_COOKIE["WP-LastViewedPosts"])) {
		$zg_cookiearray = array($zg_post_ID); // If there's no cookie set, set up a new array
	} else {
		$zg_cookiearray = unserialize(preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", stripslashes($_COOKIE["WP-LastViewedPosts"]))); // Read serialized array from cooke and unserialize it
		if (! is_array($zg_cookiearray)) {
			$zg_cookiearray = array($zg_post_ID); // If array is fucked up...just build a new one.
		}
	}
  	if (in_array($zg_post_ID, $zg_cookiearray)) { // If the item is already included in the array then remove it
  		$zg_key = array_search($zg_post_ID, $zg_cookiearray);
  		array_splice($zg_cookiearray, $zg_key, 1);
  	}
	array_unshift($zg_cookiearray, $zg_post_ID); // Add new entry as first item in array
	global $zg_number_of_posts;
	while (count($zg_cookiearray) > $zg_number_of_posts) { // Limit array to xx (zg_number_of_posts) entries. Otherwise cut off last entry until the right count has been reached
		array_pop($zg_cookiearray);
	}
	$zg_blog_url_array = parse_url(get_bloginfo('url')); // Get URL of blog
	$zg_blog_url = $zg_blog_url_array['host']; // Get domain
	$zg_blog_url = str_replace('www.', '', $zg_blog_url);
	$zg_blog_url_dot = '.';
	$zg_blog_url_dot .= $zg_blog_url;
	$zg_path_url = $zg_blogn_url_array['path']; // Get path
	$zg_path_url_slash = '/';
	$zg_path_url .= $zg_path_url_slash;
	global $zg_cookie_expire;
	setcookie("WP-LastViewedPosts", serialize($zg_cookiearray), (time()+($zg_cookie_expire*86400)), $zg_path_url, $zg_blog_url_dot, 0);
}

function drawnavbuttons () {
	$randompost = get_posts ( array( 'posts_per_page'   => 1, 'orderby' => 'rand' ));

	$navbuttons = '<div style="text-align: center; padding-bottom: 8px;">';

	if (isset($_COOKIE["WP-LastViewedPosts"])) {
            $zg_post_IDs = unserialize(preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", stripslashes($_COOKIE["WP-LastViewedPosts"]))); // Read serialized array from cooke and unserialize it
            $prev_post = get_post($zg_post_IDs[0]);

            $navbuttons .= '<div class="btn btn-default" style="background-color: #000;">
            <a href="'.get_permalink($prev_post->ID).'"
            title="'.get_the_title($prev_post->ID).'"><span class="glyphicon glyphicon-chevron-left"></span><strong> Previous</strong></a>
            </div> ';
        }
        $navbuttons .= '<div class="btn btn-default" style="background-color: #000;">
        <a href="'.get_permalink($randompost[0]->ID).'"
        title="'.get_the_title($randompost[0]->ID).'"><strong>Next </strong><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div>
        </div>';
        return $navbuttons;
    }

    add_action('get_header','zg_lwp_header');

    ?>
