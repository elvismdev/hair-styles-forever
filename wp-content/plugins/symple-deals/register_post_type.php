<?php
/***
* Special Thanks To Devin Price
* This file is a modified of the original plugin found @https://github.com/devinsays/portfolio-post-type - Special Thanks!
***/

if ( ! class_exists( 'Symple_Deals_Post_Type' ) ) :
class Symple_Deals_Post_Type {

	// Current plugin version
	var $version = 1;

	function __construct() {

		// Runs when the plugin is activated
		register_activation_hook( __FILE__, array( &$this, 'plugin_activation' ) );

		// Add support for translations
		load_plugin_textdomain( 'symple', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );

		// Adds the deals post type and taxonomies
		add_action( 'init', array( &$this, 'deals_init' ) );

		// Thumbnail support for deals posts
		add_theme_support( 'post-thumbnails', array( 'deals' ) );

		// Adds columns in the admin view for thumbnail and taxonomies
		add_filter( 'manage_edit-deals_columns', array( &$this, 'deals_edit_columns' ) );
		add_action( 'manage_posts_custom_column', array( &$this, 'deals_column_display' ), 10, 2 );

		// Allows filtering of posts by taxonomy in the admin view
		add_action( 'restrict_manage_posts', array( &$this, 'deals_add_taxonomy_filters' ) );

		// Show deals post counts in the dashboard
		add_action( 'right_now_content_table_end', array( &$this, 'add_deals_counts' ) );

		// Give the deals menu item a unique icon
		add_action( 'admin_head', array( &$this, 'deals_icon' ) );
	}

	/**
	 * Flushes rewrite rules on plugin activation to ensure deals posts don't 404
	 * http://codex.wordpress.org/Function_Reference/flush_rewrite_rules
	 */

	function plugin_activation() {
		$this->deals_init();
		flush_rewrite_rules();
	}

	function deals_init() {

		/**
		 * Enable the Deals custom post type
		 * http://codex.wordpress.org/Function_Reference/register_post_type
		 */

		$labels = array(
			'name' => __( 'Deals', 'symple' ),
			'singular_name' => __( 'Deals Item', 'symple' ),
			'add_new' => __( 'Add New Item', 'symple' ),
			'add_new_item' => __( 'Add New Deals Item', 'symple' ),
			'edit_item' => __( 'Edit Deals Item', 'symple' ),
			'new_item' => __( 'Add New Deals Item', 'symple' ),
			'view_item' => __( 'View Item', 'symple' ),
			'search_items' => __( 'Search Deals', 'symple' ),
			'not_found' => __( 'No deals items found', 'symple' ),
			'not_found_in_trash' => __( 'No deals items found in trash', 'symple' )
		);
		
		$args = array(
	    	'labels' => $labels,
	    	'public' => true,
			'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'custom-fields', 'revisions' ),
			'capability_type' => 'post',
			'rewrite' => array("slug" => "deals"), // Permalinks format
			'menu_position' => 5,
			'has_archive' => true
		); 
		
		$args = apply_filters('symple_deals_args', $args);
		
		register_post_type( 'deals', $args );

		/**
		 * Register a taxonomy for Deals Tags
		 * http://codex.wordpress.org/Function_Reference/register_taxonomy
		 */

		$taxonomy_deals_tag_labels = array(
			'name' => _x( 'Deals Tags', 'symple' ),
			'singular_name' => _x( 'Deals Tag', 'symple' ),
			'search_items' => _x( 'Search Deals Tags', 'symple' ),
			'popular_items' => _x( 'Popular Deals Tags', 'symple' ),
			'all_items' => _x( 'All Deals Tags', 'symple' ),
			'parent_item' => _x( 'Parent Deals Tag', 'symple' ),
			'parent_item_colon' => _x( 'Parent Deals Tag:', 'symple' ),
			'edit_item' => _x( 'Edit Deals Tag', 'symple' ),
			'update_item' => _x( 'Update Deals Tag', 'symple' ),
			'add_new_item' => _x( 'Add New Deals Tag', 'symple' ),
			'new_item_name' => _x( 'New Deals Tag Name', 'symple' ),
			'separate_items_with_commas' => _x( 'Separate deals tags with commas', 'symple' ),
			'add_or_remove_items' => _x( 'Add or remove deals tags', 'symple' ),
			'choose_from_most_used' => _x( 'Choose from the most used deals tags', 'symple' ),
			'menu_name' => _x( 'Deals Tags', 'symple' )
		);

		$taxonomy_deals_tag_args = array(
			'labels' => $taxonomy_deals_tag_labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => false,
			'rewrite' => array( 'slug' => 'deals-tag' ),
			'query_var' => true
		);

		$taxonomy_deals_tag_args = apply_filters('symple_taxonomy_deals_tag_args', $taxonomy_deals_tag_args);
		
		register_taxonomy( 'deals_tag', array( 'deals' ), $taxonomy_deals_tag_args );

		/**
		 * Register a taxonomy for Deals Categories
		 * http://codex.wordpress.org/Function_Reference/register_taxonomy
		 */

	    $taxonomy_deals_category_labels = array(
			'name' => _x( 'Deals Categories', 'symple' ),
			'singular_name' => _x( 'Deals Category', 'symple' ),
			'search_items' => _x( 'Search Deals Categories', 'symple' ),
			'popular_items' => _x( 'Popular Deals Categories', 'symple' ),
			'all_items' => _x( 'All Deals Categories', 'symple' ),
			'parent_item' => _x( 'Parent Deals Category', 'symple' ),
			'parent_item_colon' => _x( 'Parent Deals Category:', 'symple' ),
			'edit_item' => _x( 'Edit Deals Category', 'symple' ),
			'update_item' => _x( 'Update Deals Category', 'symple' ),
			'add_new_item' => _x( 'Add New Deals Category', 'symple' ),
			'new_item_name' => _x( 'New Deals Category Name', 'symple' ),
			'separate_items_with_commas' => _x( 'Separate deals categories with commas', 'symple' ),
			'add_or_remove_items' => _x( 'Add or remove deals categories', 'symple' ),
			'choose_from_most_used' => _x( 'Choose from the most used deals categories', 'symple' ),
			'menu_name' => _x( 'Deals Categories', 'symple' ),
	    );

	    $taxonomy_deals_category_args = array(
			'labels' => $taxonomy_deals_category_labels,
			'public' => true,
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'show_tagcloud' => true,
			'hierarchical' => true,
			'rewrite' => array( 'slug' => 'deals-category' ),
			'query_var' => true
	    );

		$taxonomy_deals_category_args = apply_filters('symple_taxonomy_deals_category_args', $taxonomy_deals_category_args);
		
	    register_taxonomy( 'deals_category', array( 'deals' ), $taxonomy_deals_category_args );

	}

	/**
	 * Add Columns to Deals Edit Screen
	 * http://wptheming.com/2010/07/column-edit-pages/
	 */

	function deals_edit_columns( $deals_columns ) {
		$deals_columns = array(
			"cb" => "<input type=\"checkbox\" />",
			"title" => _x('Title', 'column name'),
			"deals_thumbnail" => __('Thumbnail', 'symple'),
			"deals_category" => __('Category', 'symple'),
			"deals_tag" => __('Tags', 'symple'),
			"author" => __('Author', 'symple'),
			"comments" => __('Comments', 'symple'),
			"date" => __('Date', 'symple'),
		);
		$deals_columns['comments'] = '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>';
		return $deals_columns;
	}

	function deals_column_display( $deals_columns, $post_id ) {

		// Code from: http://wpengineer.com/display-post-thumbnail-post-page-overview

		switch ( $deals_columns ) {

			// Display the thumbnail in the column view
			case "deals_thumbnail":
				$width = (int) 80;
				$height = (int) 80;
				$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );

				// Display the featured image in the column view if possible
				if ( $thumbnail_id ) {
					$thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
				}
				if ( isset( $thumb ) ) {
					echo $thumb;
				} else {
					echo __('None', 'symple');
				}
				break;	

			// Display the deals tags in the column view
			case "deals_category":

			if ( $category_list = get_the_term_list( $post_id, 'deals_category', '', ', ', '' ) ) {
				echo $category_list;
			} else {
				echo __('None', 'symple');
			}
			break;	

			// Display the deals tags in the column view
			case "deals_tag":

			if ( $tag_list = get_the_term_list( $post_id, 'deals_tag', '', ', ', '' ) ) {
				echo $tag_list;
			} else {
				echo __('None', 'symple');
			}
			break;			
		}
	}

	/**
	 * Adds taxonomy filters to the deals admin page
	 * Code artfully lifed from http://pippinsplugins.com
	 */

	function deals_add_taxonomy_filters() {
		global $typenow;

		// An array of all the taxonomyies you want to display. Use the taxonomy name or slug
		$taxonomies = array( 'deals_category', 'deals_tag' );

		// must set this to the post type you want the filter(s) displayed on
		if ( $typenow == 'deals' ) {

			foreach ( $taxonomies as $tax_slug ) {
				$current_tax_slug = isset( $_GET[$tax_slug] ) ? $_GET[$tax_slug] : false;
				$tax_obj = get_taxonomy( $tax_slug );
				$tax_name = $tax_obj->labels->name;
				$terms = get_terms($tax_slug);
				if ( count( $terms ) > 0) {
					echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
					echo "<option value=''>$tax_name</option>";
					foreach ( $terms as $term ) {
						echo '<option value=' . $term->slug, $current_tax_slug == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
					}
					echo "</select>";
				}
			}
		}
	}

	/**
	 * Add Deals count to "Right Now" Dashboard Widget
	 */

	function add_deals_counts() {
	        if ( ! post_type_exists( 'deals' ) ) {
	             return;
	        }

	        $num_posts = wp_count_posts( 'deals' );
	        $num = number_format_i18n( $num_posts->publish );
	        $text = _n( 'Deals Item', 'Deals Items', intval($num_posts->publish) );
	        if ( current_user_can( 'edit_posts' ) ) {
	            $num = "<a href='edit.php?post_type=deals'>$num</a>";
	            $text = "<a href='edit.php?post_type=deals'>$text</a>";
	        }
	        echo '<td class="first b b-deals">' . $num . '</td>';
	        echo '<td class="t deals">' . $text . '</td>';
	        echo '</tr>';

	        if ($num_posts->pending > 0) {
	            $num = number_format_i18n( $num_posts->pending );
	            $text = _n( 'Deals Item Pending', 'Deals Items Pending', intval($num_posts->pending) );
	            if ( current_user_can( 'edit_posts' ) ) {
	                $num = "<a href='edit.php?post_status=pending&post_type=deals'>$num</a>";
	                $text = "<a href='edit.php?post_status=pending&post_type=deals'>$text</a>";
	            }
	            echo '<td class="first b b-deals">' . $num . '</td>';
	            echo '<td class="t deals">' . $text . '</td>';

	            echo '</tr>';
	        }
	}

	/**
	 * Displays the custom post type icon in the dashboard
	 */

	function deals_icon() { ?>
	    <style type="text/css" media="screen">
	        #menu-posts-deals .wp-menu-image {
	            background: url(<?php echo plugin_dir_url( __FILE__ ); ?>images/deals-icon.png) no-repeat 6px -17px !important;
	        }
			#menu-posts-deals:hover .wp-menu-image, #menu-posts-deals.wp-has-current-submenu .wp-menu-image {
	            background-position:6px 7px !important;
	        }
	    </style>
	<?php }

}

new Symple_Deals_Post_Type;

endif;