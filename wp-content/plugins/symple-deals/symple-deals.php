<?php
/*
Plugin Name: Symple Deals
Plugin URI: http://themeforest.net/user/WPExplorer
Description: Registers a new "deals" custom post type
Author: AJ Clarke
Author URI: http://www.wpexplorer.com
Version: 1.0
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/


//Include files
require_once( dirname(__FILE__) . '/register_post_type.php' );