<?php

/*-----------------------------------------------------------------------------------*/
/*	Options Framework
/*-----------------------------------------------------------------------------------*/

// You can mess with these 2 if you wish.

define('OPTIONS', 'of_options'); // Name of the database row where your options are stored

/* Options Framework */
require_once('admin/index.php');







/*-----------------------------------------------------------------------------------*/
/*	Theme Setup
/*-----------------------------------------------------------------------------------*/

function uxde_setup() {
	// Add language supports.
	load_theme_textdomain('uxde');

	// Add post-formats
	add_theme_support('post-formats', array('video', 'gallery'));
	add_post_type_support('post', 'post-formats');
	
	
	// Add post thumbnail supports. http://codex.wordpress.org/Post_Thumbnails
	add_theme_support( 'automatic-feed-links' );
	add_theme_support('post-thumbnails');
	add_image_size( '681x220-thumb', 681, 220, true );
	add_image_size( '338x220-thumb', 338, 220, true );
	add_image_size( '530x260-thumb', 530, 260, true );
	add_image_size( '245x140-thumb', 245, 140, true );
	add_image_size( '382x200-thumb', 382, 200, true );
	add_image_size( '100x100-thumb', 100, 100, true );
	add_image_size( '255x230-thumb', 255, 230, true );
	add_image_size( '435x200-thumb', 435, 200, true );
	add_image_size( '250x220-thumb', 250, 220, true );
	add_image_size( '140x80-thumb', 140, 80, true );
	add_image_size( '795x350-thumb', 795, 350, true );
	add_image_size( '248x140-thumb', 248, 140, true );

	
	// Add menu supports. http://codex.wordpress.org/Function_Reference/register_nav_menus
	add_theme_support('menus');
	register_nav_menus(array(
		'top_navigation' => __('Top Navigation', 'uxde'),
		'primary_navigation' => __('Primary Navigation', 'uxde')
	));	
	
	include_once('lib/theme-editor/admin-editor.php');
	include_once('lib/theme-editor/theme-shortcodes.php');
	include_once('lib/theme-functions/theme-pagination.php');
	include_once('lib/theme-functions/theme-metaboxes.php');
	include_once('lib/theme-functions/uxde-metabox.php');
	include_once('lib/theme-functions/breadcrumbs.php');
	include_once('lib/theme-functions/user-rating.php');
	
	/* ==  Widgets  ==============================*/

	include_once('lib/theme-widgets/widget-recentposts-home-1.php');
	include_once('lib/theme-widgets/widget-recentposts-home-2.php');
	include_once('lib/theme-widgets/widget-recentposts-home-3.php');
	include_once('lib/theme-widgets/widget-recentposts-home-half.php');
	include_once('lib/theme-widgets/widget-recentposts-home-half-last.php');
	include_once('lib/theme-widgets/widget-recentposts-home-gallery.php');
	include_once('lib/theme-widgets/widget-home-ads795.php');
	include_once('lib/theme-widgets/widget-socialicons-sidebar.php');
	include_once('lib/theme-widgets/widget-socialicons-footer.php');
	include_once('lib/theme-widgets/widget-twittertweets.php');
	include_once('lib/theme-widgets/widget-facebooklike.php');
	include_once('lib/theme-widgets/widget-flickrphotos.php');
	include_once('lib/theme-widgets/widget-popularposts.php');
	include_once('lib/theme-widgets/widget-recentposts.php');
	include_once('lib/theme-widgets/widget-recent-reviews.php');
	include_once('lib/theme-widgets/widget-newsletter.php');
	include_once('lib/theme-widgets/widget-categories.php');
	include_once('lib/theme-widgets/widget-tabbed.php');
	include_once('lib/theme-widgets/widget-video.php');
	include_once('lib/theme-widgets/widget-ads125.php');
	include_once('lib/theme-widgets/widget-ads300.php');
}
add_action('after_setup_theme', 'uxde_setup');

function optionscheck_change_santiziation() {
    remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
    add_filter( 'of_sanitize_textarea', 'custom_sanitize_textarea' );
}
add_action('admin_init','optionscheck_change_santiziation', 100);

function custom_sanitize_textarea($input) {
    global $allowedposttags;
      $custom_allowedtags["script"] = array( "src" => array(), "type" => array() );
      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
      $output = wp_kses( $input, $custom_allowedtags);
    return $output;
}






/*-----------------------------------------------------------------------------------*/
/*	Configure Custom Homepage Excerpt Length
/*-----------------------------------------------------------------------------------*/
		
function limit_words($string, $word_limit) {
		 
	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character
		 
		$words = explode(' ', $string);
		 
	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character
		 
	return implode(' ', array_slice($words, 0, $word_limit));
		 
}






/*-----------------------------------------------------------------------------------*/
/*	Content Width
/*-----------------------------------------------------------------------------------*/

$content_width = 798;

if ( is_page_template('page-full.php') ) $content_width = 1181;






/*-----------------------------------------------------------------------------------*/
/*	Theme Javascripts
/*-----------------------------------------------------------------------------------*/

function uxde_specific_theme_js() {

	if(!is_admin()) {
		
		/* Register our scripts -----------------------------------------------------*/
		
		wp_register_script('jquery-api', get_template_directory_uri() . '/js/jquery.js', array('jquery'), '1.9.1', true );
		wp_register_script('jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js', array('jquery'), '1.9.2', true );
		wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.js', array('jquery'), '2.6.2', true );
		wp_register_script('superfish', get_template_directory_uri() . '/js/superfish.js', array('jquery'), '1.6.9', true );
		wp_register_script('superfish-hoverIntent', get_template_directory_uri() . '/js/hoverIntent.js', array('jquery'), '1.0.0', true );
		wp_register_script('scrollbar', get_template_directory_uri() . '/js/scrollbar.js', array('jquery'), '1.8.1', true );	
		wp_register_script('flexslider', get_template_directory_uri() . '/js/flexslider.js', array('jquery'), '2.1.0', true );
		wp_register_script('fitvids', get_template_directory_uri() . '/js/fitvids.js', array('jquery'), '1.0.0', true );	
		wp_register_script('twitter', get_template_directory_uri() . '/js/twitter.js', array('jquery'), '1.0.0', true );
		wp_register_script('user-rating', get_template_directory_uri() . '/js/user-rating.js', array('jquery'), '1.0.0', true );
		wp_register_script('backstretch', get_template_directory_uri() . '/js/backstretch.js', array('jquery'), '2.0.3', true );
		wp_register_script('infinitescroll', get_template_directory_uri() . '/js/infinitescroll.js', array('jquery'), '1.5.1', true );
		wp_register_script('retina', get_template_directory_uri() . '/js/retina.js', array('jquery'), '1.0.0', true );
		wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0', true );	

		/* Enqueue our scripts ------------------------------------------------------*/

		wp_enqueue_script('jquery-api');
		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('modernizr');
		wp_enqueue_script('superfish');
		wp_enqueue_script('superfish-hoverIntent');
		if ( is_home() ) {
		global $data;
			if ( $data['uxde_slider'] ) {
				wp_enqueue_script('scrollbar');
			}
		}
		wp_enqueue_script('flexslider');
		wp_enqueue_script('fitvids');
		wp_enqueue_script('twitter');
		if ( is_single() ) {
			wp_enqueue_script('user-rating');
		}
		wp_enqueue_script('backstretch');
		global $data;
		if ($data['uxde_pagination_style'] == 'Infinite') {
			wp_enqueue_script('infinitescroll');
		}
		wp_enqueue_script('retina');
		wp_enqueue_script('custom');
		
		if ( is_singular() ) {
			wp_enqueue_script( 'comment-reply' );
		}
 
		// declare the URL to the file that handles the AJAX request (wp-admin/admin-ajax.php)
		wp_localize_script( 'my-ajax-request', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );


	}

}

add_action('wp_print_scripts', 'uxde_specific_theme_js');






/*-----------------------------------------------------------------------------------*/
/*	Theme Styles
/*-----------------------------------------------------------------------------------*/

function uxde_specific_theme_style()  {  

	wp_register_style( 'extras-style', get_template_directory_uri() . '/extras.css', array(), '1', 'all' ); 
	wp_register_style( 'fontawesome-style', get_template_directory_uri() . '/css/font-awesome.css', array(), '3.0', 'all' ); 
	wp_register_style( 'flexslider-style', get_template_directory_uri() . '/css/flexslider.css', array(), '1', 'all' );
	wp_register_style( 'responsive-style', get_template_directory_uri() . '/media-queries.css', array(), '1', 'all' );   


	wp_enqueue_style( 'extras-style' );  	
	wp_enqueue_style( 'fontawesome-style' );
	wp_enqueue_style( 'flexslider-style' );
	global $data;
	if ( $data['uxde_responsive'] ) {
			wp_enqueue_style( 'responsive-style' );
		}
}

add_action( 'wp_enqueue_scripts', 'uxde_specific_theme_style' );  






/*-----------------------------------------------------------------------------------*/
/*	Widgets Register
/*-----------------------------------------------------------------------------------*/

if(function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => 'Sidebar Widgets',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Homepage Widgets',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Footer Widget 1',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Footer Widget 2',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Footer Widget 3',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Footer Widget 4',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'name' => 'Footer Widget 5',
		'before_widget' => '<div id="%1$s" class="row widget %2$s"><div class="widget-section">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));

}






/*-----------------------------------------------------------------------------------*/
/*	Customize Excerpt
/*-----------------------------------------------------------------------------------*/

function trim_excerpt($text)
{
	return str_replace(' [...]', '...', $text);
}
add_filter('get_the_excerpt', 'trim_excerpt');




 
 
/*-----------------------------------------------------------------------------------*/
/*	Browser Classes
/*-----------------------------------------------------------------------------------*/

add_filter('body_class','browser_body_class');
function browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown';
    if($is_iphone) $classes[] = 'iphone';
    return $classes;
}






/*-----------------------------------------------------------------------------------*/
/*	Menu Mobile
/*-----------------------------------------------------------------------------------*/

function uxde_menu_mobile(){

wp_nav_menu(array(

  'theme_location' => 'top_navigation', // your theme location here

  'walker'         => new Walker_Nav_Menu_Dropdown(),

  'items_wrap'     => '<select id="sec-selector" name="sec-selector" onchange="location.href = document.getElementById(\'sec-selector\').value;">%3$s</select>',

  'container_id' => 'main-menu-mobile'

));

}

function uxde_primary_menu_mobile(){

wp_nav_menu(array(

  'theme_location' => 'primary_navigation', // your theme location here

  'walker'         => new Walker_Nav_Menu_Dropdown(),

  'items_wrap'     => '<select id="sec-selector-primary" name="sec-selector" onchange="location.href = document.getElementById(\'sec-selector-primary\').value;">%3$s</select>',

  'container_id' => 'main-menu-mobile'

));

}

class Walker_Nav_Menu_Dropdown extends Walker_Nav_Menu{

	var $to_depth = -1;

    function start_lvl(&$output, $depth){

      $output .= '</option>';

    }

    function end_lvl(&$output, $depth){

      $indent = str_repeat("\t", $depth); 

    }

    function start_el(&$output, $item, $depth, $args){

		$indent = ( $depth ) ? str_repeat( "&nbsp;", $depth * 4 ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;

		$classes[] = 'menu-item-' . $item->ID;

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

		$class_names = ' class="' . esc_attr( $class_names ) . '"';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );

		$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

		$value = ' value="'. $item->url .'"';

		$output .= '<option'.$id.$value.$class_names.'>';

		$item_output = $args->before;

		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

		$output .= $indent.$item_output;
    }

    function end_el(&$output, $item, $depth){

		if(substr($output, -9) != '</option>')

      		$output .= "</option>"; // replace closing </li> with the option tag

    }

}






/*-----------------------------------------------------------------------------------*/
/*	Class to Menus
/*-----------------------------------------------------------------------------------*/

add_filter('nav_menu_css_class', 'dt_add_ancestor_class', 2, 10);

function dt_add_ancestor_class($classlist, $item){
	global $wp_query, $wpdb;
	//get the ID of the object, to which menu item points
	$id = get_post_meta($item->ID, '_menu_item_object_id', true);
	//get first menu item that is a child of that object
	$children = $wpdb->get_var('SELECT post_id FROM '.$wpdb->postmeta.' WHERE meta_key like "_menu_item_menu_item_parent" AND meta_value='.$item->ID.' LIMIT 1');
	//if there is at least one item, then $children variable will contain it's ID (which is of course more than 0)
	if($children>0)
		//in that case - add the CSS class
		$classlist[]='menu-item-ancestor';
	//return class list
	return $classlist;
}






/*-----------------------------------------------------------------------------------*/
/*	Calculate Score
/*-----------------------------------------------------------------------------------*/

function uxde_calculate_score($num, $type, $star = false) {
		
		switch ($type) :
		
			case 'star' :
				if(!$star == false){
					if ( $num <= 2 ) $output = '<span title="1 star"><i class="icon-star"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i></span>';
					if ( $num > 2 && $num <= 4 ) 
					$output = '<span title="2 stars"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i></span>';
					if ( $num > 4 && $num <= 6 )
					$output = '<span title="3 stars"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i><i class="icon-star-empty"></i></span>';
					if ( $num > 6 && $num <= 8 ) 
					$output = '<span title="4 stars"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></span>';
					if ( $num > 8 && $num <= 10 ) 
					$output = '<span title="5 stars"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i></span>';
				} else {
					$output = $num;
				}
				break;
			
			case 'letter' :
				if ( $num <= 2 ) $output = 'E';
				if ( $num > 2 && $num <= 4 ) $output = 'D';
				if ( $num > 4 && $num <= 6 ) $output = 'C';
				if ( $num > 6 && $num <= 8 ) $output = 'B';
				if ( $num > 8 && $num <= 10 ) $output = 'A';
				break;
			
			case 'percent';
				$output = $num * 10 . '%';
				break;
			
			case 'number';
				$output = $num;
				break;
			
		endswitch;
		
		return $output;
}






/*-----------------------------------------------------------------------------------*/
/*	Review System Box
/*-----------------------------------------------------------------------------------*/

function uxde_print_review_box($post_id, $class, $echo = true) {
		
		$review_box_title = get_post_meta( $post_id, 'uxde' . 'review_box_title', true );
		$review_box_total_score_label = get_post_meta( $post_id, 'uxde' . 'review_box_total_score_label', true );
		$review_summary = get_post_meta( $post_id, 'uxde' . 'review_summary', true );
		
		$rating_type = get_post_meta( $post_id, 'uxde' . 'rating_type', true );
		$rating_criteria = get_post_meta( $post_id, 'uxde' . 'rating_criteria', true );
		$rating_criteria_count =  count($rating_criteria);
		$author = get_the_author();
		$pfx_date = get_the_date();

		$score_array = array();
			foreach ($rating_criteria as $criteria) {
				$score_array []= $criteria['score'];
			}
			$final_score = array_sum($score_array);
			$final_score = $final_score / $rating_criteria_count;
			$final_score = number_format($final_score, 1, '.', '');
		
		$output = '';
		$output .= '<div itemscope itemtype="http://data-vocabulary.org/Review" id="review-box" class="center">';
		$output .= '<h5 itemprop="itemreviewed" class="itemreviewed">'.$review_box_title.'</h5>';
		$output .= '<span class="reviewer" itemprop="reviewer" style="display:none;">'.$author.'</span>';
		$output .= '<time class="dtreviewed" itemprop="dtreviewed" style="display:none;">'.$pfx_date.'</time>';
		$output .= '<span class="summary" itemprop="summary" style="display:none;">'.$review_summary.'</span>';
		$output .= '<span class="rating" style="display: none;" itemprop="rating">'. uxde_calculate_score($final_score, true).'';
		$output .= '<span itemprop="worst">1</span>';
		$output .= '<span itemprop="best">10</span>';
		$output .= '</span>';
		$output .= '<ul>';
			foreach ($rating_criteria as $criteria) {
				$percentage_score = $criteria['score'] * 10;
				
				if($criteria['c_label'])
				$output .= '<li><div class="review-criteria-score clearfix"><span class="left">'.$criteria['c_label'].'</span><span class="right">'.uxde_calculate_score($criteria['score'], $rating_type, true).'</span></div>';
				$output .= '<div class="review-criteria-bar-container"><div class="review-criteria-bar" style="width:'.$percentage_score.'%"></div></div></li>';
				
			}
		$output .= '</ul>';
		
		$output .= '<div class="review-total-score clearfix">';
			
			$output .='<span class="left align-right">'.$review_box_total_score_label.'</span>';
		    $output .='<span class="right rating review-total-score-boxtype-'.$rating_type.'">'. uxde_calculate_score($final_score, $rating_type, true).'</span>';
    	$output .= '</div>';

		$output .= '<div class="review-summary">';

			$output .= '<p>'.$review_summary.'</p>';

		$output .= '</div>';
    
		$output .= '</div><!-- REVIEW_BOX -->';
		
		if($echo == 'true') :
		echo $output;
		else :
		return $output;
		endif;
}






/*-----------------------------------------------------------------------------------*/
/*	Display Review Badge
/*-----------------------------------------------------------------------------------*/

function uxde_print_review_badge($post_id, $echo = true) {
		
		$rating_type = get_post_meta( $post_id, 'uxde' . 'rating_type', true );
		$rating_criteria = get_post_meta( $post_id, 'uxde' . 'rating_criteria', true );
		$rating_criteria_count =  count($rating_criteria);
		
		$output = '';
		$score_array = array();
		
		if($rating_criteria){
			foreach ($rating_criteria as $criteria) {
				$score_array []= $criteria['score'];
			}
		}
		
		$final_score = array_sum($score_array);
		$final_score = $final_score / $rating_criteria_count;
		$final_score = number_format($final_score, 1, '.', '');
		
		$output = '<div class="review-badge review-badge-'.$rating_type.'">'.uxde_calculate_score($final_score, $rating_type, true).'</div>';
    
		if($echo == 'true') :
		echo $output;
		else :
		return $output;
		endif;
}






/*-----------------------------------------------------------------------------------*/
/*	Active Plugins
/*-----------------------------------------------------------------------------------*/

require_once(TEMPLATEPATH.'/plugins/class-tgm-plugin-activation.php');
add_action('tgmpa_register', 'uxde_register_required_plugins');
function uxde_register_required_plugins() {
	$plugins = array(
		array(
			'name'     				=> 'simple-page-sidebars', // The plugin name
			'slug'     				=> 'simple-page-sidebars', // The plugin slug (typically the folder name)
			'source'   				=> get_template_directory_uri() . '/plugins/simple-page-sidebars.zip', // The plugin source
			'required' 				=> true, // If false, the plugin is only 'recommended' instead of required
			'version' 				=> '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' 		=> false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' 	=> false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' 			=> '', // If set, overrides default API URL and points to an external URL
		),

	);

// Change this to your theme text domain, used for internationalising strings
	$theme_text_domain = 'uxde';

/**
	 * Array of configuration settings. Amend each line as needed.
	 * If you want the default strings to be available under your own theme domain,
	 * leave the strings uncommented.
	 * Some of the strings are added into a sprintf, so see the comments at the
	 * end of each line for what each argument will be.
	 */
	$config = array(
		'domain'       		=> $theme_text_domain,         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> '',                         	// Default absolute path to pre-packaged plugins
		'parent_menu_slug' 	=> 'themes.php', 				// Default parent menu slug
		'parent_url_slug' 	=> 'themes.php', 				// Default parent URL slug
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
		'message' 			=> '',							// Message to output right before the plugins table
		'strings'      		=> array(
			'page_title'                       			=> __( 'Install Required Plugins', $theme_text_domain ),
			'menu_title'                       			=> __( 'Install Plugins', $theme_text_domain ),
			'installing'                       			=> __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
			'oops'                             			=> __( 'Something went wrong with the plugin API.', $theme_text_domain ),
			'notice_can_install_required'     			=> _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_install_recommended'			=> _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_install'  					=> _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
			'notice_can_activate_required'    			=> _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_can_activate_recommended'			=> _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_activate' 					=> _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
			'notice_ask_to_update' 						=> _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
			'notice_cannot_update' 						=> _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
			'install_link' 					  			=> _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
			'activate_link' 				  			=> _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
			'return'                           			=> __( 'Return to Required Plugins Installer', $theme_text_domain ),
			'plugin_activated'                 			=> __( 'Plugin activated successfully.', $theme_text_domain ),
			'complete' 									=> __( 'All plugins installed and activated successfully. %s', $theme_text_domain ), // %1$s = dashboard link
			'nag_type'									=> 'updated' // Determines admin notice type - can only be 'updated' or 'error'
		)
	);

	tgmpa($plugins, $config);
}






/*-----------------------------------------------------------------------------------*/
/* Update Theme
/*-----------------------------------------------------------------------------------*/

require_once('theme-update-notifier.php');






/*-----------------------------------------------------------------------------------*/
/* Pinterest
/*-----------------------------------------------------------------------------------*/

function catch_that_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];

  if(empty($first_img)){ //Defines a default image
    $first_img = "/images/default.jpg";
  }
  return $first_img;
}






/*-----------------------------------------------------------------------------------*/
/* Custom Fields - Categories 
/*-----------------------------------------------------------------------------------*/


//include the main class file
require_once("lib/theme-functions/Tax-meta-class/Tax-meta-class.php");

/*
* configure taxonomy custom fields
*/
$config = array(
   'id' => 'custom_color_categories',                         // meta box id, unique per meta box
   'title' => 'Custom Color Categories',                      // meta box title
   'pages' => array('category'),                    // taxonomy name, accept categories, post_tag and custom taxonomies
   'context' => 'normal',                           // where the meta box appear: normal (default), advanced, side; optional
   'fields' => array(),                             // list of meta fields (can be added by field arrays)
   'local_images' => false,                         // Use local or hosted images (meta box images for add/remove)
   'use_with_theme' => true                       //change path if used with theme set to true, false for a plugin or anything else for a custom path(default false).
);

/*
* Initiate your taxonomy custom fields
*/

$my_meta = new Tax_Meta_Class($config);


/*
* Add fields 
*/

$my_meta->addColor('color_field_id',array('name'=> 'Custom Color '));
$my_meta->addSelect('background_type',array('jquery'=> 'Jquery Full Background', 'css'=> 'CSS Background'), array('name'=> 'Background Type', 'std'=> array('jquery')));
$my_meta->addImage('image_field_id',array('name'=> 'Background Image'));
$my_meta->addColor('background_color',array('name'=> 'Background Color'));
$my_meta->addSelect('background_repeat',array('repeat'=> 'Repeat', 'no-repeat'=> 'No Repeat', 'repeat-x'=> 'Repeat X', 'repeat-y'=>'Repeat Y'), array('name'=> 'Background Repeat', 'std'=> array('repeat')));
$my_meta->addSelect('background_position',array('top left'=> 'Top Left', 'top center'=> 'Top Center', 'top right'=>'Top Right', 'middle left'=>'Middle Left', 'middle center'=>'Middle Center', 'middle right'=>'Middle Right', 'bottom left'=>'Bottom Left', 'bottom center'=>'Bottom Center', 'bottom right'=>'Bottom Right'), array('name'=> 'Background Position', 'std'=> array('top center')));
$my_meta->addSelect('background_attachment',array('fixed'=> 'Fixed', 'scroll'=> 'Scroll'), array('name'=> 'Background Attachment', 'std'=> array('fixed')));

/*
* Don't Forget to Close up the meta box deceleration
*/
//Finish Taxonomy Extra fields Deceleration
$my_meta->Finish();