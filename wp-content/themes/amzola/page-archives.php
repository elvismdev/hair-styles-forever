<?php
/*
Template Name: Archives
*/
get_header(); ?>

<?php $custom_post_layout_meta = get_post_meta(get_the_ID(), 'uxdecustom_post_layout_meta', true); ?>

		<div id="content" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>class="full-width" style="float: left; width: 100%;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?> style="float: left;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: right;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?><?php endif; ?>>

		<div class="post-box">

		<div class="block loop-single">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php echo ux_breadcrumbs(); ?>

				<div class="archive-lists">

					<div class="recent-post-list">

					<h2><?php _e('Recent Posts', 'uxde'); ?></h2>
						<?php query_posts( 'showposts=10' ); ?>
						<?php if ( have_posts() ) : ?>
						<ul>
							<?php while (have_posts()) : the_post(); ?>
							<li>

								<article id="post-<?php the_ID(); ?>-recent" class="widget-post">

									<?php if ( has_post_thumbnail() ) : ?>

										<div class="featured-image">

											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('100x100-thumb'); ?></a>

										</div>

									<?php endif; ?>
	
								<header>
			
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

									<span class="meta-category"><?php $category = get_the_category(); if ($category) { echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'uxde' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> '; } ?></span>

								</header>
			
							</article>	

							</li>
							<?php endwhile; endif; ?>
							 <?php wp_reset_query(); ?> 
						</ul>

					</div>

					<div class="popular-post-list">

					<h2><?php _e('Popular Posts', 'uxde'); ?></h2>
						<?php $year = date('Y'); query_posts('post_type=post&posts_per_page=10&orderby=comment_count&order=DESC&year=' . $year . ''); ?>
						<?php if ( have_posts() ) : ?>
						<ul>
							<?php while (have_posts()) : the_post(); ?>
							<li>

								<article id="post-<?php the_ID(); ?>-recent" class="widget-post">

									<?php if ( has_post_thumbnail() ) : ?>

										<div class="featured-image">

											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('100x100-thumb'); ?></a>

										</div>

									<?php endif; ?>
	
								<header>
			
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

									<span class="meta-category"><?php $category = get_the_category(); if ($category) { echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'uxde' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> '; } ?></span>

								</header>
			
							</article>	

							</li>
							<?php endwhile; endif; ?>
							 <?php wp_reset_query(); ?> 
						</ul>

					</div>

					<div class="clearfix"></div>

					<div class="archives">

					<h2><?php _e('Monthly Archives', 'uxde'); ?></h2>
						<ul class="archive-month">
							<?php
								$args = array (
								'echo' => 0,
								'show_option_all'    => '',
								'show_post_count' => 1,
								'title_li' => ''
								);
								$variable = wp_get_archives($args);
								$variable = str_replace ( "(" , "<span>", $variable );
								$variable = str_replace ( ")" , "</span>", $variable );
								echo $variable;
							?>
						</ul>		

					</div>

					<div class="categories">

					<h2><?php _e('Archives by Categories', 'uxde'); ?></h2>
						<ul class="archive-cat">
							<?php
								$args = array (
								'echo' => 0,
								'show_option_all'    => '',
								'orderby'            => 'name',
								'show_count' => 1,
								'title_li' => '',
								'exclude'  => '',
								'depth' => 1
								);
								$variable = wp_list_categories($args);
								$variable = str_replace ( "(" , "<span>", $variable );
								$variable = str_replace ( ")" , "</span>", $variable );
								echo $variable;
							?>
						</ul>

					</div>

				</div>

			</article>

		</div>

		</div>
	
	</div>

	<aside id="sidebar" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>style="display: none;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?>style="float: right;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: left;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?><?php endif; ?>>

		<?php get_sidebar(''); ?>

	</aside>
		
<?php get_footer(); ?>

