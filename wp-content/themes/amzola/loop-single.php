<?php /* Start loop */ ?><?php global $data; ?>

<?php if ( have_posts() ) : ?>

<?php while (have_posts()) : the_post(); ?>

<?php $custom_gallery_id = get_post_meta(get_the_ID(), 'uxdecustom_gallery_id', true); ?>
<?php $custom_gallery_columns = get_post_meta(get_the_ID(), 'uxdecustom_gallery_columns', true); ?>
<?php $custom_video_code = get_post_meta(get_the_ID(), 'uxdecustom_video_code', true); ?>

<?php $format = get_post_format(); if (false === $format) $format = 'standard'; ?>

<?php $ratings = new uxde_user_review(); ?>

<div class="block loop-single">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<header class="post-header">

			<?php echo ux_breadcrumbs(); ?>

			<h1 class="post-title entry-title"><?php the_title(); ?></h1>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_featured_image', true ) == 'true') : ?>

			<?php if ($format == 'gallery') { ?>

			<div class="featured-image-gallery">

				<?php echo do_shortcode('[gallery orderby="id" columns="' . $custom_gallery_columns . '" ids="' . $custom_gallery_id . '" link="file"]'); ?>

			</div>

			<?php } elseif ($format == 'video') { ?>

				<div class="featured-image video">

					 <?php echo $custom_video_code; ?>

				</div>

			<?php } else { ?>

			<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image clearfix">

					<?php the_post_thumbnail('795x350-thumb'); ?>

				</div>

			<?php endif; ?>

			<?php } ?>

			<?php endif; ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_featured_image', true ) == '') : ?>

			<?php if ($format == 'gallery') { ?>

			<div class="featured-image-gallery">

				<?php echo do_shortcode('[gallery orderby="id" columns="' . $custom_gallery_columns . '" ids="' . $custom_gallery_id . '" link="file"]'); ?>

			</div>

			<?php } elseif ($format == 'video') { ?>

				<div class="featured-image video">

					 <?php echo $custom_video_code; ?>

				</div>

			<?php } else { ?>

			<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image clearfix">

					<?php the_post_thumbnail('795x350-thumb'); ?>

				</div>

			<?php endif; ?>

			<?php } ?>	

			<?php endif; ?>

		</header>
			
		<div class="post-content">

			<span class="meta-info">

				<?php _e('By', 'uxde'); ?> <span class="vcard author"><span class="fn"><?php the_author_posts_link(); ?></span></span> 
			
				<span> - <time class="updated meta-button" datetime="<?php the_time('F j, Y g:i A'); ?>" pubdate> <?php the_time('F j, Y g:i A'); ?></time> - <span class="comment-info"><?php comments_popup_link(__('0 Comments', 'uxde'), __('1 Comment', 'uxde'), '% '.__('Comments', 'uxde')); ?></span></span>
			
			</span>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'enable_review', true ) == true) : ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'review_box_pos', true ) == 'top') : ?>
	
			<?php uxde_print_review_box($post->ID, get_post_meta( $post->ID, 'uxde' . 'review_box_pos', true ));  ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'enable_user_review', true ) == true): ?>

				<div class="uxde-user-review-criteria">

                	<span class="uxde-user-review-description"><b><span class="your_rating" style="display:none;"><?php _e('Your Rating', 'uxde'); ?></span><span class="user_rating"><?php _e('User Rating', 'uxde'); ?></span></b>: <span class="score"><?php echo $ratings->current_rating; ?></span> <em>(<span class="count"><?php echo $ratings->count; ?></span> <?php _e('Votes', 'uxde'); ?>)</em></span>
						
					<span class="uxde-user-review-rating">
						<span class="uxde-criteria-star-under"><span class="uxde-criteria-star-top" style="width:<?php echo $ratings->current_position; ?>%"></span></span>
					</span>

            	</div>

			<?php endif; ?>

			<?php endif; ?>

			<?php endif; ?>

			<?php the_content(); ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'enable_review', true ) == true) : ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'review_box_pos', true ) == 'bottom') : ?>
	
			<?php uxde_print_review_box($post->ID, get_post_meta( $post->ID, 'uxde' . 'review_box_pos', true ));  ?>

			<?php if(get_post_meta( $post->ID, 'uxde' . 'enable_user_review', true ) == true): ?>

				<div class="uxde-user-review-criteria">

                	<span class="uxde-user-review-description"><b><span class="your_rating" style="display:none;"><?php _e('Your Rating', 'uxde'); ?></span><span class="user_rating"><?php _e('User Rating', 'uxde'); ?></span></b>: <span class="score"><?php echo $ratings->current_rating; ?></span> <em>(<span class="count"><?php echo $ratings->count; ?></span> <?php _e('Votes', 'uxde'); ?>)</em></span>
						
					<span class="uxde-user-review-rating">
						<span class="uxde-criteria-star-under"><span class="uxde-criteria-star-top" style="width:<?php echo $ratings->current_position; ?>%"></span></span>
					</span>

            	</div>

			<?php endif; ?>

			<?php endif; ?>

			<?php endif; ?>

			<?php if($data['uxde_share']): ?>

			<div class="share-buttons">
					
				<ul>

					<li class="twitter-button"><a href="http://twitter.com/share" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>" class="twitter-share-button" data-count="horizontal" data-via="<?php if($data['uxde_twitter_username']): ?><?php echo $data['uxde_twitter_username']; ?><?php endif; ?>">Tweet</a></li>
					<li class="facebook-button"><iframe src="http://www.facebook.com/plugins/like.php?href=<?php the_permalink(); ?>&layout=button_count" scrolling="no" frameborder="0" style="height: 21px; width: 92px" allowTransparency="true"></iframe><div id="fb-root"></div></li>
					<li class="google-button"><g:plusone size="medium"></g:plusone></li>
					<li class="linkedin-button"><script src="//platform.linkedin.com/in.js" type="text/javascript"></script><script type="IN/Share"></script></li>
					<li class="pinterest-button"><a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php if (has_post_thumbnail( $post->ID ) ): ?><?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?><?php echo $image[0]; ?><?php endif; ?>&description=<?php the_title(); ?>" class="pin-it-button" count-layout="horizontal" always-show-count="true"><img border="0" src="http://assets.pinterest.com/images/PinExt.png" title="Pin It" /></a></li>
					<li class="stumbleupon-button"><su:badge layout="1"></su:badge></li>

						<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

						<script>(function(d, s, id) {
  						var js, fjs = d.getElementsByTagName(s)[0];
  						if (d.getElementById(id)) return;
  						js = d.createElement(s); js.id = id;
  						js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=143969905702939";
 						 fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
	
						<script type="text/javascript">
  						(function() {
    					var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    					po.src = 'https://apis.google.com/js/plusone.js';
   				 		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  						})();
						</script>

						<script type="text/javascript">
  						(function() {
    					var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
    					li.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//platform.stumbleupon.com/1/widgets.js';
    					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
  						})();
						</script>
						
				</ul>

			</div>

			<?php endif; ?>

			<div class="tags-nav">

				<div class="post-tags">

					<span><?php _e('Post Tags', 'uxde' ) ?></span> : <?php the_tags( '',', ','' ); ?>
		
				</div>

				<div class="post-nav-link">

					<div class="prev"><?php previous_post_link('%link'); ?></div>

					<div class="next"><?php next_post_link('%link'); ?></div>

				</div>

			</div>

		</div>

		<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_author_box', true ) == 'true') : ?>

		<div class="author-wrap">

			<div class="author-gravatar">

				<?php echo get_avatar( get_the_author_meta('email'), '100' ); ?>

			</div>
				
			<div class="author-info">

				<div class="vcard author author-title"><span class="fn"><?php the_author_link(); ?></span></div>

				<div class="author-description"><p><?php the_author_meta('description'); ?></p></div>

			</div>

		</div>

		<?php endif; ?>

		<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_author_box', true ) == '') : ?>

		<div class="author-wrap">

			<div class="author-gravatar">

				<?php echo get_avatar( get_the_author_meta('email'), '100' ); ?>

			</div>
				
			<div class="author-info">

				<div class="vcard author author-title"><span class="fn"><?php the_author_link(); ?></span></div>

				<div class="author-description"><p><?php the_author_meta('description'); ?></p></div>

			</div>

		</div>

		<?php endif; ?>

		<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_related_posts', true ) == 'true') : ?>

		<div class="related-posts">

			<h3><?php _e('Related Posts', 'uxde'); ?></h3>
	
			<ul>

				<?php $orig_post = $post;
    				global $post;
    				$categories = get_the_category($post->ID);
    				if ($categories) {
    				$category_ids = array();
    				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

    				$args=array(
    				'category__in' => $category_ids,
    				'post__not_in' => array($post->ID),
    				'posts_per_page'=> 3,
    				'orderby'=> 'rand',
    				'ignore_sticky_posts'=> 1
    				);

    				$my_query = new wp_query( $args );
    					if( $my_query->have_posts() ) {
    						while( $my_query->have_posts() ) { $my_query->the_post();?>

    							<li><article class="relate-post-column">
									
									<?php if ( has_post_thumbnail() ) : ?>

									<div class="featured-image">

										<a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_post_thumbnail('248x140-thumb'); ?></a>

									</div>

									<?php endif; ?>									
									
									<h4 class="related-article"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h4>
									
								</article></li>

    			<? } } } $post = $orig_post; wp_reset_query(); ?>

			</ul>

		</div>

		<?php endif; ?>

		<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_related_posts', true ) == '') : ?>

		<div class="related-posts">

			<h3><?php _e('Related Posts', 'uxde'); ?></h3>
	
			<ul>

			<?php $orig_post = $post;
    				global $post;
    				$categories = get_the_category($post->ID);
    				if ($categories) {
    				$category_ids = array();
    				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

    				$args=array(
    				'category__in' => $category_ids,
    				'post__not_in' => array($post->ID),
    				'posts_per_page'=> 3,
    				'orderby'=> 'rand',
    				'ignore_sticky_posts'=> 1
    				);

    				$my_query = new wp_query( $args );
    					if( $my_query->have_posts() ) {
    						while( $my_query->have_posts() ) { $my_query->the_post();?>

    							<li><article class="relate-post-column">
									
									<?php if ( has_post_thumbnail() ) : ?>

									<div class="featured-image">

										<a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_post_thumbnail('248x140-thumb'); ?></a>

									</div>

									<?php endif; ?>									
									
									<h4 class="related-article"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h4>
									
								</article></li>

    			<? } } } $post = $orig_post; wp_reset_query(); ?>

			</ul>

		</div>

		<?php endif; ?>
		
		<?php comments_template(); ?>
		
	</article>	

</div>
	
<?php endwhile; ?>

<?php endif; ?>