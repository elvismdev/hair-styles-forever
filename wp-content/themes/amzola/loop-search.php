<?php /* Start loop */ ?>

<?php while (have_posts()) : the_post(); ?>

	<div class="block">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="post-header">

			<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('250x220-thumb'); ?></a>

				</div>

			<?php endif; ?>

		</header>

		<div class="post-content">

			<h2 class="post-title entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>


			<div class="meta-info">

				<span class="vcard author"><span class="fn"><?php the_author_posts_link(); ?></span></span>
			
				<span> - <time class="updated meta-button" datetime="<?php the_time('F j, Y'); ?>" pubdate> <?php the_time('F j, Y'); ?></time></span>
			
				<span> - <?php comments_popup_link(__('0 Comments', 'uxde'), __('1 Comment', 'uxde'), '% '.__('Comments', 'uxde')); ?></span>

			</div>

			
			<div class="description">

				<p><?php echo limit_words(get_the_excerpt(), '30'); ?>...</p>

			</div>

			<a class="more-link" href="<?php the_permalink(); ?>"><?php _e('Read More', 'uxde') ?></a>

		</div>
		
	</article>	

</div>

<?php endwhile; ?>