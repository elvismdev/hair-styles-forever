<?php global $data; ?>

<?php 

global $wp_query;

function ux_category_top_parent_id ($catid) {
	while ($catid) {
		$cat = get_category($catid);
		$catid = $cat->category_parent;
		$catParent = $cat ->cat_ID;
	}
	return(isset($catParent)?$catParent:null);
}

if (is_single() ) {
	
	$category = get_the_category();
	$category_ID = $category[0] ->cat_ID;
	$category_parent = ux_category_top_parent_id ($category_ID);

	// Get Datas
	$category_color = get_tax_meta($category_ID, 'color_field_id');
	$category_background_type = get_tax_meta($category_ID, 'background_type');
	$category_background = get_tax_meta($category_ID, 'image_field_id');
	$category_background_color = get_tax_meta($category_ID, 'background_color');
	$category_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_background_position = get_tax_meta($category_ID, 'background_position');
	$category_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	//Get Parent Datas
	$category_parent_color = get_tax_meta($category_ID, 'color_field_id');
	$category_parent_background_type = get_tax_meta($category_ID, 'background_type');
	$category_parent_background = get_tax_meta($category_ID, 'image_field_id');
	$category_parent_background_color = get_tax_meta($category_ID, 'background_color');
	$category_parent_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_parent_background_position = get_tax_meta($category_ID, 'background_position');
	$category_parent_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	if (strlen($category_color) < 2) {
		$category_color = $category_parent_color;		
	}	

	if ($category_background_type == '') {
		$category_background_type = $category_parent_background_type;		
	}

	if ($category_background == '') {
		$category_background = $category_parent_background;		
	}

	if ($category_background_color == '') {
		$category_background_color = $category_parent_background_color;		
	}

	if ($category_background_repeat == '') {
		$category_background_repeat = $category_parent_background_repeat;		
	}

	if ($category_background_position == '') {
		$category_background_position = $category_parent_background_position;		
	}

	if ($category_background_attachment == '') {
		$category_background_attachment = $category_parent_background_attachment;		
	}


} 

else if (is_category() ) {
	
	$category_ID = get_query_var('cat');
	$category_parent = ux_category_top_parent_id ($category_ID);

	// Get Datas
	$category_color = get_tax_meta($category_ID, 'color_field_id');
	$category_background_type = get_tax_meta($category_ID, 'background_type');
	$category_background = get_tax_meta($category_ID, 'image_field_id');
	$category_background_color = get_tax_meta($category_ID, 'background_color');
	$category_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_background_position = get_tax_meta($category_ID, 'background_position');
	$category_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	//Get Parent Datas
	$category_parent_color = get_tax_meta($category_ID, 'color_field_id');
	$category_parent_background_type = get_tax_meta($category_ID, 'background_type');
	$category_parent_background = get_tax_meta($category_ID, 'image_field_id');
	$category_parent_background_color = get_tax_meta($category_ID, 'background_color');
	$category_parent_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_parent_background_position = get_tax_meta($category_ID, 'background_position');
	$category_parent_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	if (strlen($category_color) < 2) {
		$category_color = $category_parent_color;		
	}	

	if ($category_background_type == '') {
		$category_background_type = $category_parent_background_type;		
	}

	if ($category_background == '') {
		$category_background = $category_parent_background;		
	}

	if ($category_background_color == '') {
		$category_background_color = $category_parent_background_color;		
	}

	if ($category_background_repeat == '') {
		$category_background_repeat = $category_parent_background_repeat;		
	}

	if ($category_background_position == '') {
		$category_background_position = $category_parent_background_position;		
	}

	if ($category_background_attachment == '') {
		$category_background_attachment = $category_parent_background_attachment;		
	}


} ?>

<?php if ( is_single() || is_category() ) :?>

<style type="text/css">

	a, a:hover { color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	#primary-navigation, .widget .widget-tabs ul.drop, .block.loop-single .hentry .post-content .share-buttons { border-bottom: 5px solid <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	.uxde_posts_widget_home_1 ul li .meta-info a, .uxde_posts_widget_home_1 ul li .meta-info a:hover { color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	.uxde_posts_widget_half ul li a.more-link, .uxde_posts_widget_half_last ul li a.more-link, .uxde_posts_widget_home_3 ul li a.more-link, .block .hentry .post-content a.more-link { background-color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	.widget .widget-tabs ul.drop li.ui-tabs-active a, .widget .widget-tabs ul.drop li a:hover, .pagination a { background: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	.widget .review-badge { background: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?> ! important; }

	#review-box ul li .review-criteria-bar, #review-box .review-total-score .right { background-color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?> ! important; }

	.block.loop-single .hentry .post-content .meta-info a, .block.loop-single .hentry .post-content .meta-info a:hover,
	.block.loop-single .breadcrumbs a:hover, .post-box-archives .breadcrumbs a:hover, .widget a, #sidebar .widget_categories li a:hover, #sidebar .widget_archive li a:hover, 
	#sidebar .widget_meta li a:hover, #sidebar .widget_pages li a:hover, #sidebar .widget_nav_menu li a:hover, a.comment-reply-link {
		color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>;
	}

	.pagination a, .block.loop-single .hentry .related-posts .meta-category a, .block.loop-single .hentry .related-posts .meta-category a:hover, #respond #submit, 
	.uxde_newsletter_widget input[type="submit"], .widget_calendar table#wp-calendar thead > tr > th, .widget_calendar table > tbody > tr td#today, #sidebar .uxde_categories_widget li:hover span {
		background: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>;
	}

	.widget_calendar table > thead > tr { border: 1px solid <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; }

	#sidebar .widget_categories li a:hover, #sidebar .widget_archive li a:hover, #sidebar .widget_meta li a:hover, #sidebar .widget_pages li a:hover, #sidebar .widget_nav_menu li a:hover { 
		color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; 
	}

</style>

<?php if ($category_background_type === "css") : ?>

<style type="text/css">

	body {
		background-image: url("<?php if (!empty($category_background) ) : ?><?php echo $category_background['src']; ?><?php endif; ?>");
		background-color: <?php if (!empty($category_background_color) ) : ?><?php echo $category_background_color; ?><?php endif; ?>;
		background-repeat: <?php if (!empty($category_background_repeat) ) : ?><?php echo $category_background_repeat; ?><?php endif; ?>;
		background-position: <?php if (!empty($category_background_position) ) : ?><?php echo $category_background_position; ?><?php endif; ?>;
		background-attachment: <?php if (!empty($category_background_attachment) ) : ?><?php echo $category_background_attachment; ?><?php endif; ?>;
	}

</style>

<?php endif; ?>

<?php if (!empty($category_background) ) : ?>

<?php else : ?>

<?php if($data['uxde_background_type'] == 'CSS'): ?>

	<style type="text/css">

		body {
				background-image: url("<?php if($data['uxde_background_image']): ?><?php echo $data['uxde_background_image']; ?><?php endif; ?>");
				background-color: <?php if($data['uxde_background_color']): ?><?php echo $data['uxde_background_color']; ?><?php endif; ?>;
				background-repeat: <?php if($data['uxde_background_repeat']): ?><?php echo $data['uxde_background_repeat']; ?><?php endif; ?>;
				background-position: <?php if($data['uxde_background_position']): ?><?php echo $data['uxde_background_position']; ?><?php endif; ?>;
				background-attachment: <?php if($data['uxde_background_attachment']): ?><?php echo $data['uxde_background_attachment']; ?><?php endif; ?>;
			}

	</style>

<?php endif; ?>

<?php endif; ?>

<?php endif; ?>

