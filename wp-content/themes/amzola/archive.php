<?php get_header(); ?><?php global $data; ?>

	<div id="content" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?>>

		<?php /* If there are no posts to display, such as an empty archive page */ ?>

		<?php if (!have_posts()) : ?>
	
		<div class="post-box-archives">

			<?php echo ux_breadcrumbs(); ?>

			<h1 class="main-page-title"><?php _e('Sorry, no results were found.', 'uxde'); ?></h1>

		</div>

		<?php else: ?>

		<div class="post-box-archives">

			<div class="header-archives">

			<?php echo ux_breadcrumbs(); ?>

			<h1 class="main-page-title">

				<?php if (is_day()) : ?>

					<?php printf(__('Daily Archives: %s', 'uxde'), get_the_date()); ?>

				<?php elseif (is_month()) : ?>

					<?php printf(__('Monthly Archives: %s', 'uxde'), get_the_date('F Y')); ?>

				<?php elseif (is_year()) : ?>

					<?php printf(__('Yearly Archives: %s', 'uxde'), get_the_date('Y')); ?>

				<?php elseif (is_tag()) : ?>

					<?php echo __('Posts Tagged', 'uxde'); ?>: "<?php single_cat_title(); ?>"

				<?php elseif (is_author()) : ?>

						<?php 
						// Get author data
						if(get_query_var('author_name')) :
							$curauth = get_user_by('login', get_query_var('author_name'));
						else :
							$curauth = get_userdata(get_query_var('author'));
						endif;
						printf(__('Posts by: %s', 'uxde'), $curauth->display_name); 
						?>
						
				<?php else : ?>

					<?php _e('Category', 'uxde'); ?> "<?php single_cat_title(); ?>"
					
				<?php endif; ?>

			</h1>

			<?php if (is_day()) : ?>
				
			<?php elseif (is_month()) : ?>
				
			<?php elseif (is_year()) : ?>
					
			<?php elseif (is_tag()) : ?>
					
			<?php elseif (is_author()) : ?>

				<?php $curauth = (get_query_var('author_name')) ? get_user_by('slug', get_query_var('author_name')) : get_userdata(get_query_var('author')); ?>

				<div class="author-wrap">

					<div class="author-gravatar">

						<?php echo get_avatar( $curauth->user_email, '100' ); ?>

					</div>
				
					<div class="author-info">

						<div class="vcard author author-title"><span class="fn"><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->display_name; ?></a></span></div>

						<div class="author-description"><p><?php echo $curauth->description; ?></p></div>
	
					</div>

				</div>
				
			<?php else : ?>
					
				<div class="category-description"><?php echo category_description(); ?></div>

			<?php endif; ?>

			</div>

			<?php get_template_part('loop', 'archives'); ?>
		
		</div>

		<?php endif; ?>
			
		<?php if ($wp_query->max_num_pages > 1) : ?>
			
		<nav id="post-nav" class="post-nav-archives">

			<?php uxde_pagination(); ?>

		</nav>
				
		<?php endif; ?>

	</div>

	<aside id="sidebar" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?>>

		<?php get_sidebar(); ?>

	</aside>
		
<?php get_footer(); ?>