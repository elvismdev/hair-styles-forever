<?php get_header(); ?>

<?php $custom_post_layout_meta = get_post_meta(get_the_ID(), 'uxdecustom_post_layout_meta', true); ?>

		<div id="content" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>class="full-width" style="float: left; width: 100%;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?> style="float: left;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: right;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?><?php endif; ?>>
	
			<div class="post-box">

				<?php get_template_part('loop', 'page'); ?>

			</div>

		</div>
		
		<aside id="sidebar" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>style="display: none;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?>style="float: right;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: left;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?><?php endif; ?>>

			<?php get_sidebar(''); ?>

		</aside>
		
<?php get_footer(); ?>