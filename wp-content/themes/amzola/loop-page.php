<?php /* Start loop */ ?><?php global $data; ?>

<?php while (have_posts()) : the_post(); ?>

<div class="block loop-single">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="post-header">

			<?php echo ux_breadcrumbs(); ?>

			<h1 class="post-title entry-title"><?php the_title(); ?></h1>

		</header>

		<div class="post-content-single">

			<?php the_content(); ?>

			<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'uxde'), 'after' => '</p></nav>' )); ?>

		</div>

			
		<?php // comments_template(); ?>	

	</article>

</div>
	
<?php endwhile; // End the loop ?>