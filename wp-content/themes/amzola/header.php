<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head><?php global $data; ?>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
	<meta charset="<?php bloginfo('charset'); ?>">
	<?php if($data['uxde_responsive']): ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />		
	<?php endif; ?>

	<title><?php wp_title('|',true,'right'); ?><?php bloginfo('name'); ?></title>
	
	<!-- Included CSS Files -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

	<link href='http://fonts.googleapis.com/css?family=Raleway:300,400,500,600' rel='stylesheet' type='text/css'>
	
	<!-- IE Fix for HTML5 Tags -->
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	
	<!--[if (gte IE 6)&(lte IE 8)]>
	  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/selectivizr.js"></script>
	<![endif]-->

	<!-- Favicon and Feed -->
	<?php if($data['uxde_favicon']): ?>
	<link rel="shortcut icon" href="<?php echo $data['uxde_favicon']; ?>" type="image/x-icon" />
	<?php endif; ?>
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	
	<!--  iPhone Web App Home Screen Icon -->
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />

	<?php if ( is_home() ) : ?>

	<?php if($data['uxde_slider']): ?>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/metro.css">

	<?php endif; ?>

	<?php endif; ?>

	<?php if($data['uxde_custom_css']): ?>

	<style type="text/css">

		<?php echo $data['uxde_custom_css']; ?>

	</style>

	<?php endif; ?>

	<?php if ( is_single() || is_category() ) :?>

	<?php else : ?>

	<?php if($data['uxde_background_type'] == 'CSS'): ?>

	<style type="text/css">

		body {
				background-image: url("<?php if($data['uxde_background_image']): ?><?php echo $data['uxde_background_image']; ?><?php endif; ?>");
				background-color: <?php if($data['uxde_background_color']): ?><?php echo $data['uxde_background_color']; ?><?php endif; ?>;
				background-repeat: <?php if($data['uxde_background_repeat']): ?><?php echo $data['uxde_background_repeat']; ?><?php endif; ?>;
				background-position: <?php if($data['uxde_background_position']): ?><?php echo $data['uxde_background_position']; ?><?php endif; ?>;
				background-attachment: <?php if($data['uxde_background_attachment']): ?><?php echo $data['uxde_background_attachment']; ?><?php endif; ?>;
			}

	</style>

	<?php endif; ?>

	<?php endif; ?>

	<?php if ( is_single() || is_category() ) :?>

	<?php else : ?>

	<style type="text/css">

		a, a:hover { color: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		#primary-navigation, .widget .widget-tabs ul.drop { border-bottom: 5px solid <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		.uxde_posts_widget_home_1 ul li .meta-info a, .uxde_posts_widget_home_1 ul li .meta-info a:hover { color: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		.uxde_posts_widget_half ul li a.more-link, .uxde_posts_widget_half_last ul li a.more-link, .uxde_posts_widget_home_3 ul li a.more-link, .block .hentry .post-content a.more-link { background-color: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		#load-more a, .widget .widget-tabs ul.drop li.ui-tabs-active a, .widget .widget-tabs ul.drop li a:hover, .pagination a, .widget_calendar table#wp-calendar thead > tr > th, .widget_calendar table > tbody > tr td#today, 
		.block.loop-single .hentry .archive-lists ul li .widget-post .meta-category a, .block.loop-single .hentry .archive-lists ul li .widget-post .meta-category a:hover, .archive-lists .archives ul li:hover span, .archive-lists .categories ul li:hover span,
		.uxde_newsletter_widget input[type="submit"], #sidebar .uxde_categories_widget li:hover span { 
			background: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; 
		}

		.widget .review-badge { background: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?> ! important; }

		#sidebar .widget_categories li a:hover, #sidebar .widget_archive li a:hover, #sidebar .widget_meta li a:hover, #sidebar .widget_pages li a:hover, #sidebar .widget_nav_menu li a:hover { color: <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		.widget_calendar table > thead > tr { border: 1px solid <?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?>; }

		#sidebar .widget_categories li a:hover, #sidebar .widget_archive li a:hover, #sidebar .widget_meta li a:hover, #sidebar .widget_pages li a:hover, #sidebar .widget_nav_menu li a:hover { 
			color: <?php if (!empty($category_color) ) : ?><?php echo $category_color; ?><?php else : ?><?php if($data['uxde_main_color']): ?><?php echo $data['uxde_main_color']; ?><?php endif; ?><?php endif; ?>; 
		}

	</style>

	<?php endif; ?>

	<?php get_template_part('header-color'); ?>

	<?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>

<div class="body <?php if($data['uxde_layout_style'] == 'Boxed'): ?>boxed<?php else: ?>full<?php endif; ?>">

<div class="container">

<aside id="top"></aside>
	
<header id="header" class="wrapper">

	<div id="top-navigation">

	<div class="top-navigation-left">

		<nav id="top-menu">

			<?php /*
				Our navigation menu. If one isn't filled out, wp_nav_menu falls
					back to wp_page_menu. The menu assigned to the primary position is
				the one used. If none is assigned, the menu with the lowest ID is
				used. */
							
				wp_nav_menu( array(
				'theme_location' => 'top_navigation',
				'container' =>false,
				'menu_class' => 'top-menu',
				'echo' => true,
				'before' => '',
				'after' => '',
				'link_before' => '',
				'link_after' => '',
				'depth' => 3,
				'items_wrap' => '<ul class="top-menu">%3$s</ul>')
			); ?>
						
		</nav>

		<form action="/" class="top-form-mobile">

			<?php echo uxde_menu_mobile(); ?>

		</form>

		</div>

		<div class="top-navigation-right">

			<ul>
				<?php if($data['uxde_rss_feed']): ?><li class="rss"><a target="_blank" href="<?php echo $data['uxde_rss_feed']; ?>"><i class="icon-rss"></i></a></li><?php endif; ?>
				<?php if($data['uxde_twitter_username']): ?><li class="twitter"><a target="_blank" href="http://twitter.com/<?php echo $data['uxde_twitter_username']; ?>"><i class="icon-twitter"></i></a></li><?php endif; ?>
				<?php if($data['uxde_facebook_fanpage']): ?><li class="facebook"><a target="_blank" href="<?php echo $data['uxde_facebook_fanpage']; ?>"><i class="icon-facebook"></i></a></li><?php endif; ?>
				<?php if($data['uxde_google_plus_url']): ?><li class="google"><a target="_blank" href="<?php echo $data['uxde_google_plus_url']; ?>"><i class="icon-google-plus"></i></a></li><?php endif; ?>
				<?php if($data['uxde_pinterest']): ?><li class="pinterest"><a target="_blank" href="<?php echo $data['uxde_pinterest']; ?>"><i class="icon-pinterest"></i></a></li><?php endif; ?>
				<li class="search"><a class="search-button"><i class="icon-search"></i>Search</a></li>
			</ul>

			<div class="search-hide-form">

			 	<?php get_search_form( true ); ?> 
			 	<div class="bg"></div>

			</div>

		</div>

	</div>

	<div id="logo-ads">
					
	<div class="logo">

		<?php if($data['uxde_logo']): ?>
			
		<?php if(is_home() || is_front_page()) : ?>

			<h1 class="custom-logo">

				<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php if($data['uxde_logo']): ?><?php echo $data['uxde_logo']; ?><?php endif; ?>" width="<?php if($data['uxde_logo_width']): ?><?php echo $data['uxde_logo_width']; ?><?php endif; ?>" height="<?php if($data['uxde_logo_height']): ?><?php echo $data['uxde_logo_height']; ?><?php endif; ?>" /></a>

				<p><?php bloginfo('name'); ?></p>
							
			</h1>

		<?php else: ?>

			<div class="custom-logo">

				<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php if($data['uxde_logo']): ?><?php echo $data['uxde_logo']; ?><?php endif; ?>" width="<?php if($data['uxde_logo_width']): ?><?php echo $data['uxde_logo_width']; ?><?php endif; ?>" height="<?php if($data['uxde_logo_height']): ?><?php echo $data['uxde_logo_height']; ?><?php endif; ?>" /></a>

			</div>

		<?php endif; ?>

		<?php else: ?>

			<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>

			<p class="subheader"><?php bloginfo('description'); ?></p>

		<?php endif; ?>
						
	</div>

	<?php if($data['uxde_headerads']): ?>

	<div class="header-ads">

		<?php echo $data['uxde_headerads']; ?>

	</div>
	
	<?php endif; ?>

	</div>

	<div id="primary-navigation">

		<nav id="primary-menu">

			<?php /*
				Our navigation menu. If one isn't filled out, wp_nav_menu falls
					back to wp_page_menu. The menu assigned to the primary position is
				the one used. If none is assigned, the menu with the lowest ID is
				used. */
							
				wp_nav_menu( array(
				'theme_location' => 'primary_navigation',
				'container' =>false,
				'menu_class' => 'primary-menu',
				'echo' => true,
				'before' => '',
				'after' => '',
				'link_before' => '',
				'link_after' => '',
				'depth' => 3,
				'items_wrap' => '<ul class="primary-menu">%3$s</ul>')
			); ?>
						
		</nav>

		<form action="/" class="primary-form-mobile">

			<?php echo uxde_primary_menu_mobile(); ?>

		</form>

	</div>
					
</header>
		
<div id="main">

	<div class="wrapper">
