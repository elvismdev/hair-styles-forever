<?php get_header(); ?>

	<?php include (TEMPLATEPATH . '/lib/theme-functions/theme-metro.php'); ?>

	<div id="main-content">

		<div id="content" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?>>

		<?php if($data['uxde_homepage_style'] == 'Magazine'): ?>

		<?php dynamic_sidebar("Homepage Widgets"); ?>

		<?php if($data['uxde_page_load_more']): ?>

		<div id="load-more" class="clearfix">

			<a href="<?php echo $data['uxde_page_load_more']; ?>"><?php if($data['uxde_page_load_more_text']): ?><?php echo $data['uxde_page_load_more_text']; ?><?php endif; ?></a>

		</div>

		<?php endif; ?>

		<?php else: ?>

		<?php if (have_posts()) : ?>

		<div class="header-archives">

			<h3 class="main-page-title entry-title"><?php _e('Recent Posts at', 'uxde') ?> <?php bloginfo('name'); ?></h3>

		</div>

		<?php get_template_part('loop', 'news'); ?>

		<?php endif; ?>

		<?php if ($wp_query->max_num_pages > 1) : ?>
			
		<nav id="post-nav">

			<?php uxde_pagination(); ?>

		</nav>
				
		<?php endif; ?>

		<?php endif; ?>

		</div>

		<aside id="sidebar" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?>>

			<?php get_sidebar(); ?>

		</aside>

	</div>
		
<?php get_footer(); ?>