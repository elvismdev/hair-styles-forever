<?php global $data; ?>

<?php if($data['uxde_slider']): ?>

<div id="metro-box">

<?php if($data['uxde_slider_tags']): ?>

	<?php global $wp_query; $args = array( 'posts_per_page' => $data['uxde_slider_entries'], 'tag' => $data['uxde_slider_tags'], ); query_posts( $args ); ?>

	<?php if (have_posts()) : ?>

	<?php $count = 0; ?>
					
	<?php while (have_posts()) : the_post(); ?>

	<?php $count++; ?>

	<?php if ($count == 1) : ?>

	<div class="column">

		<div class="metro-item big">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('681x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '35'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 5) : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	</div>

	<?php elseif ($count == 6) : ?>

	<div class="column">

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 9) : ?>

		<div class="metro-item big">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('681x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '35'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div>

	<?php elseif ($count == 10) : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	</div>

	<?php elseif ($count == 11) : ?>

	<div class="column">

		<div class="metro-item big">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('681x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '35'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 15) : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	</div>

	<?php elseif ($count == 16) : ?>

	<div class="column">

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 19) : ?>

		<div class="metro-item big">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('681x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '35'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 20) : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	</div>

	<?php elseif ($count == 21) : ?>

	<div class="column">

		<div class="metro-item big">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('681x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '35'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php elseif ($count == 25) : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	</div>

	<?php else : ?>

		<div class="metro-item">
	
			<div class="featured-image">
			
				<a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?><?php the_post_thumbnail('338x220-thumb'); ?><?php endif; ?></a>
				
				<div class="metro-content color-<?php echo $count; ?>">

					<div class="metro-description">

						<span class="post-icon"></span>

						<div class="metro-header">

							<span class="meta-info"><time class="updated meta-button" datetime="<?php the_time('F j, Y g:i a'); ?>" pubdate> <?php the_time('F j, Y g:i a'); ?></time></span>
					
							<h2><?php the_title(); ?></h2>

						</div>
					
						<p class="description"><?php echo limit_words(get_the_excerpt(), '25'); ?>...</p>

					</div>

					<a class="link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
				
				</div>
			
			</div>

		</div> 

	<?php endif; ?>

	<?php endwhile; ?>

	<?php endif; ?>

	<?php wp_reset_query(); ?>

<?php else : ?>

	<p style="text-align: center;">Please setup Metro Tag(s) in Theme Options. You must setup tags that are used on active posts.</p>

<?php endif; ?>

</div>

<div class="clearfix"></div>

<?php endif; ?>