jQuery.noConflict();

/** Fire up jQuery - let's dance! 
 */
jQuery(document).ready(function($){

	//Masked Inputs (images as radio buttons)
	$('.of-radio-img-img').click(function(){
		$(this).parent().parent().find('.of-radio-img-img').removeClass('of-radio-img-selected');
		$(this).addClass('of-radio-img-selected');
	});

	$(".of-radio-img-img").click(function(){
        var parent = $(this).parents('span');
        $('.of-radio-img-img',parent).removeClass('of-radio-img-selected');
        $(this).addClass('of-radio-img-selected');
        $('.of-radio-img-radio',parent).attr('checked', true);
    });

	$('.of-radio-img-label').hide();
	$('.of-radio-img-img').show();
	$('.of-radio-img-radio').hide();

}); //end doc ready