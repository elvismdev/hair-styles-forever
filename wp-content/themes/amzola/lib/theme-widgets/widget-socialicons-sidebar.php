<?php

/*
 * Add function to widgets_init that'll load our widget.
 */
add_action( 'widgets_init', 'uxde_Socialicons_Widget_Sidebar' );

/*
 * Register widget.
 */
function uxde_Socialicons_Widget_Sidebar() {
	register_widget( 'uxde_Socialicons_Widget_Sidebar' );
}

/*
 * Widget class.
 */
class uxde_Socialicons_Widget_sidebar extends WP_Widget {

	/* ---------------------------- */
	/* -------- Widget setup -------- */
	/* ---------------------------- */
	
	function uxde_Socialicons_Widget_Sidebar() {
	
		/* Widget settings */
		$widget_ops = array( 'classname' => 'socialicons_widget_sidebar', 'description' => __('A widget that displays your social media icons ( Sidebar ).', 'uxde') );

		/* Widget control settings */
		$control_ops = array('id_base' => 'socialicons_widget_sidebar');

		/* Create the widget */
		$this->WP_Widget( 'socialicons_widget_sidebar', __('UXDE - Social Icons ( Sidebar )', 'uxde'), $widget_ops, $control_ops );
	}

	/* ---------------------------- */
	/* ------- Display Widget -------- */
	/* ---------------------------- */
	
	function widget( $args, $instance ) {
		extract( $args );

		/* Our variables from the widget settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$username = $instance['username'];
		$twitter = $instance['twitter'];
		$facebook = $instance['facebook'];
		$google = $instance['google'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Display the widget title if one was input (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
	   
	?>

	<ul>

        <li class="rss-icon"><a target="_blank" href="http://feeds.feedburner.com/<?php echo $username; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sidebar/rss.png" width="147" height="147" /><span><?php _e('Rss Feed', 'uxde') ?></span></a></li>
        
        <li class="twitter-icon"><a target="_blank" href="<?php echo "http://www.twitter.com/$twitter"; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sidebar/twitter.png" width="147" height="147" /><span><?php _e('Twitter', 'uxde') ?></span></a></li>

		<li class="facebook-icon"><a target="_blank" href="<?php echo "$facebook"; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sidebar/facebook.png" width="147" height="147" /><span><?php _e('Facebook', 'uxde') ?></span></a></li>

		<li class="google-icon"><a target="_blank" href="<?php echo "$google"; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/sidebar/google.png" width="147" height="147" /><span><?php _e('Google+', 'uxde') ?></span></a></li>

     </ul>
                        
        <?php

		/* After widget (defined by themes). */
		echo $after_widget;
	}

	/* ---------------------------- */
	/* ------- Update Widget -------- */
	/* ---------------------------- */
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags for title and name to remove HTML (important for text inputs). */
		$instance['title'] = strip_tags( $new_instance['title'] );

		/* No need to strip tags */
		$instance['username'] = $new_instance['username'];
		
		$instance['twitter'] = $new_instance['twitter'];

		$instance['facebook'] = $new_instance['facebook'];
	
		$instance['google'] = $new_instance['google'];

		return $instance;
	}
	
	/* ---------------------------- */
	/* ------- Widget Settings ------- */
	/* ---------------------------- */
	
	/**
	 * Displays the widget settings controls on the widget panel.
	 * Make use of the get_field_id() and get_field_name() function
	 * when creating your form elements. This handles the confusing stuff.
	 */
	
	function form( $instance ) {
	
		/* Set up some default widget settings. */
		$defaults = array(
		'title' => 'Social Media',
		'username' => "uxdedotnet",
		'twitter' => 'uxdedotnet',
		'facebook' => 'http://www.facebook.com/uxdedotnet/',	
		'google' => 'http://plus.google.com/',
		);
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<!-- Widget Title: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

		<!-- Ad image url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'username' ); ?>"><?php _e('RSS Acount:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'username' ); ?>" name="<?php echo $this->get_field_name( 'username' ); ?>" value="<?php echo $instance['username']; ?>" />
		</p>
		
		<!-- Ad twitter url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e('Twitter Acount:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo $instance['twitter']; ?>" />
		</p>

		<!-- Ad facebook url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e('Facebook URL:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo $instance['facebook']; ?>" />
		</p>

		<!-- Ad google plus url: Text Input -->
		<p>
			<label for="<?php echo $this->get_field_id( 'google' ); ?>"><?php _e('Google URL:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'google' ); ?>" name="<?php echo $this->get_field_name( 'google' ); ?>" value="<?php echo $instance['google']; ?>" />
		</p>

	<?php
	}
}
?>