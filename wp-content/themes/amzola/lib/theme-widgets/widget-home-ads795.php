<?php
add_action('widgets_init','uxde_795xxx_load_widgets');

function uxde_795xxx_load_widgets(){
		register_widget("UXDE_795xxx_Widget");
}

class UXDE_795xxx_Widget extends WP_widget{
	
	function UXDE_795xxx_Widget()
{
		$widget_ops = array('classname' => 'uxde_795xxx_widget', 'description' => __('A widget that displays your homepage ads block ( 795 x xx ).', 'uxde') );

		$control_ops = array( 'id_base' => 'uxde_795xxx_widget' );

		$this->WP_Widget('uxde_795xxx_widget', __('UXDE - Homepage - 795 x XX Ads', 'uxde'), $widget_ops, $control_ops);
		
	}
	
	function widget($args,$instance){
		extract($args);

		$link = $instance['link'];
		$image = $instance['image'];

		echo $before_widget;
		?>
			
			<div class="ads795xxx"><a href="<?php echo $link; ?>"><img src="<?php echo $image; ?>" alt="" width="795" /></a></div>

<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		
		$instance['link'] = $new_instance['link'];
		$instance['image'] = $new_instance['image'];
		
		return $instance;
	}
	
	function form($instance){
		$defaults = array('link' => '', 'image' => '',);
		$instance = wp_parse_args((array) $instance, $defaults); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e('Image URL:', 'uxde') ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id( 'image' ); ?>" name="<?php echo $this->get_field_name( 'image' ); ?>" value="<?php echo $instance['image']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e('Link URL:', 'uxde') ?></label>
			<input class="widefat" type="text" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name('link'); ?>" value="<?php echo $instance['link']; ?>" />
		</p>

		<?php

	}
}
?>