<?php
add_action('widgets_init', 'uxde_posts_load_widgets_home_gallery');

function uxde_posts_load_widgets_home_gallery()
{
	register_widget('UXDE_Posts_Widget_Home_Gallery');
}


/* ==  Widget ==============================*/

class UXDE_Posts_Widget_Home_Gallery extends WP_Widget {
	

/* ==  Widget Setup ==============================*/

	function UXDE_Posts_Widget_Home_Gallery()
	{
		$widget_ops = array('classname' => 'uxde_posts_widget_home_gallery', 'description' => __('A widget that displays your most recent posts on homepage ( gallery ).', 'uxde') );

		$control_ops = array('id_base' => 'uxde_posts_widget_home_gallery');

		$this->WP_Widget('uxde_posts_widget_home_gallery', __('UXDE - Homepage - Posts ( Gallery )', 'uxde'), $widget_ops, $control_ops);
	}
	

/* ==  Display Widget ==============================*/

	function widget($args, $instance)
	{
		extract($args);
		
		$categories = $instance['categories'];
		if( $categories != '' ) :
		$title = '<a href="'.get_category_link( $categories ).'">'. apply_filters('widget_title', $instance['title'] ) .'</a>';
		else :
		$title = apply_filters('widget_title', $instance['title'] );
		endif;
		if( $categories == 'all' ) :
		$title = apply_filters('widget_title', $instance['title'] );
		endif;
		$posts = $instance['posts'];
		
		echo $before_widget;
		?>

		<?php
		if($title) {
			echo $before_title.$title.$after_title;
		}
		?>
		
		<?php $recent_posts = new WP_Query(array('showposts' => $posts,'cat' => $categories,)); ?>

		<div class="homepage-style-gallery">

		<ul class="slides"> 

		<?php if ($recent_posts->have_posts()) : ?>

		<?php $count = 0; ?>
					
		<?php while($recent_posts->have_posts()): $recent_posts->the_post(); global $post; ?>

		<?php $count++; ?>

		<?php if ($count == 1) : ?>

		<li>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
	
			</article>

		<?php elseif ($count == 3) : ?>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
			
			</article>

		</li>	

		<?php elseif ($count == 4) : ?>

		<li>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
			
			</article>

		<?php elseif ($count == 6) : ?>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
			
			</article>

		</li>

		<?php elseif ($count == 7) : ?>

		<li>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
	
			</article>

		<?php elseif ($count == 9) : ?>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
	
			</article>

		</li>

		<?php else : ?>

			<article id="post-<?php the_ID(); ?>-recent" <?php post_class(); ?>>

				<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('255x230-thumb'); ?></a>

					<header>

						<h2 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		
					</header>

				</div>

				<?php endif; ?>
	
			</article>

		<?php endif; ?>	

		<?php endwhile; ?>

		<?php endif; ?>

		</ul>

		</div>

		<!-- END WIDGET -->
		<?php
		echo $after_widget;
	}
	
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['categories'] = $new_instance['categories'];
		$instance['posts'] = $new_instance['posts'];
		
		return $instance;
	}

	function form($instance)
	{
		$defaults = array('title' => 'Recent Posts', 'categories' => 'all', 'posts' => 4);
		$instance = wp_parse_args((array) $instance, $defaults); ?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'uxde') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id('categories'); ?>"><?php _e('Filter by Category:', 'uxde') ?></label> 
			<select id="<?php echo $this->get_field_id('categories'); ?>" name="<?php echo $this->get_field_name('categories'); ?>" class="widefat categories">
				<option value='all' <?php if ('all' == $instance['categories']) echo 'selected="selected"'; ?>>All categories</option>
				<?php $categories = get_categories('hide_empty=0&depth=1&type=post'); ?>
				<?php foreach($categories as $category) { ?>
				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['categories']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
				<?php } ?>
			</select>
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'posts' ); ?>"><?php _e('Number', 'uxde') ?></label>
			<select id="<?php echo $this->get_field_id( 'posts' ); ?>" name="<?php echo $this->get_field_name( 'posts' ); ?>" class="widefat">
				<option <?php if ( '3' == $instance['posts'] ) echo 'selected="selected"'; ?>>3</option>
				<option <?php if ( '6' == $instance['posts'] ) echo 'selected="selected"'; ?>>6</option>
				<option <?php if ( '9' == $instance['posts'] ) echo 'selected="selected"'; ?>>9</option>
			</select>
		</p>
		
	<?php 
	}
}
?>