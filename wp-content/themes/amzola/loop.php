<?php /* Start loop */ ?>

<?php if (have_posts()) : ?>

<?php $count = 0; ?>

<?php while (have_posts()) : the_post(); ?>

<?php $count++; ?>

<div class="block">

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="post-header">

			<?php if ( has_post_thumbnail() ) : ?>

				<div class="featured-image clearfix">

					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('block-thumb'); ?></a>

				</div>

			<?php endif; ?>

			<span class="meta-category color-<?php echo $count; ?>"><?php $category = get_the_category(); if ($category) { echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'uxde' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> '; } ?></span>

		</header>

		<div class="post-content-right">

			<h2 class="post-title entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>


			<div class="meta-info">

				<span class="vcard author"><span class="fn"><?php the_author_posts_link(); ?></span></span>
			
				<span> | <time class="updated meta-button" datetime="<?php the_time('F, jS Y'); ?>" pubdate> <?php the_time('F, jS Y'); ?></time></span>
			
				<span> | <?php comments_popup_link(__('0 Comments', 'uxde'), __('1 Comment', 'uxde'), '% '.__('Comments', 'uxde')); ?></span>

			</div>

			
			<div class="post-content-front">

				<p><?php echo limit_words(get_the_excerpt(), '30'); ?>...</p>

			</div>

		</div>
		
	</article>	

</div>

<?php endwhile; ?>

<?php endif; ?>