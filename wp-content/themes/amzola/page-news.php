<?php
/*
Template Name: News
*/
get_header(); ?>

<?php $custom_post_layout_meta = get_post_meta(get_the_ID(), 'uxdecustom_post_layout_meta', true); ?>

		<div id="content" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>class="full-width" style="float: left; width: 100%;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?> style="float: left;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: right;"<?php endif; ?>
						  <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?><?php endif; ?>>

			<?php global $wp_query; $args = array( 'posts_per_page' => 10, 'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1), ); query_posts( $args ); ?>

			<?php if (!have_posts()) : ?>

			<div class="post-box-archives">

				<div class="header-archives">

				<h1 class="main-page-title entry-title"><?php _e('Sorry, no results were found.', 'uxde'); ?></h1>

				</div>

			</div>

			<?php else: ?>
	
			<div class="post-box-archives">

				<div class="header-archives">

					<h1 class="main-page-title entry-title"><?php the_title(); ?></h1>

				</div>

				<?php get_template_part('loop', 'news'); ?>

			</div>

			<?php if ($wp_query->max_num_pages > 1) : ?>
			
				<nav id="post-nav" class="post-nav-archives">

					<?php uxde_pagination(); ?>

				</nav>
				
			<?php endif; ?>

			<?php endif; ?>

			<?php wp_reset_query(); ?>

		</div>

		<aside id="sidebar" <?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-full') : ?>style="display: none;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-left') : ?>style="float: right;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == 'content-right') : ?>style="float: left;"<?php endif;?>
							<?php if(get_post_meta( $post->ID, 'uxde' . 'custom_post_layout_meta', true ) == '') : ?><?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?><?php endif; ?>>

			<?php get_sidebar(''); ?>

		</aside>
		
<?php get_footer(); ?>