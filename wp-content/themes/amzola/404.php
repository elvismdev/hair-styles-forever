<?php get_header(); ?>

	<div id="content" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?>>
	
		<div class="post-box">

		<div class="block loop-single">

			<article id="post-404" class="post hentry">

				<div class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">

					<span typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="<?php echo home_url(); ?>"><?php _e('Homepage', 'uxde'); ?></a></span>

					<span class="sep">&raquo;</span>
						
					<span typeof="v:Breadcrumb"><span property="v:title" class="current"><?php _e('404 Page', 'uxde'); ?></span></span>

				</div>

				<h1><?php _e('Not Found, 404 Error.', 'uxde'); ?></h1>

				<div class="error">

					<p class="bottom"><?php _e('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable. Using the following search box to track down the page you were after:', 'uxde'); ?></p>

					<div class="error-search">
	
						<?php get_search_form( true ); ?>

					</div>
					
				</div>

				<div class="archive-lists">

					<div class="recent-post-list">

					<h2><?php _e('Recent Posts', 'uxde'); ?></h2>
						<?php query_posts( 'showposts=10' ); ?>
						<?php if ( have_posts() ) : ?>
						<ul>
							<?php while (have_posts()) : the_post(); ?>
							<li>

								<article id="post-<?php the_ID(); ?>-recent" class="widget-post">

									<?php if ( has_post_thumbnail() ) : ?>

										<div class="featured-image">

											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('sidebar-thumb'); ?></a>

										</div>

									<?php endif; ?>
	
								<header>
			
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

									<span class="meta-category"><?php $category = get_the_category(); if ($category) { echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'uxde' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> '; } ?></span>

								</header>
			
							</article>	

							</li>
							<?php endwhile; endif; ?>
							 <?php wp_reset_query(); ?> 
						</ul>

					</div>

					<div class="popular-post-list">

					<h2><?php _e('Popular Posts', 'uxde'); ?></h2>
						<?php $year = date('Y'); query_posts('post_type=post&posts_per_page=10&orderby=comment_count&order=DESC&year=' . $year . ''); ?>
						<?php if ( have_posts() ) : ?>
						<ul>
							<?php while (have_posts()) : the_post(); ?>
							<li>

								<article id="post-<?php the_ID(); ?>-recent" class="widget-post">

									<?php if ( has_post_thumbnail() ) : ?>

										<div class="featured-image">

											<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('sidebar-thumb'); ?></a>

										</div>

									<?php endif; ?>
	
								<header>
			
									<h4 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

									<span class="meta-category"><?php $category = get_the_category(); if ($category) { echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s", 'uxde' ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> '; } ?></span>

								</header>
			
							</article>	

							</li>
							<?php endwhile; endif; ?>
							 <?php wp_reset_query(); ?> 
						</ul>

					</div>

					<div class="clearfix"></div>

					<div class="archives">

					<h2><?php _e('Monthly Archives', 'uxde'); ?></h2>
						<ul class="archive-month">
							<?php
								$args = array (
								'echo' => 0,
								'show_option_all'    => '',
								'show_post_count' => 1,
								'title_li' => ''
								);
								$variable = wp_get_archives($args);
								$variable = str_replace ( "(" , "<span>", $variable );
								$variable = str_replace ( ")" , "</span>", $variable );
								echo $variable;
							?>
						</ul>		

					</div>

					<div class="categories">

					<h2><?php _e('Archives By Categories', 'uxde'); ?></h2>
						<ul class="archive-cat">
							<?php
								$args = array (
								'echo' => 0,
								'show_option_all'    => '',
								'orderby'            => 'name',
								'show_count' => 1,
								'title_li' => '',
								'exclude'  => '',
								'depth' => 1
								);
								$variable = wp_list_categories($args);
								$variable = str_replace ( "(" , "<span>", $variable );
								$variable = str_replace ( ")" , "</span>", $variable );
								echo $variable;
							?>
						</ul>

					</div>

				</div>

			</article>

		</div>

		</div>

	</div>

	<aside id="sidebar" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?>>
		
		<?php get_sidebar(); ?>

	</aside>
		
<?php get_footer(); ?>