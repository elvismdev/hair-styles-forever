</div>

</div>

</div>

<?php global $data; ?>

<footer id="footer">

	<div class="border"></div>

	<div class="widgets">

		<div class="widget-1">

			<?php dynamic_sidebar("Footer Widget 1"); ?>

			<div class="copyright">

				<p style="margin-bottom: 5px;"><?php if($data['uxde_footer_text']): ?><?php echo $data['uxde_footer_text']; ?><?php endif; ?></p>

				<p style="margin-bottom: 0;">Powered by Wordpress. Designed by <a href="http://www.uxde.net">UXDE dot Net</a>.</p>

			</div>

		</div>

		<div class="widget-2">

			<?php dynamic_sidebar("Footer Widget 2"); ?>

		</div>

		<div class="widget-3">

			<?php dynamic_sidebar("Footer Widget 3"); ?>

		</div>

		<div class="widget-4">

			<?php dynamic_sidebar("Footer Widget 4"); ?>

		</div>

		<div class="widget-5">

			<?php dynamic_sidebar("Footer Widget 5"); ?>

		</div>

	</div>

</footer>

</div>

<p id="back-top">

	<a href="#top">
		<span></span>
	</a>
	
</p>
					
<?php wp_footer(); ?>

<?php if($data['uxde_google_analytics']): ?><?php echo $data['uxde_google_analytics']; ?><?php endif; ?>

<?php if($data['uxde_slider']): ?>

<?php if ( is_home() ) : ?>

	<script typte="text/javascript">

		jQuery(document).ready(function($) {

			$("#metro-box").mCustomScrollbar({
					horizontalScroll:true,
					scrollButtons:{
						enable:true
					}
				});

		});

	</script>

<?php endif; ?>

<?php endif; ?>

<?php if($data['uxde_pagination_style'] == 'Infinite'): ?>

	<script typte="text/javascript">

		$('#content').infinitescroll({
 
    		navSelector  : "#post-nav",            
                
    		nextSelector : "#post-nav a.inactive",    
                
    		itemSelector : "#content .block",    

    		loading: {
          	finishedMsg: "<em><?php if($data['uxde_pagination_finish']): ?><?php echo $data['uxde_pagination_finish']; ?><?php endif; ?></em>",
          	msgText: "<em><?php if($data['uxde_pagination_text']): ?><?php echo $data['uxde_pagination_text']; ?><?php endif; ?></em>",
          	img: "http://www.uxde.net/test/amzola/wp-content/uploads/2013/04/ajax-loader.gif"
        	},      

		});

	</script>

<?php endif; ?>

<?php if ( is_single() || is_category() ) :?>

<?php get_template_part('footer-background'); ?>

<?php else: ?>

<?php if($data['uxde_background_type'] == 'Jquery'): ?>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		$.backstretch(["<?php if($data['uxde_background_image']): ?><?php echo $data['uxde_background_image']; ?><?php endif; ?>"]);

	});

</script>

<?php endif; ?>

<?php endif; ?>

</body>
</html>