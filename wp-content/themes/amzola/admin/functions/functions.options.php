<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories = array();  
		$of_categories_obj = get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp = array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages = array();
		$of_pages_obj = get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp = array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select = array("one","two","three","four","five"); 
		$of_options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" => "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$bg_images_path = STYLESHEETPATH. '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_bloginfo('template_url').'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}

		// Background Repeat
		$background_repeat = array("repeat" => "repeat", "repeat-x" => "repeat-x", "repeat-y" => "repeat-y", "no-repeat" => "no-repeat");

		// Background Position
		$background_position = array("top left" => "top left", "top center" => "top center", "top right" => "top right", "middle left" => "middle left", "middle center" => "middle center", "middle right" => "middle right", "bottom left" => "bottom left", "bottom center" => "bottom center", "bottom right" => "bottom right");

		// Background Attachment
		$background_attachment = array("scroll" => "scroll", "fixed" => "fixed");
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr = wp_upload_dir();
		$all_uploads_path = $uploads_arr['path'];
		$all_uploads = get_option('of_uploads');
		$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$metro_entries = array("Select a number:","10","15","20","25");
		$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 

		$layout = get_bloginfo('template_url').'/images/layout/';


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

$of_options[] = array( "name" => "General Settings",
					"type" => "heading");


$of_options[] = array( "name" => "Custom Logo",
					"desc" => "Upload an image that will represent your website's logo. You can also upload a retina logo here.",
					"id" => "uxde_logo",
					"std" => "",
					"type" => "media");

$of_options[] = array( "name" => "Custom Logo ( Width )",
					"desc" => "Enter the width of website's logo.",
					"id" => "uxde_logo_width",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Custom Logo ( Height )",
					"desc" => "Enter the height of website's logo.",
					"id" => "uxde_logo_height",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Homepage Style",
					"desc" => "Select your homepage style.",
					"id" => "uxde_homepage_style",
					"std" => "Magazine",
					"type" => "select", 	
					"options" => array('Magazine' => 'Magazine', 'Blog' => 'Blog',));

$of_options[] = array( "name" => "Mai Layout",
					"desc" => "Select main content and sidebar alignment.",
					"id" => "uxde_main_layout",
					"std" => "content-left",
					"type" => "images",
					"options" => array( 'content-left' => $layout .'2cr.png', 'content-right' => $layout . '2cl.png' ) );

$of_options[] = array( "name" => "Custom Favicon",
					"desc" => "Upload a 16px x 16px Png/Gif image that will represent your website's favicon.",
					"id" => "uxde_favicon",
					"std" => "",
					"type" => "upload");

$of_options[] = array( "name" => "Footer Text",
                    "desc" => "Enter your copyright text here. You can use HTML code for it.",
                    "id" => "uxde_footer_text",
                    "std" => "Copyright &copy; 2013 Codilight. All Rights Reserved.",
                    "type" => "textarea");   

                                                       
    



$of_options[] = array( "name" => "Metro Box",
					"type" => "heading");

$of_options[] = array( "name" => "Enable Slider",
					"desc" => "Turn ON/OFF the slider on the Homepage.",
					"id" => "uxde_slider",
					"std" => 1,
					"type" => "checkbox"); 

$of_options[] = array( "name" => "Slider Tag(s)",
					"desc" => "Add comma separated list for the tags that you would like to have displayed in the homepage slider.",
					"id" => "uxde_slider_tags",
					"std" => "",
					"type" => "text"); 

$of_options[] = array( "name" => "Slider Entries",
					"desc" => "Select the number of entries that should appear in the homepage slider.",
					"id" => "uxde_slider_entries",
					"std" => 15,
					"type" => "select",
					"options" => $metro_entries);






$of_options[] = array( "name" => "Styling Options",
					"type" => "heading");

$of_options[] = array( "name" => "Layout Style",
					"desc" => "Select your layout style.",
					"id" => "uxde_layout_style",
					"std" => "Boxed",
					"type" => "select", 	
					"options" => array('Boxed' => 'Boxed', 'Full' => 'Full',));

$of_options[] = array( "name" => "Responsive Design",
					"desc" => "Turn ON/OFF the responsive design layout.",
					"id" => "uxde_responsive",
					"std" => 1,
					"type" => "checkbox"); 

$of_options[] = array( "name" => "Pagination",
					"desc" => "Select your pagination style.",
					"id" => "uxde_pagination_style",
					"std" => "Infinite",
					"type" => "select", 	
					"options" => array('Infinite' => 'Infinite', 'Default' => 'Default',));

$of_options[] = array( "name" => "Loading Text for Pagination",
					"desc" => "Enter your loading text for pagination.",
					"id" => "uxde_pagination_text",
					"std" => "Loading the next set of posts...",
					"type" => "text");

$of_options[] = array( "name" => "Finish Text for Pagination",
					"desc" => "Enter your finish text for pagination.",
					"id" => "uxde_pagination_finish",
					"std" => "Congratulations, you've reached the end of the content.",
					"type" => "text");

$of_options[] = array( "name" => "Page URL - Load More Articles",
					"desc" => "Enter page URL for Load More Articles.",
					"id" => "uxde_page_load_more",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Load More Text",
					"desc" => "Enter your load more Text here.",
					"id" => "uxde_page_load_more_text",
					"std" => "Load More Articles",
					"type" => "text");

$of_options[] = array( "name" => "Background Type",
					"desc" => "",
					"id" => "uxde_background_type",
					"std" => "Jquery",
					"type" => "select",
					"options" => array('Jquery' => 'Jquery', 'CSS' => 'CSS',));

$of_options[] = array( "name" => "Background Image",
					"desc" => "Upload an image for your homepage background.",
					"id" => "uxde_background_image",
					"std" => "",
					"type" => "upload");

$of_options[] = array( "name" =>  "Background Color",
					"desc" => "Pick a background color for your homepage.",
					"id" => "uxde_background_color",
					"std" => "",
					"type" => "color");

$of_options[] = array( "name" => "Background Repeat",
					"desc" => ".",
					"id" => "uxde_background_repeat",
					"std" => "repeat",
					"type" => "select",
					"options" => array('repeat' => 'repeat', 'no-repeat' => 'no-repeat', 'repeat-x' => 'repeat-x', 'repeat-y' => 'repeat-y',));

$of_options[] = array( "name" => "Background Position",
					"desc" => ".",
					"id" => "uxde_background_position",
					"std" => "top center",
					"type" => "select",
					"options" => array('top left' => 'top left', 'top center' => 'top center', 'top right' => 'top right', 'middle left' => 'middle left', 'middle center' => 'middle center', 'middle right' => 'middle right', 'bottom left' => 'bottom left', 'bottom center' => 'bottom center', 'bottom right' => 'bottom right',));

$of_options[] = array( "name" => "Background Attachment",
					"desc" => ".",
					"id" => "uxde_background_attachment",
					"std" => "scroll",
					"type" => "select",
					"options" => array('scroll' => 'scroll', 'fixed' => 'fixed',));

$of_options[] = array( "name" => "Main Color",
					"desc" => "Pick the main color for your website.",
					"id" => "uxde_main_color",
					"std" => "#45a4f5",
					"type" => "color");

$of_options[] = array( "name" => "Custom CSS",
                    "desc" => "Quickly add some CSS to your theme by adding it to this block.",
                    "id" => "uxde_custom_css",
                    "std" => "",
                    "type" => "textarea");






//Social Share
$of_options[] = array( "name" => "Social Share",
					"type" => "heading");
          
$of_options[] = array( "name" => "Enable Social Share",
					"desc" => "Turn ON/OFF the social share buttons in the single post.",
					"id" => "uxde_share",
					"std" => 1,
					"type" => "checkbox");

$of_options[] = array( "name" => "RSS Feed",
					"desc" => "Enter your RSS Feed to display it on Menu Bar.",
					"id" => "uxde_rss_feed",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Twitter Username",
					"desc" => "Enter your Twitter Usename to display it on Menu Bar and for Social Share Buttons.",
					"id" => "uxde_twitter_username",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Facebook Fanpage URL",
					"desc" => "Enter your Facebook fanpage URL to display it on Menu Bar.",
					"id" => "uxde_facebook_fanpage",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Google Plus",
					"desc" => "Enter your Google Plus URL to display it on Menu Bar.",
					"id" => "uxde_google_plus_url",
					"std" => "",
					"type" => "text");

$of_options[] = array( "name" => "Pinterest URL",
					"desc" => "Enter your Pinterest URL to display it on Menu Bar.",
					"id" => "uxde_pinterest",
					"std" => "",
					"type" => "text");






//Advertise Settings
$of_options[] = array( "name" => "Advertisement",
					"type" => "heading");

$of_options[] = array( "name" => "Header Ads",
					"desc" => "Enter your HTML code for the header ads.",
					"id" => "uxde_headerads",
					"std" => "",
					"type" => "textarea"); 

					




// SEO Marketing
$of_options[] = array( "name" => "SEO Marketing",
					"type" => "heading");

$of_options[] = array( "name" => "Breadcrumbs",
					"desc" => "Turn ON/OFF the breadcrumbs.",
					"id" => "uxde_enable_breadcrumbs",
					"std" => 1,
					"type" => "checkbox"); 

$of_options[] = array( "name" => "Tracking Code",
					"desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
					"id" => "uxde_google_analytics",
					"std" => "",
					"type" => "textarea");

$of_options[] = array( "name" => "Yoast SEO Plugin",
					"desc" => "We highlight recommended you to use Yoast SEO plugin: http://yoast.com/wordpress/seo/ . It's the best SEO now and we're using it for our websites. It incorporates everything from a snippet preview and page analysis functionality that helps you optimize your robots, Google Authorship, pages content, images titles, meta descriptions and more to XML sitemaps, and loads of optimization options in between.",
					"id" => "uxde_seo_info",
					"std" => "",
					"type" => "info");






// Backup Options
$of_options[] = array( "name" => "Backup Options",
					"type" => "heading");
					
$of_options[] = array( "name" => "Backup and Restore Options",
                    "id" => "of_backup",
                    "std" => "",
                    "type" => "backup",
					"desc" => 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.',
					);
					
$of_options[] = array( "name" => "Transfer Theme Options Data",
                    "id" => "of_transfer",
                    "std" => "",
                    "type" => "transfer",
					"desc" => 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".
						',
					);
					
	}
}
?>
