<?php global $data; ?>

<?php 

global $wp_query;

function ux_category_footer_parent_id ($catid) {
	while ($catid) {
		$cat = get_category($catid);
		$catid = $cat->category_parent;
		$catParent = $cat ->cat_ID;
	}
	return(isset($catParent)?$catParent:null);
}

if (is_single()) {
	
	wp_reset_query();
	$category = get_the_category();
	$category_ID = $category[0] ->cat_ID;
	$category_parent = ux_category_footer_parent_id ($category_ID);

	// Get Datas
	$category_color = get_tax_meta($category_ID, 'color_field_id');
	$category_background_type = get_tax_meta($category_ID, 'background_type');
	$category_background = get_tax_meta($category_ID, 'image_field_id');
	$category_background_color = get_tax_meta($category_ID, 'background_color');
	$category_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_background_position = get_tax_meta($category_ID, 'background_position');
	$category_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	//Get Parent Datas
	$category_parent_color = get_tax_meta($category_ID, 'color_field_id');
	$category_parent_background_type = get_tax_meta($category_ID, 'background_type');
	$category_parent_background = get_tax_meta($category_ID, 'image_field_id');
	$category_parent_background_color = get_tax_meta($category_ID, 'background_color');
	$category_parent_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_parent_background_position = get_tax_meta($category_ID, 'background_position');
	$category_parent_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	if (strlen($category_color) < 2) {
		$category_color = $category_parent_color;		
	}	

	if ($category_background_type == '') {
		$category_background_type = $category_parent_background_type;		
	}

	if ($category_background == '') {
		$category_background = $category_parent_background;		
	}

	if ($category_background_color == '') {
		$category_background_color = $category_parent_background_color;		
	}

	if ($category_background_repeat == '') {
		$category_background_repeat = $category_parent_background_repeat;		
	}

	if ($category_background_position == '') {
		$category_background_position = $category_parent_background_position;		
	}

	if ($category_background_attachment == '') {
		$category_background_attachment = $category_parent_background_attachment;		
	}


} 

else if (is_category() ) {
	
	$category_ID = get_query_var('cat');
	$category_parent = ux_category_footer_parent_id ($category_ID);

	// Get Datas
	$category_color = get_tax_meta($category_ID, 'color_field_id');
	$category_background_type = get_tax_meta($category_ID, 'background_type');
	$category_background = get_tax_meta($category_ID, 'image_field_id');
	$category_background_color = get_tax_meta($category_ID, 'background_color');
	$category_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_background_position = get_tax_meta($category_ID, 'background_position');
	$category_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	//Get Parent Datas
	$category_parent_color = get_tax_meta($category_ID, 'color_field_id');
	$category_parent_background_type = get_tax_meta($category_ID, 'background_type');
	$category_parent_background = get_tax_meta($category_ID, 'image_field_id');
	$category_parent_background_color = get_tax_meta($category_ID, 'background_color');
	$category_parent_background_repeat = get_tax_meta($category_ID, 'background_repeat');
	$category_parent_background_position = get_tax_meta($category_ID, 'background_position');
	$category_parent_background_attachment = get_tax_meta($category_ID, 'background_attachment');

	if (strlen($category_color) < 2) {
		$category_color = $category_parent_color;		
	}	

	if ($category_background_type == '') {
		$category_background_type = $category_parent_background_type;		
	}

	if ($category_background == '') {
		$category_background = $category_parent_background;		
	}

	if ($category_background_color == '') {
		$category_background_color = $category_parent_background_color;		
	}

	if ($category_background_repeat == '') {
		$category_background_repeat = $category_parent_background_repeat;		
	}

	if ($category_background_position == '') {
		$category_background_position = $category_parent_background_position;		
	}

	if ($category_background_attachment == '') {
		$category_background_attachment = $category_parent_background_attachment;		
	}



} ?>

<?php if ( is_single() || is_category() ) :?>

<?php if ($category_background_type === "jquery") : ?>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		$.backstretch(["<?php if (!empty($category_background) ) : ?><?php echo $category_background['src']; ?><?php endif; ?>"]);

	});

</script>

<?php endif; ?>

<?php if (!empty($category_background) ) : ?>

<?php else : ?>

<?php if($data['uxde_background_type'] == 'Jquery'): ?>

<script type="text/javascript">

	jQuery(document).ready(function($) {

		$.backstretch(["<?php if($data['uxde_background_image']): ?><?php echo $data['uxde_background_image']; ?><?php endif; ?>"]);

	});

</script>

<?php endif; ?>

<?php endif; ?>

<?php endif; ?>