/*-----------------------------------------------------------------------------------*/
/*	Begin
/*-----------------------------------------------------------------------------------*/


jQuery(document).ready(function() {

	"use strict";

/*-----------------------------------------------------------------------------------*/
/*	Superfish Settings - http://users.tpg.com.au/j_birch/plugins/superfish/
/*-----------------------------------------------------------------------------------*/

	
	$('ul.top-menu').superfish({
		delay: 800,
		animation: { opacity: 'show', height:'show'},
		speed: 'fast',
		autoArrows: false,
		dropShadows: false
	});

	$('ul.primary-menu').superfish({
		delay: 800,
		animation: { opacity: 'show', height:'show'},
		speed: 'fast',
		autoArrows: false,
		dropShadows: false
	});

/*-----------------------------------------------------------------------------------*/
/*	Search Form
/*-----------------------------------------------------------------------------------*/

$('.search-button').click(function(){
    $('.search-hide-form').fadeIn(300, function(){
        $('.search-hide-form #searchform').fadeIn(100);
        $('.search-input').focus();
    });
    $('.search-hide-form .bg').click(function(){
        $('.search-hide-form #searchform').fadeOut(100, function(){
        	$('.search-hide-form').fadeOut(300);
    	});
    });
});

/*-----------------------------------------------------------------------------------*/
/*	Tab Shortcode
/*-----------------------------------------------------------------------------------*/
  
	$(".uxde-tabs").tabs();

/*-----------------------------------------------------------------------------------*/
/*	Toggles Shortcode
/*-----------------------------------------------------------------------------------*/
	
	$(".uxde-toggle").each( function () {
		if($(this).attr('data-id') === 'closed') {
			$(this).accordion({ header: '.uxde-toggle-title', collapsible: true, autoHeight: false, active: false  });
		} else {
			$(this).accordion({ header: '.uxde-toggle-title', collapsible: true, autoHeight: false});
		}
	});

/*-----------------------------------------------------------------------------------*/
/*	FitVids
/*-----------------------------------------------------------------------------------*/

	$(".body").fitVids();

/*-----------------------------------------------------------------------------------*/
/*	Tabbed Widget
/*-----------------------------------------------------------------------------------*/

	$(".widget-tabs").tabs({ fx: { opacity: 'show' } });
	$(".widget-tab li:last-child").addClass('last');
	
	$(".widget-tabs").tabs({ 
		fx: { opacity: 'toggle' } 
	});
	
	$(".toggle").each( function () {
		if(jQuery(this).attr('data-id') === 'closed') {
			jQuery(this).accordion({ header: 'h4', collapsible: true, active: false  });
		} else {
			jQuery(this).accordion({ header: 'h4', collapsible: true});
		}
	});

/*-----------------------------------------------------------------------------------*/
/*	Back Top
/*-----------------------------------------------------------------------------------*/

$("#back-top").hide();
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 400) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		$('#back-top').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 1000);
			return false;
	});
}); 

/*-----------------------------------------------------------------------------------*/
/*	FlexSlider
/*-----------------------------------------------------------------------------------*/

	$('.homepage-style-1').flexslider({
    	animation: "slide",
    	slideshow: false
  	});

  	$('.homepage-style-2').flexslider({
    	animation: "slide",
    	slideshow: false
  	});

  	$('.homepage-style-3').flexslider({
    	animation: "slide",
    	slideshow: false
  	});

  	$('.homepage-style-half').flexslider({
    	animation: "slide",
    	slideshow: false
  	});

  	$('.homepage-style-gallery').flexslider({
    	animation: "slide",
    	slideshow: false
  	});

/*-----------------------------------------------------------------------------------*/
/*	The End
/*-----------------------------------------------------------------------------------*/	

});