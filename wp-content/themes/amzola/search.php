<?php get_header(); ?><?php global $data; ?>

	<div id="content" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: left"<?php else :?>style="float: right;"<?php endif;?>>

		<?php /* If there are no posts to display, such as an empty archive page */ ?>

		<?php if (!have_posts()) : ?>
	
		<div class="post-box-archives">
			
			<?php echo ux_breadcrumbs(); ?>

			<h1 class="main-page-title"><?php _e('Search Results for', 'uxde'); ?> "<?php echo get_search_query(); ?>"</h1>

			<p><?php _e('Sorry, no results were found. Please try again with another keyword.', 'uxde'); ?></p>

		</div>

		<?php else: ?>
			
		<div class="post-box-archives">

			<div class="header-archives">

				<?php echo ux_breadcrumbs(); ?>

				<h1 class="main-page-title"><?php _e('Search Results for', 'uxde'); ?> "<?php echo get_search_query(); ?>"</h1>

			</div>

			<?php get_template_part('loop', 'search'); ?>

		</div>

		<?php endif; ?>
			
		<?php if ($wp_query->max_num_pages > 1) : ?>
			
		<nav id="post-nav" class="post-nav-archives">

			<?php uxde_pagination(); ?>

		</nav>
				
		<?php endif; ?>

	</div>

	<aside id="sidebar" <?php if($data['uxde_main_layout'] == 'content-left'): ?>style="float: right"<?php else :?>style="float: left;"<?php endif;?>>

		<?php get_sidebar(); ?>

	</aside>
		
<?php get_footer(); ?>