<!DOCTYPE>
<html <?php language_attributes(); ?>>
<head>

<title><?php
global $page, $paged;
wp_title( '|', true, 'right' );
bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
echo " | $site_description";
if ( $paged >= 2 || $page >= 2 )
echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
?></title>


<?php include (TEMPLATEPATH . '/meta.php'); ?>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="Shortcut Icon" href="<?php bloginfo('stylesheet_directory');?>/gbr/fav.png" type="image/x-icon" />

<?php
if ( is_singular() && get_option( 'thread_comments' ) )
wp_enqueue_script( 'comment-reply' );
wp_head();
?>

<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory');?>/js/stepcarousel.js"></script>
<script type="text/javascript">
stepcarousel.setup({
galleryid: 'mygallery',
beltclass: 'belt',
panelclass: 'panel',
autostep: {enable:true, moveby:1, pause:3000},
panelbehavior: {speed:500, wraparound:false, wrapbehavior:'slide', persist:true},
defaultbuttons: {enable: true, moveby: 1, leftnav: ['http://waallpapers.com/wp-content/themes/Poseidon/gbr/prev.png', -49, -10], rightnav: ['http://waallpapers.com/wp-content/themes/Poseidon/gbr/next.png', 10, -10]},
statusvars: ['statusA', 'statusB', 'statusC'],
contenttype: ['inline'] 
})
</script>


</head>

<body>

	<div class="kepala">
		<div class="wadah">
		<?php if (is_home()) { ?>
			<div class="logo">
				<h1>
					<img src="<?php bloginfo('template_directory'); ?>/gbr/logo.png"/>
				</h1>
			</div>
			<?php } else { ?>
			<div class="logo">
					<img src="<?php bloginfo('template_directory'); ?>/gbr/logo.png"/>
			</div>
			<?php } ?>
			<div class="sosial">
			Follow us on:
				<a href="#" class="akun fb">Facebook</a>
				<a href="#" rel="publisher" class="akun gp">Google+</a>
				<a href="#" class="akun tw">Twitter</a>
				<a href="#" class="akun pt">Pinterest</a>
			</div>
		</div>
		
		<div class="fix"></div>
	</div>
	
	<div class="menuatas">
		<div class="wadah">
			<div class="menu">
				<div class="menusirah">
					<ul class="menu">
						<li>
							<a href="<?php bloginfo('url'); ?>" class="hm">Home</a>
							<a href="<?php bloginfo('url'); ?>/trending" class="tr">Trending</a>
							<a href="<?php bloginfo('url'); ?>/random" class="br">Browse</a>
							<a href="<?php bloginfo('url'); ?>" class="fr">Fresh</a>
						</li>
					</ul>
				</div>
				<div class="fix"></div>
			</div>
			<div class="cari">
				<div class="formulir">
					<form method="get" action="<?php echo home_url( '/' ); ?>">
						<input type="text" class="input" name="s" id="s" placeholder="Search...">
						
					</form>
				</div>
			</div>
			<div class="fix"></div>
		</div>
	</div>
	<div class="fix"></div>
	
<?php include (TEMPLATEPATH . '/leher.php'); ?>