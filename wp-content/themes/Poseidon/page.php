<?php get_header(); ?>

	<div class="wadah">
	
		<div class="badan">

		<?php get_sidebar(); ?>
		
			<div class="konten laman">
			
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
						<h1><?php the_title (''); ?></h1>
						<?php the_content (''); ?>
					<?php endwhile; ?>        
				<?php else : ?>
				<h2>Not Found</h2>
				<p>Sorry, but you are looking for something that isn't here.</p>
				<?php endif; ?>
				
				<div class="fix"></div>
				<?php kriesi_pagination($additional_loop->max_num_pages); ?>
			

			</div>
			
		</div>
	</div>
	
<?php get_footer(); ?>