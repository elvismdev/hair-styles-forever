<div xmlns:v="http://rdf.data-vocabulary.org/#" id="breadcrumbs">
<?php if(is_home()) { ?>
<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span><?php bloginfo('description'); ?></span>
<?php } else if(is_attachment()) { ?><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php echo get_permalink($post->post_parent); ?>" ><?php echo get_the_title($post->post_parent); ?></a></span> &rsaquo; <span><?php single_post_title(); ?></span><?php } else if(is_single() && !is_attachment()) { ?><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <?php $category = get_the_category(); if($category[0]) { echo '<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="'.get_category_link($category[0]->term_id ).'">'.$category[0]->cat_name.'</a></span>'; } ?>  &rsaquo; <span><?php single_post_title(); ?></span><?php } else if(is_category()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span><?php single_cat_title(); ?></span>

<?php } else if (is_tag()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; </span><span><?php single_tag_title(); ?></span>

<?php } else if (is_year()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Archive</a> &rsaquo; </span><span><?php the_time('Y'); ?></span>

<?php } else if (is_month()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Archive</a> &rsaquo; </span><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>/<?php the_time('Y'); ?>"><?php the_time('Y'); ?></a></span> &rsaquo; <span><?php the_time('F'); ?></span>

<?php } else if (is_day()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Archive</a> &rsaquo; </span><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>/<?php the_time('Y'); ?>"><?php the_time('Y'); ?></a></span> &rsaquo; <span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>/<?php the_time('Y'); ?>/<?php the_time('m'); ?>"><?php the_time('F'); ?></a></span> &rsaquo; <span><?php the_time('j'); ?></span>

<?php } else if (is_page()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Page</a> &rsaquo; </span><span><?php single_post_title(); ?></span>

<?php } else if (is_author()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Archive</a> &rsaquo; </span><span><?php global $author; $userdata = get_userdata($author); echo $userdata->display_name; ?></span>

<?php } else if (is_search()) { ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb">Search Result &rsaquo; </span><span style="text-transform:capitalize;"><?php the_search_query(); ?></span>

<?php } else if(is_404()): ?>

<span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>">Home</a></span> &rsaquo; <span class="hidden" typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="<?php bloginfo('url'); ?>/sitemap">Archive</a> &rsaquo; </span><span>Not Found</span>

<?php endif; ?>
</div>
