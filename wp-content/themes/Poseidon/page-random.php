<?php
/**
 * Template Name: Browser Random Post
 */
?>


<?php get_header(); ?>

	<div class="wadah">
	
		<div class="badan">

		<?php get_sidebar(); ?>
		
			<div class="konten">
			
				<div class="iklan728">
					<div class="iklan728isi">
						
					</div>
				</div>
			
				<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args= array(
					'orderby' => 'rand',
					'posts_per_page' => 21,
					'paged' => $paged
					);
					query_posts($args);
					if( have_posts() ) :
				?>
				<?php while ( have_posts() ) : the_post(); ?>                                                              
				<?php include (TEMPLATEPATH . '/gambarcilik.php'); ?>
				<?php endwhile; ?>        
				<?php endif; ?>
				
				<div class="fix"></div>
				<?php kriesi_pagination($additional_loop->max_num_pages); ?>
			

			</div>
			
		</div>
	</div>
	
<?php get_footer(); ?>