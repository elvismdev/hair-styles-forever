<?php
/**
 * Template Name: Trending Last Week
 */
?>

<?php get_header(); ?>

	<div class="wadah">
	
		<div class="badan">

		<?php get_sidebar(); ?>
		
			<div class="konten">
			
				<div class="iklan728">
					<div class="iklan728isi">
						
					</div>
				</div>
			
				<?php
					$args= array(
					'meta_key' => 'post_views_count',
					'orderby' => 'meta_value_num',
					'posts_per_page' => 21,
					'year' => date('Y'),
					'w' => date('W'),
					);
					query_posts($args);
					if( have_posts() ) :
				?>
				<?php while ( have_posts() ) : the_post(); ?>                                                              
				<?php include (TEMPLATEPATH . '/gambarcilik.php'); ?>
				<?php endwhile; ?>        
				<?php else : ?>
				<?php $lastposts = get_posts('numberposts=20&orderby=rand');foreach($lastposts as $post):setup_postdata($post);?>
				<?php include (TEMPLATEPATH . '/gambarcilik.php'); ?>
				<?php endforeach; ?>
				<?php endif; ?>
				
				<div class="fix"></div>
				<?php kriesi_pagination($additional_loop->max_num_pages); ?>
			

			</div>
			
		</div>
	</div>
	
<?php get_footer(); ?>