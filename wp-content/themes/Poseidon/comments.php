<?php
if (!function_exists("custom_comment")) {
function custom_comment($comment, $args, $depth) {
 $GLOBALS['comment'] = $comment; ?>
<li <?php comment_class(); ?>>
<a name="comment-<?php comment_ID() ?>"></a>
<div id="li-comment-<?php comment_ID() ?>" class="comment-container">
<div class="comment-head">
</div>
<div class="comment-entry"id="comment-<?php comment_ID(); ?>">
<span class="arrow"></span>
<div class="comment-info">
<div class="left"><span class="name"><?php the_commenter_link() ?></span></div>
<div class="right">
<span class="date"><?php echo get_comment_date(get_option( 'date_format' )) ?> <?php _e('at', 'wallpaperday'); ?> <?php echo get_comment_time(get_option( 'time_format' )); ?></span>
<span class="edit"><?php edit_comment_link(__('Edit', 'wallpaperday'), '', ''); ?></span>
</div>
<div class="clear"></div>
</div><div class="clear"></div>
<?php //comment_text() ?>
<?php if ($comment->comment_approved == '0') { ?>
<p class='unapproved'>
<span class="deskriplap1">
<?php comments_number(__('You have reported', 'wallpaperday'), __('One Report', 'wallpaperday'), __('% Report', 'wallpaperday') );?> <?php _e('', 'wallpaperday') ?> &#8220;<?php the_title(); ?>&#8221; -
<?php _e('Your report is awaiting moderation.', 'wallpaperday'); ?>
</span>
<span class="deskriplap2">
By submitting this content you are granting to WallpaperDay the right to use it to improve our services. If you submit feedback or reports about our Services, we may use your feedback or reports without obligation to you. Thank you for your feedback or reports. We value every piece of feedback and reports we receive. We cannot respond individually to every one, but we will use your post as we strive to improve WallpaperDay services.
</span>
</p>
<?php } ?>
</div>
</div>
<?php 
}
}

// PINGBACK / TRACKBACK OUTPUT
if (!function_exists("list_pings")) {
function list_pings($comment, $args, $depth) {

$GLOBALS['comment'] = $comment; ?>
<li id="comment-<?php comment_ID(); ?>">
<span class="author"><?php comment_author_link(); ?></span> -
<span class="date"><?php echo get_comment_date(get_option( 'date_format' )) ?></span>
<span class="pingcontent"><?php comment_text() ?></span>
<?php 
} 
}

if (!function_exists("the_commenter_link")) {
function the_commenter_link() {
$commenter = get_comment_author_link();
if ( ereg( ']* class=[^>]+>', $commenter ) ) {$commenter = ereg_replace( '(]* class=[\'"]?)', '\\1url ' , $commenter );
} else { $commenter = ereg_replace( '(<a )/', '\\1class="url "' , $commenter );}
echo $commenter ;
}
}

?>
<?php

// Do not delete these lines

if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) { ?>
<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.', 'wallpaperday') ?></p>
<?php return; } ?>
<?php $comments_by_type = &separate_comments($comments); ?>
<div id="comments">
<?php if ( have_comments() ) : ?>
<?php if ( ! empty($comments_by_type['comment']) ) : ?>
<ol class="commentlist">
<?php wp_list_comments('avatar_size=35&callback=custom_comment&type=comment'); ?>
</ol>
<div class="navigation">
<div class="left"><?php previous_comments_link() ?></div>
<div class="right"><?php next_comments_link() ?></div>
<div class="clear"></div>
</div>
<?php endif; ?>
<?php if ( ! empty($comments_by_type['pings']) ) : ?>
<h3 id="pings"><?php _e('Trackbacks/Pingbacks', 'wallpaperday') ?></h3>
<ol class="pinglist">
<?php wp_list_comments('type=pings&callback=list_pings'); ?>
</ol>
<?php endif; ?>
<?php else : // ini ditampilkan jika tidak ada komentar sejauh ini ?>
<?php if ('open' == $post->comment_status) : ?>
<p class="nocomments"><?php _e(' ', 'wallpaperday') ?></p>
<?php else : // Komentar ditutup ?>
<p class="nocomments"><?php _e('Comments are closed.', 'wallpaperday') ?></p>
<?php endif; ?>
<?php endif; ?>
</div>
<?php if ('open' == $post->comment_status) : ?>
<div class="fix"></div>
<div id="respond">
<h3><?php comment_form_title( __('Report this image', 'wallpaperday'), __('Report %s', 'wallpaperday') ); ?></h3>
<?php if ( get_option('comment_registration') && !$user_ID ) : //Jika pendaftaran diperlukan & belum login ?>
<p><?php _e('You must be', 'wallpaperday') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in', 'wallpaperday') ?></a> <?php _e('to post a comment.', 'wallpaperday') ?></p>
<?php else : //Tidak ada pendaftaran diperlukan ?>
<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
<?php if ( $user_ID ) : //Jika pengguna login ?>
<p><?php _e('Logged in as', 'wallpaperday') ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(); ?>" title="<?php _e('Log out of this account', 'wallpaperday') ?>"><?php _e('Logout', 'wallpaperday') ?> &raquo;</a></p>
<?php else : //Jika pengguna tidak login ?>
<p>
<label for="url"><?php _e('Subject', 'wallpaperday') ?> :</label>
<input type="text" name="url" class="txt" id="url" value="<?php the_title('Subject: ',''); ?>" size="22" tabindex="3" style="margin-left:0"/>
</p>
<p>
<label for="author"><?php _e('Your Name', 'wallpaperday') ?> <?php if ($req) ?> (<?php _e('Required', 'wallpaperday'); ?>) <?php ; ?> :</label>
<input type="text" name="author" class="txt" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" placeholder="Your Name"/>
</p>
<p>
<label for="email"><?php _e('Your Mail, will not be published', 'wallpaperday') ?> <?php if ($req) ?> (<?php _e('Required', 'wallpaperday'); ?>) <?php ; ?> :</label>
<input type="text" name="email" class="txt" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" placeholder="Your Mail"/>
</p>
<?php endif; // Akhir jika login ?>
<p><textarea name="comment" id="comment" rows="10" cols="50" tabindex="4"></textarea></p>
<input name="submit" type="submit" id="submit" class="button" style="float:right" tabindex="5" value="<?php _e('Send Report', 'wallpaperday') ?>" />
<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
<?php comment_id_fields(); ?>
<?php do_action('comment_form', $post->ID); ?>
</form>
<?php endif; // Jika pendaftaran diperlukan ?>
<div class="fix"></div>
</div>
<div class="fix"></div>
<?php endif; // jika Anda menghapus ini langit akan runtuh di kepala Anda :D ?>