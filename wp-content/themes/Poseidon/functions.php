<?php
register_nav_menus( array(
	'Atas' => 'Menu Navigasi Header',
	'Bawah' => 'Menu Navigasi footer'
) );

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
add_theme_support( 'post-thumbnails' );
add_image_size( 'homepage-thumb', 220, 150, true);
update_option('thumbnail_size_w', 205);
update_option('thumbnail_size_h', 130);
update_option('thumbnail_crop', 1);
}

function kriesi_pagination($pages = '', $range = 30)
{  
$showitems = ($range * 2)+1;  
global $paged;
if(empty($paged)) $paged = 1;
if($pages == '')
{
global $wp_query;
$pages = $wp_query->max_num_pages;
if(!$pages)
{
$pages = 1;
}
}   
if(1 != $pages)
{
echo "<div class='pagination'>";
if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";
for ($i=1; $i <= $pages; $i++)
{
if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
{
echo ($paged == $i)? "<span class='current'>".'&#8226;'."</span>":"<a href='".get_pagenum_link($i)."' title='Page $i' class='inactive' >".'&#8226;'."</a>";
}
}
if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
echo "</div>\n";
}
}

function dp_attachment_image_link($postid=0, $size='full', $attributes='') {
if ($postid<1) $postid = get_the_ID();
if ($images = get_children(array(
'post_parent' => $postid,
'post_type' => 'attachment',
'numberposts' => 1,
'post_mime_type' => 'image',)))
foreach($images as $image) {
$attachment=wp_get_attachment_image_src($image->ID, $size);
echo $attachment[0] ;
}
}

function attachment_size() {
global $post;
$attachments = get_children(array('post_parent'=>$post->ID));
$nbImg = count($attachments);
if ( $images = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => 1,
'post_mime_type' => 'image',)))
{
foreach( $images as $image ) {
$metadata = wp_get_attachment_metadata($image->ID);
$width = $metadata['width'];
$height = $metadata['height'];
echo $width.'&times;'.$height;
}
} else {
echo "Unknown Resolutions";
}
}

function attfilesize() {
global $post;
$attachments = get_children(array('post_parent'=>$post->ID));
$nbImg = count($attachments);
if ( $images = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => 1,
'post_mime_type' => 'image',)))
{
foreach( $images as $image ) {
$myfile = filesize( get_attached_file( $image->ID ) );
$docsize = size_format($myfile);
echo $docsize;
}
} else {
echo "Unknown Size";
}
}

function excerpt($limit) {
$excerpt = explode(' ', get_the_excerpt(), $limit);
if (count($excerpt)>=$limit) {
array_pop($excerpt);
$excerpt = implode(" ",$excerpt).'';
} else {
$excerpt = implode(" ",$excerpt);
}
$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
return $excerpt;
}
 
function content($limit) {
$content = explode(' ', get_the_content(), $limit);
if (count($content)>=$limit) {
array_pop($content);
$content = implode(" ",$content).'...';
} else {
$content = implode(" ",$content);
}
$content = preg_replace('/\[.+\]/','', $content);
$content = apply_filters('the_content', $content); 
$content = str_replace(']]>', ']]&gt;', $content);
return $content;
}

//views
function tampilandilihatbanyak($post_ID){$count_key='post_views_count';$count=get_post_meta($post_ID,$count_key,true);if($count==''){$count=0;delete_post_meta($post_ID,$count_key);add_post_meta($post_ID,$count_key,'0');return $count.' View';}else{$count++;update_post_meta($post_ID,$count_key,$count);if($count=='1'){return $count.' Views';}
else{return $count.' ';}}}

function inforiatif_get_thumb($postid=0, $size='homepage-thumb') {
if ($postid<1) 
$postid = get_the_ID();
$thumb = get_post_meta($postid, "thumb", TRUE);
if ($thumb != ''){
echo $thumb;
} 
elseif ($images = get_children(array( 
'post_parent' => $postid,
'post_type' => 'attachment',
'numberposts' => '1',
'orderby' => 'menu_order',
'order' => 'ASC',
'post_mime_type' => 'image', )))
foreach($images as $image) {
$thumbnail=wp_get_attachment_image_src($image->ID, $size); 
echo $thumbnail[0]; }

else {
echo get_bloginfo ( 'template_directory' );
echo '/images/thumbnail.jpg';
}
}

function feedsize() {
global $post;
$attachments = get_children(array('post_parent'=>$post->ID));
$nbImg = count($attachments);
if ( $images = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => 1,
'post_mime_type' => 'image',)))
{
foreach( $images as $image ) {
$myfile = filesize( get_attached_file( $image->ID ) );
$docsize = size_format($myfile);
echo ', <br/>Image Size: '.$docsize;
}
} else {
echo "Unknown Size";
}
}
function feedattress() {
global $post;
$attachments = get_children(array('post_parent'=>$post->ID));
$nbImg = count($attachments);
if ( $images = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => 1,
'post_mime_type' => 'image',)))
{
foreach( $images as $image ) {
$metadata = wp_get_attachment_metadata($image->ID);
$width = $metadata['width'];
$height = $metadata['height'];
echo ', <br/>Image Resolutions: '.$width.'&times;'.$height;
}
} else {
echo "No Image";
}
}
function feedtitle() {
echo '<br/>Title: '.get_the_title($ID);
}
function feeddate() {
echo ', <br/>Added on : '.get_the_time('F j, Y');
}
function imagetipe() {
global $post;
$attachments = get_children(array('post_parent'=>$post->ID));
if ( $attachments = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => 1,
'post_status' => null,)))
{
foreach ( $attachments as $attachment ) {
echo ', <br/>Image Format: '.$attachment->post_mime_type;
}
} else {
echo "Unknown Size";
}
}
function feedimage($size='homepage-thumb') {
global $post;
$limitImages = 1;

if ( $images = get_children(array(
'post_parent' => $post->ID,
'post_type' => 'attachment',
'numberposts' => $limitImages,
'orderby' => 'ASC',
'post_mime_type' => 'image',)))
{
foreach( $images as $image ) {
$attachmenturl=get_attachment_link($image->ID);
$attachmentimage=wp_get_attachment_image( $image->ID, $size );
$img_title = $image->post_title;
echo $attachmentimage;
}
} else {
echo "No Image";
}
}

function customfeed($content) {
global $post;
$content = $content .''.feedimage().''.feedtitle().''.feedattress() .''.feedsize() .''.imagetipe().''.feeddate().'';
return $content;
}
add_filter('the_content_feed', 'customfeed');
add_filter('the_excerpt_rss', 'customfeed');

?>