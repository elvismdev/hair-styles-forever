<?php
/**
 * This file is used for your entries and post media
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

$wpex_related_category = NULL;
$wpex_related_category = get_the_category(); //get first current category ID
$wpex_related_category = $wpex_related_category[0]->cat_ID;

$this_post = $post->ID; // get ID of current post

$args = array(
	'numberposts' => of_get_option('related_random_count', '3'),
	'orderby' => 'rand',
	'category' => $wpex_related_category,
	'exclude' => $this_post,
	'offset' => null,
	'suppress_filters' => false, // WPML support
);
$posts = get_posts($args);
if($posts) { ?>
    <section id="related-posts">
        <h4 id="related-heading"><span><?php _e('Related Posts','wpex'); ?></span></h4>
        <ul>
		<?php
        foreach($posts as $post) : setup_postdata($post); ?>
        	<li class="related-post-entry clearfix">
                <?php if( has_post_thumbnail() ) { ?>
                	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="related-post-thumbnail"><img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ), 80, 80, true ); ?>" alt="<?php the_title(); ?>" /></a>
                <?php } ?>
                <div class="related-post-description <?php if( !has_post_thumbnail() ) echo 'class="full-width'; ?>">
                	<h5 class="related-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
                	<p><?php echo wp_trim_words( strip_shortcodes( get_the_content() ), 20, '&hellip;' ); ?><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more"><?php _e('read more','wpex'); ?> &rarr;</a></p>
                </div><!-- /related-post-description -->
            </li>
        <?php endforeach; wp_reset_postdata(); ?>
        </ul>
    </section> <!-- /related-posts --> 
<?php } // no posts found