<?php
/**
 * This file is used for your audio entries and post media
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
 
 
/******************************************************
 * Single Posts
 * @since 1.0
*****************************************************/
if ( is_singular() && is_main_query() ) {
	
	
	// No featured image for music posts

/******************************************************
 * Entries
 * @since 1.0
*****************************************************/
} else { ?>

    <article <?php post_class('post-entry clearfix'); ?>>  
        <?php if( has_post_thumbnail() ) {  ?>
            <div class="post-entry-thumbnail">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                	<img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ),  wpex_img( 'entry_width' ), wpex_img( 'entry_height' ), wpex_img( 'entry_crop' ) ); ?>" alt="<?php echo the_title(); ?>" />
                    <?php
					// hover overlay
                    if( of_get_option('entry_overlay','1') == '1') { ?><span class="overlay <?php echo get_post_format(); ?>"></span><?php }
					// Sticky post star graphic
                    if(is_sticky()) echo '<span class="sticky-overlay"></span>'; ?>
                </a>
            </div><!-- /post-entry-thumbnail -->
        <?php } ?>
        <div class="post-entry-details <?php if( !has_post_thumbnail() ) echo 'full-width'; ?>">
        	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
            <ul class="post-entry-meta clearfix">
            	<li><?php echo get_the_date(); ?> //</li>
                <li><a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author_meta('display_name'); ?></a></li>
                <?php if(comments_open()) { ?><li class="meta-comments comments-scroll">// <?php comments_popup_link(__('0 comments', 'wpex'), __('1 comment', 'wpex'), __('% comments', 'wpex'), 'comments-link' ); ?></li><?php } ?>
            </ul><!-- /post-entry-meta -->
            <div class="post-entry-excerpt">
            	<?php the_excerpt(); ?>
            </div><!-- /post-entry-excerpt -->
        </div><!-- /post-entry-details -->
    </article><!-- /post-entry -->

<?php } ?>