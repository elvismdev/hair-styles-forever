<?php
/**
 * This file is used for your entries and post media
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

if( of_get_option('homepage_large_slider_category','') !== '' ) {
	
	// Get posts
	global $post;
	$wpex_blog_featured_posts = get_posts(
		array(
			'post_type' =>'post',
			'numberposts' => of_get_option('homepage_slider_count','8'),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'id',
					'terms' => of_get_option('homepage_large_slider_category','')
					)
				),
			'suppress_filters' => false, // WPML support
		)
	);
	if( $wpex_blog_featured_posts ) {
		
		// Get Scripts
		wp_enqueue_style('royalslider');
		wp_enqueue_style('rs-default');
		wp_enqueue_script('royalslider');
		wp_enqueue_script('wpex-home-slider-init'); ?>
        
		<div id="featured-slider">
			<div id="video-gallery" class="royalSlider videoGallery rsDefault">
				<?php
                foreach ($wpex_blog_featured_posts as $post ) : setup_postdata( $post ); ?>
                	<?php if ( get_post_format() == 'video' ) { ?>
                    <div class="rsContent">
                    	<div class="rsABlock" data-move-effect="left" data-move-offset="40">
							<h2><?php the_title(); ?></h2>
                            <p><?php echo wp_trim_words( strip_shortcodes( get_the_content() ), 15, '&hellip;' ); ?><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php _e('read more','wpex'); ?> &rarr;</a></p>
                        </div>
                        <a class="rsImg" data-rsvideo="<?php echo get_post_meta( get_the_ID(), 'wpex_post_video', true ); ?>" href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>">
							<div class="rsTmb">
                                <span class="rsTmbTitle"><?php echo wp_trim_words( get_the_title(), '4' ); ?></span>
								<?php
                                $category = get_the_category();
                                if( $category[0] && $category[0]->term_id !== of_get_option( 'homepage_large_slider_category' ) ){
                                    echo '<span class="rsTmbCat">'. $category[0]->cat_name .'</span>';
                                } elseif( isset ( $category[1] ) ) {
										echo '<span class="rsTmbCat">'. $category[1]->cat_name .'</span>';
								} else { 
									echo '<span class="rsTmbCat">'. $category[0]->cat_name .'</span>';
								} ?>
                        	</div>
                        </a>
                    </div>
                    <?php } else { ?>
					<div class="rsContent">
                    	<div class="rsABlock" data-move-effect="left" data-move-offset="40">
							<h2><?php the_title(); ?></h2>
                            <p><?php echo wp_trim_words( strip_shortcodes( get_the_content() ), 15, '&hellip;' ); ?><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php _e('read more','wpex'); ?> &rarr;</a></p>
                        </div>
                        <?php if ( get_post_format() == 'link' ) { ?>
                        	<a href="<?php echo get_post_meta( get_the_ID(), 'wpex_post_url', TRUE ); ?>" title="<?php the_title(); ?>" target="_blank">
                        <?php } else { ?>
                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php } ?>
                        <img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ),  wpex_img( 'royalslider_home_width' ), wpex_img( 'royalslider_home_height' ), wpex_img( 'royalslider_home_crop' ) ); ?>" alt="<?php echo the_title(); ?>" />
                        </a>
                        	<div class="rsTmb">
                                <span class="rsTmbTitle"><?php echo wp_trim_words( get_the_title(), '4' ); ?></span>
								<?php
                                $category = get_the_category();
                                if( $category[0] && $category[0]->term_id !== of_get_option( 'homepage_large_slider_category' ) ){
                                    echo '<span class="rsTmbCat">'. $category[0]->cat_name .'</span>';
                                } elseif( isset ( $category[1] ) ) {
										echo '<span class="rsTmbCat">'. $category[1]->cat_name .'</span>';
								} else { 
									echo '<span class="rsTmbCat">'. $category[0]->cat_name .'</span>';
								} ?>
                        	</div>
                    </div>  
                    <?php } ?>
			   <?php endforeach; ?>
			</div><!-- #video-gallery -->
		</div><!-- #featured-slider -->
	<?php
	}
	wp_reset_postdata();
}

if( of_get_option('homepage_slider_alt','') !== '' ) {
	if( of_get_option('homepage_slider_alt_url') !== '') { ?>
		<a href="<?php echo of_get_option('homepage_slider_alt_url'); ?>" title=""><img src="<?php echo of_get_option('homepage_slider_alt',''); ?>" alt="" id="home-slider-alt" /></a>
    <?php } else { ?>
    	<img src="<?php echo of_get_option('homepage_slider_alt',''); ?>" alt="" id="home-slider-alt" />
<?php }
}