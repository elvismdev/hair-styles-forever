<?php
/**
 * This file is used for your entries and post media
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

if( of_get_option('homepage_small_slider_category','') !== '' ) {
	
	// Get posts
	global $post;
	$wpex_blog_featured_posts = get_posts(
		array(
			'post_type' =>'post',
			'numberposts' => of_get_option('homepage_slider_count','8'),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'id',
					'terms' => of_get_option('homepage_small_slider_category','')
					)
				),
			'suppress_filters' => false, // WPML support
		)
	);
	if( $wpex_blog_featured_posts ) {
		
		// Get Scripts
		wp_enqueue_script('flexslider');
		wp_enqueue_script('wpex-flexslider-home-init'); ?>
        
		<div id="featured-flexslider" class="flexslider">
			<ul class="slides">
				<?php foreach ($wpex_blog_featured_posts as $post ) : setup_postdata( $post ); ?> 
				<li class="featured-flexslider">
                	<?php if ( get_post_format() == 'link' ) { ?>
                    	<a href="<?php echo get_post_meta( get_the_ID(), 'wpex_post_url', TRUE ); ?>" title="<?php the_title(); ?>" target="_blank" class="featured-flexslider-img">
                    <?php } else { ?>
                    	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="featured-flexslider-img">
                    <?php } ?>
                        <img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ),  wpex_img( 'flex_home_slide_width' ), wpex_img( 'flex_home_slide_height' ), wpex_img( 'flex_home_slide_crop' ) ); ?>" alt="<?php echo the_title(); ?>" />
                    </a>
                    <div class="featured-flexslider-caption">
						<?php the_title(); ?>
                    </div><!-- /featured-flexslider-caption -->
				</li><!-- .featured-flexslider--> 
			   <?php endforeach; ?>
			</ul><!--.slides -->
		</div><!-- #featured-flexslider -->
    
	<?php
	}
	wp_reset_postdata(); ?>
    <h2 class="home-title"><span><?php _e('Latest Posts','wpex'); ?></span></h2>
<?php } ?>