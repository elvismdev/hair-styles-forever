<?php
/**
 * Author.php loads the author pages with a listing of their posts
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template ?>

<div id="post" class="post clearfix">  

    <header id="page-heading" class="author-page-description">
        <?php
		// Get author name
        if(isset($_GET['author_name'])) {
        	$curauth = get_userdatabylogin($author_name);
		} else {
        	$curauth = get_userdata(intval($author));
		}
		// Show author gravatar
		echo get_avatar( get_the_author_meta('user_email'), '60', '' ); ?>
        <h1><?php _e('Author','wpex'); ?>: <?php echo $curauth->display_name; ?></h1>
        <p><?php the_author_meta('description'); ?></p>
    </header><!-- /page-heading -->

	<?php if( have_posts() ) : // check for posts ?>
    
        <div id="post-entries" class="clearfix">
            <?php
            while (have_posts()) : the_post();
                get_template_part( 'content', get_post_format() );  
            endwhile; ?>
        </div><!-- /post-entries -->
	   <?php
        if( of_get_option('ajax_loading', '1') == 1 ) {
            wp_enqueue_script('wpex-ajax-load'); // get js scripts for ajax posts loading
            echo aq_load_more(); // ajax pagination
        } else {
            wpex_pagination(); // regular 1,2,3 pagination
        }
		
	endif; // end have_posts check ?>
    
    
</div><!--/post -->
<?php
get_sidebar(); // Loads the sidebar.php file
get_footer(); // Loads the footer.php file