<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {
    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = 'options_wpex_themes';
    update_option('optionsframework', $optionsframework_settings);
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'wpex'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */



// START OPTIONS
function optionsframework_options() {
	
	//Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	$options_categories[''] = __('Select a category:','att');
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	//image path for admin options
	$wpex_bgs =  get_template_directory_uri() . '/images/options/bgs/';
	$imagepath =  get_template_directory_uri() . '/images/options/';

	//start array
	$options = array();
	
	//GENERAL	
	$options[] = array(
		'name' => __('General', 'wpex'),
		'type' => 'heading');
		
	$options['custom_logo'] = array(
		'name' => __('Custom Logo', 'wpex'),
		'desc' => __('Upload your custom logo.', 'wpex'),
		'std' => '',
		'id' => 'custom_logo',
		'type' => 'upload');
				
	$options['background'] = array(
		'name' => __('Predefined Background','wpex'),
		'desc' => __('Select your color of choice for your theme design. Note: this option is for the BOXED layout only. The Full-Width layout will have a white background, the only way to override is at Apperance/Background.','wpex'),
		'id' => "background",
		'std' => "none",
		'type' => "images",
		'options' => array(
			'none' => $wpex_bgs . 'none.png',
			'noise-see-through' => $wpex_bgs . 'noise-see-through.png',
			'noise' => $wpex_bgs . 'noise.png',
			'wood_2' => $wpex_bgs . 'wood_2.png',
			'debut_dark' => $wpex_bgs . 'debut_dark.png',
			'gray_jean' => $wpex_bgs . 'gray_jean.png',
			'diamond_upholstery' => $wpex_bgs . 'diamond_upholstery.png',
			'gray_jean' => $wpex_bgs . 'gray_jean.png',
			'grid' => $wpex_bgs . 'grid.png',
			'light_noise_diagonal' => $wpex_bgs . 'light_noise_diagonal.png',
			'noisy_grid' => $wpex_bgs . 'noisy_grid.png',
			'retina_dust' => $wpex_bgs . 'retina_dust.png',
			'retina_wood' => $wpex_bgs . 'retina_wood.png',
			'shattered' => $wpex_bgs . 'shattered.png',
			'solid' => $wpex_bgs . 'solid.png',
			'textured_stripes' => $wpex_bgs . 'textured_stripes.png',
			'dark_wall' => $wpex_bgs . 'dark_wall.png',
			'classy_fabric' => $wpex_bgs . 'classy_fabric.png',
			'dark_tire' => $wpex_bgs . 'dark_tire.png',
			'husk' => $wpex_bgs . 'husk.png',
			'first_aid_kit' => $wpex_bgs . 'first_aid_kit.png',
			'rough_diagonal' => $wpex_bgs . 'rough_diagonal.png',
			'texturetastic_gray' => $wpex_bgs . 'texturetastic_gray.png'
		)
	);
		
	$options['sidebar_location'] = array(
		'name' => "Sidebar Location",
		'desc' => "Select your default sidebar location (left or right)",
		'id' => "sidebar_location",
		'std' => "right",
		'type' => "images",
		'options' => array(
			'right' => $imagepath . '2cr.png',
			'left' => $imagepath . '2cl.png'
		)
	);
							
	$options['responsive'] = array(
		'name' => __('Responsive?', 'wpex'),
		'desc' => __('Check to enable the responsive layout option.', 'wpex'),
		'id' => 'responsive',
		'std' => '1',
		'type' => 'checkbox');
			
	$options['main_boxshadow'] = array(
		'name' => __('Main Box Shadow', 'wpex'),
		'desc' => __('Check box to enable the main box shadow around the top header region, the main content and the footer carousel.', 'wpex'),
		'id' => 'main_boxshadow',
		'std' => '1',
		'type' => 'checkbox');
	
	$options['entry_overlay'] = array(
		'name' => __('Entry Image Hover Overlay', 'wpex'),
		'desc' => __('Check box to enable the entry overlays. These are the colored boxes with icons that show up when you hover over an entry image.', 'wpex'),
		'id' => 'entry_overlay',
		'std' => '1',
		'type' => 'checkbox');
		
	$options['main_color'] = array(
		'name' => __('Main Color', 'wpex'),
		'desc' => __('Select a color for your theme highlights.', 'wpex'),
		'id' => 'main_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['standard_overlay_color'] = array(
		'name' => __('Standard Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your standard entry image overlay hover.', 'wpex'),
		'id' => 'standard_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['video_overlay_color'] = array(
		'name' => __('Video Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your video entry image overlay hover.', 'wpex'),
		'id' => 'video_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['image_overlay_color'] = array(
		'name' => __('Image Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your image entry image overlay hover.', 'wpex'),
		'id' => 'image_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['gallery_overlay_color'] = array(
		'name' => __('Gallery Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your gallery entry image overlay hover.', 'wpex'),
		'id' => 'gallery_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['link_overlay_color'] = array(
		'name' => __('Link Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your link entry image overlay hover.', 'wpex'),
		'id' => 'link_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');
		
	$options['audio_overlay_color'] = array(
		'name' => __('Audio Entry Image Overlay Background', 'wpex'),
		'desc' => __('Select a color for your audio entry image overlay hover.', 'wpex'),
		'id' => 'audio_overlay_color',
		'std' => '#0090ff',
		'type' => 'color');	
		
	//HEADER
	$options[] = array(
		'name' => __('Header', 'wpex'),
		'type' => 'heading');
		
	$options['notification_bar'] = array(
		'name' => __('Breaking News Area &amp; Search Bar?', 'wpex'),
		'desc' => __('Check box to enable the breaking news and search area ', 'wpex'),
		'id' => 'notification_bar',
		'std' => '1',
		'type' => 'checkbox');
		
	$options['header_breaking_news_title'] = array(
	'name' => __('Breaking News Title', 'wpex'),
		'desc' => __('Enter the title for your breaking news section here.', 'wpex'),
		'id' => 'header_breaking_news_title',
		'std' => 'Breaking News',
		'type' => 'text');	
		
	$options['header_breaking_news_title_color'] = array(
		'name' => __('Breaking News Title Background', 'wpex'),
		'desc' => __('Select a color for the breaking news title background.', 'wpex'),
		'id' => 'header_breaking_news_title_color',
		'std' => '#da3610',
		'type' => 'color');
		
	$options['header_breaking_news'] = array(
	'name' => __('Breaking News', 'wpex'),
		'desc' => __('Enter your breaking news text below.', 'wpex'),
		'id' => 'header_breaking_news',
		'std' => 'You can add anything you want here, even add links with HTML <a href="#" title="Link Title">check it out &rarr;</a>',
		'type' => 'textarea');	
		
		
	//SOCIAL
	$options[] = array(
		'name' => __('Social', 'wpex'),
		'type' => 'heading');
			
	$options['social'] = array(
		'name' => __('Social?', 'wpex'),
		'desc' => __('Check box to enable the social in the theme header.', 'wpex'),
		'id' => 'social',
		'std' => '1',
		'type' => 'checkbox');
		
	if(function_exists('wpex_get_social')) {	
		$social_links = wpex_get_social();
		foreach($social_links as $social_link) {
		$options[] = array( "name" => ucfirst($social_link),
							'desc' => ' '. __('Enter your ','wpex') . $social_link . __(' url','wpex') .' <br />'. __('Include http:// at the front of the url.', 'wpex'),
							'id' => $social_link,
							'std' => '#',
							'type' => 'text');
		}
	}

	
	//Homepage
	$options[] = array(
				'name' => __('Homepage', 'att'),
				'type' => 'heading');
				
	$options['homepage_tagline'] = array(
		'name' => __('Homepage Tagline', 'att'),
		'desc' => __('Enter your homepage tagline here. Leave blank to show nothing.', 'att'),
		'id' => 'homepage_tagline',
		'std' => 'Welcome to Bloggit <br /> the HOTTEST blog on the interwebs!',
		'type' => 'textarea');
				
	$options['homepage_large_slider_category'] = array(
		'name' => __('Large Slider Category', 'options_check'),
		'desc' => __('Select a category to display posts from for the homepage large slider.', 'options_check'),
		'id' => 'homepage_large_slider_category',
		'type' => 'select',
		'options' => $options_categories);
		
	$options['homepage_small_slider_category'] = array(
		'name' => __('Small Slider Category', 'options_check'),
		'desc' => __('Select a category to display posts from for the homepage small slider.', 'options_check'),
		'id' => 'homepage_small_slider_category',
		'type' => 'select',
		'options' => $options_categories);
		
	$options['homepage_slider_count'] = array(
		'name' => __('Featured Slider Count', 'att'),
		'desc' => __('How many items do you wish to show for your featured category blog slider?', 'att'),
		'id' => 'homepage_slider_count',
		'std' => '8',
		'type' => 'text');
		
	$options['homepage_slider_alt'] = array(
		'name' => __('Slider Alternative Single Image', 'wpex'),
		'desc' => __('Upload an image to replace the default slider. If defined, this will also replace your slider for responsive browsers.', 'wpex'),
		'std' => '',
		'id' => 'homepage_slider_alt',
		'type' => 'upload');
		
	$options['homepage_slider_alt_url'] = array(
		'name' => __('Slider Alternative URL', 'wpex'),
		'desc' => __('Enter a URL if you wish to link your slider alternative image to another site/post.', 'wpex'),
		'std' => '',
		'id' => 'homepage_slider_alt_url',
		'type' => 'text');
		
		
	//BLOG
	$options[] = array(
				'name' => __('Blog', 'att'),
				'type' => 'heading');	
					
	$options['ajax_loading'] = array(
		'name' => __('AJAX Loading Instead of Pagination?', 'wpex'),
		'desc' => __('Check box to enable the load more ', 'wpex'),
		'id' => 'ajax_loading',
		'std' => '1',
		'type' => 'checkbox');
	
	$options['single_post_media'] = array(
		'name' => __('Featured Image/Video on single posts?', 'wpex'),
		'desc' => __('Check this box to automatically Show Featured Image/Video/Galleries on single blog posts.', 'wpex'),
		'id' => 'single_post_media',
		'std' => '1',
		'type' => 'checkbox');
		
	$options['single_post_tags'] = array(
		'name' => __('Show Tags?', 'wpex'),
		'desc' => __('Check this box to show the tags the current post belongs to below the post.', 'wpex'),
		'id' => 'single_post_tags',
		'std' => '1',
		'type' => 'checkbox');
		
	$options['single_post_author'] = array(
		'name' => __('Single Post Author?', 'wpex'),
		'desc' => __('Check this box to show the author bio below the posts.', 'wpex'),
		'id' => 'single_post_author',
		'std' => '1',
		'type' => 'checkbox');
		
	$options['single_post_related'] = array(
		'name' => __('Related Posts?', 'wpex'),
		'desc' => __('Check this box to show the related posts section below posts.', 'wpex'),
		'id' => 'single_post_related',
		'std' => '1',
		'type' => 'checkbox');
	
	//ADS
	$options[] = array(
				'name' => __('Ads', 'att'),
				'type' => 'heading');
				
	$options['header_banner'] = array(
		'name' => __('Header Banner Code', 'att'),
		'desc' => __('Enter your code for your header banner here such as HTML or Google Adsense.', 'att'),
		'id' => 'header_banner',
		'std' => '<a href="http://marketplace.tutsplus.com/?ref=wpexplorer" target="_blank" rel="nofollow"><img src="'. get_template_directory_uri() .'/images/top-banner.gif" alt="banner" /></a>',
		'type' => 'editor'
	);
	
	$options['header_banner_margin'] = array(
		'name' => __('Header Banner Top Margin', 'att'),
		'desc' => __('Enter a top margin in pixels for your header banner, default is 10px.', 'att'),
		'id' => 'header_banner_margin',
		'std' => '0px',
		'type' => 'text'
	);
	
	$options['footer_banner'] = array(
		'name' => __('Footer Banner Code', 'att'),
		'desc' => __('Enter your code for your footer banner here such as HTML or Google Adsense.', 'att'),
		'id' => 'footer_banner',
		'std' => '<a href="http://marketplace.tutsplus.com/?ref=wpexplorer" target="_blank" rel="nofollow"><img src="'. get_template_directory_uri() .'/images/banner-large.gif" alt="banner" /></a>',
		'type' => 'editor'
	);
	
	
	
	//FOOTER
	$options[] = array(
				'name' => __('Footer', 'att'),
				'type' => 'heading');
				
	$options['widgetized_footer_one'] = array(
		'name' => __('Widgetized Footer 1', 'att'),
		'desc' => __('Check to enable the main widgetized footer (inside the boxed container).', 'att'),
		'id' => 'widgetized_footer_one',
		'std' => '1',
		'type' => 'checkbox'
	);
	
	$options['footer_carousel_category'] = array(
		'name' => __('Footer Carousel Category', 'options_check'),
		'desc' => __('Select a category to display posts from for your footer carousel. There should be at least 8 posts in this category for the carousel to run smoothly.', 'options_check'),
		'id' => 'footer_carousel_category',
		'type' => 'select',
		'options' => $options_categories);
		
	$options['footer_carousel_count'] = array(
		'name' => __('Footer Carousel Count', 'options_check'),
		'desc' => __('How many posts to show in the footer carousel. Must be 8 or greater', 'options_check'),
		'id' => 'footer_carousel_count',
		'std' => '8',
		'type' => 'text',
		'options' => $options_categories);	
			
	$options['widgetized_footer_two'] = array(
		'name' => __('Widgetized Footer 2', 'att'),
		'desc' => __('Check to enable the bttom most widgetized footer.', 'att'),
		'id' => 'widgetized_footer_two',
		'std' => '1',
		'type' => 'checkbox'
	);

	return $options;
}


/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {
  		$('#section-example_text_hidden').fadeToggle(400);
	});

	if ($('#example_showhidden:checked').val() !== undefined) {
		$('#section-example_text_hidden').show();
	}

});
</script>

<?php } ?>