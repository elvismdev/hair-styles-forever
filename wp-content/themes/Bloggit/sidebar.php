<?php
/**
 * Sidebar.php is used to show your sidebar widgets on pages/posts
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
?>

<aside id="sidebar">
	<?php dynamic_sidebar('sidebar'); ?>
</aside><!-- /sidebar -->