<?php
/**
 * Index.php is the default template. This file is used when a more specific template can not be found to display your posts.
 * It is unlikely this template file will ever be used, but it's here to back you up just incase.
 *
 *
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template
if ( !is_paged() ) get_template_part('includes/slider', 'one' ); //homepage large slider

if( of_get_option('homepage_tagline', 'Welcome to Bloggit <br /> the HOTTEST blog on the interwebs!') !== '' ) { ?>
    <div id="homepage-tagline" class="clearfix">
        <h1><?php echo of_get_option('homepage_tagline', 'Welcome to Bloggit <br /> the HOTTEST blog on the interwebs!'); ?></h1>
    </div><!-- /homepage-tagline -->
<?php } ?>
    
<div id="post" class="clearfix">

	<?php
    if ( !is_paged() ) get_template_part('includes/slider', 'two' ); //homepage small slider
	if (have_posts()) : ?>
    
        <div id="post-entries" class="clearfix">
            <?php
            while (have_posts()) : the_post();
                get_template_part( 'content', get_post_format() );  
            endwhile; ?>
        </div><!-- /post-entries -->
    
	<?php endif;
	if( of_get_option('ajax_loading', '1') == 1 ) {
		wp_enqueue_script('wpex-ajax-load');
		echo aq_load_more();
	} else {
		wpex_pagination();
	} ?>
    
</div><!-- /post -->  

<?php
get_sidebar(); //get template sidebar
get_footer(); //get template footer