<?php
/**
 * This file is used for your deals entries and post media
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
 
 
/******************************************************
 * Single Posts
 * @since 1.0
*****************************************************/
if ( is_singular('deals') && is_main_query() ) {
	remove_filter( 'the_content', 'sharing_display',19);
	remove_filter( 'the_excerpt', 'sharing_display',19); ?>

    <article id="deals-post" class="entry clearfix">  
        <?php if( has_post_thumbnail() ) {  ?>
            <div class="deals-entry-thumbnail">
                <?php if( get_post_meta(get_the_ID(), 'wpex_deals_url', true) !== '' ) { ?>
                    <a href="<?php echo get_post_meta(get_the_ID(), 'wpex_deals_url', true); ?>" title="<?php the_title(); ?>" rel="nofollow" target="_blank">
                        <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="<?php echo the_title(); ?>" />
                    </a>
                <?php } else { ?>
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="<?php echo the_title(); ?>" />
                <?php } ?>
            </div><!-- /deals-entry-thumbnail -->
        <?php } ?>
        <div class="deals-entry-details">
            <h2><?php the_title(); ?></h2>
            <div class="deals-entry-excerpt">
                <?php the_content(); ?>
            </div><!-- /deals-entry-excerpt -->
            <?php
            if( get_post_meta(get_the_ID(), 'wpex_deals_url', true) !== '' ) {
                echo '<a href="'. get_post_meta(get_the_ID(), 'wpex_deals_url', true) .'" title="'. get_the_title() .'" rel="nofollow" class="deals-button" target="_blank"><span>'. __('Visit Site','wpex') .'</span></a>';
            } ?>
        </div><!-- /deals-entry-details -->
    </article><!-- /deals-entry -->

<?php
/******************************************************
 * Entries
 * @since 1.0
*****************************************************/
} else { ?>

    <article <?php post_class('deals-entry clearfix'); ?>>  
        <?php if( has_post_thumbnail() ) {  ?>
            <div class="deals-entry-thumbnail">
            	<?php if( get_post_meta(get_the_ID(), 'wpex_deals_url', true) !== '' ) { ?>
					<a href="<?php echo get_post_meta(get_the_ID(), 'wpex_deals_url', true); ?>" title="<?php the_title(); ?>" rel="nofollow" target="_blank">
                		<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="<?php echo the_title(); ?>" />
                	</a>
                <?php } else { ?>
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="<?php echo the_title(); ?>" />
                <?php } ?>
            </div><!-- /deals-entry-thumbnail -->
        <?php } ?>
        <div class="deals-entry-details">
        	<h2><?php the_title(); ?></h2>
            <div class="deals-entry-excerpt">
            	<?php the_content(); ?>
            </div><!-- /deals-entry-excerpt -->
            <?php
			if( get_post_meta(get_the_ID(), 'wpex_deals_url', true) !== '' ) {
				echo '<a href="'. get_post_meta(get_the_ID(), 'wpex_deals_url', true) .'" title="'. get_the_title() .'" rel="nofollow" class="deals-button" target="_blank"><span>'. __('Visit Site','wpex') .'</span></a>';
			} ?>
        </div><!-- /deals-entry-details -->
    </article><!-- /deals-entry -->

<?php } ?>