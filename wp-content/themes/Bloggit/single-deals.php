<?php
/**
 * Default file for single regular posts.
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
 
get_header(); // Loads the header.php template
if ( have_posts( )) : // Check for posts
while (have_posts()) : the_post(); // Loop through post ?>

    <div id="post" class="post-single clearfix">
        
        <header id="post-heading">
            <h1>
			<?php the_title(); ?>
            </h1>     
	        <ul class="meta clearfix">
				<li class="date"><span><?php _e('Posted On', 'wpex'); ?></span>: <?php the_time('jS F Y'); ?></li> 
				<li class="author"><span><?php _e('By', 'wpex'); ?></span>: <?php the_author_posts_link(); ?></li>
                <?php if(comments_open() && !post_password_required() ) { ?><li class="comment-scroll"><span><?php _e('With', 'wpex'); ?></span> <?php comments_popup_link(__('0 Comments', 'wpex'), __('1 Comment', 'wpex'), __('% Comments', 'wpex'), 'comments-link' ); ?></li><?php } ?>
		    </ul><!-- /meta --> 
        </header><!-- /page-heading -->
        
        <?php
		// show media (image/video/gallery) if NOT a private post
        if ( !post_password_required() && of_get_option('single_post_media','1') == '1' && $page < 2 ) {
			get_template_part( 'content',  'deals' );
		}
		
		// Show jetpack social sharing
		if ( function_exists( 'sharing_display' ) ) echo sharing_display();
        
		// Get comments template located at comments.php
		comments_template(); ?>
            
    </div><!-- /post -->

<?php
endwhile; // end posts loop
endif; // end have_posts check
get_sidebar(); // Get template sidebar ?>

<div class="clear"></div>
<div class="post-pagination clearfix">
    <div class="post-prev"><?php next_post_link('%link', '&larr; %title', false); ?></div> 
    <div class="post-next"><?php previous_post_link('%link', '%title &rarr;', false); ?></div>
</div><!-- /post-pagination -->
        
<?php get_footer(); // Get template footer