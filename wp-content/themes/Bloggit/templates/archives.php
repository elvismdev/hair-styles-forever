<?php
/**
 * Template Name: Archives
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template

if (have_posts()) : while (have_posts()) : the_post(); ?>

<div id="post" class="archives-template clearfix">

    <header id="page-heading">
    	<h1><?php the_title(); ?></h1>
    </header><!-- /page-heading -->
    
    <article class="entry clearfix">	
        <?php the_content(); ?>
    </article><!-- /entry -->
    
	<?php
    $terms = get_terms( 'category' );
    foreach($terms as $term) { ?>
    <div id="archives-wrap" class="clearfix">
        <section class="archives-section">
        	<h2><a href="<?php echo get_term_link($term->slug, 'category'); ?>" title="<?php echo $term->name; ?>"><?php echo $term->name; ?></a></h2>
            <ul class="archives-list clearfix">
				<?php
                $term_posts = get_posts(array(
                    'post_type' => 'post',
                    'numberposts' => -1,
					'orderby' => 'post_date',
					'order' => 'ASC',
                    'tax_query' => array( array( 'taxonomy' => 'category', 'terms' => $term->slug, 'field' => 'slug' ) )
                ));
                $count=0;
                foreach ($term_posts as $post) : setup_postdata($post);
                $count++; ?>
                    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span class="archives-count"><?php echo $count; ?></span><?php the_title(); ?> <span class="archives-date">- <?php echo get_the_time('m/d/y'); ?></span></a></li>
                <?php endforeach; wp_reset_postdata(); ?>
            </ul>
        </section><!-- /archives-section -->
    </div><!-- /archives-wrap -->
    <?php } ?>
    
</div><!-- /archives-template -->
 
<?php
endwhile; endif; // End loop
get_sidebar(); // Loads the sidebar.php file
get_footer(); // Loads the footer.php file