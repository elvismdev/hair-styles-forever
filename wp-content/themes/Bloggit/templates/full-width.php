<?php
/**
 * Template Name: Full-Width
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template
if (have_posts()) : while (have_posts()) : the_post(); // Start main loop
	
// Show featured image
if( has_post_thumbnail() ) {
	echo '<div id="full-page-featured-img"><img src="'. wp_get_attachment_url( get_post_thumbnail_id() ) .'" alt="'. get_the_title() .'" /></div>';
} ?>

<div id="full-width-post" class="clearfix">

    <header id="page-heading">
    	<h1><?php the_title(); ?></h1>
    </header><!-- /page-heading -->
    
    <article class="entry clearfix">	
        <?php the_content(); ?>
    </article><!-- /entry -->

</div><!-- /full-width-post -->
 
<?php
endwhile; endif; // End loop
get_footer(); // Loads the footer.php file