<?php

/**
 * Creates a function for your featured image sizes which can be altered via your child theme
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
 
if ( ! function_exists( 'wpex_img_heights' ) ) :
	function wpex_img($args){
		
		//homapage royal slider
		if( $args == 'royalslider_home_width' ) return '700';
		if( $args == 'royalslider_home_height' ) return '450';
		if( $args == 'royalslider_home_crop' ) return true;
		
		//homapage flexslider
		if( $args == 'flex_home_slide_width' ) return '590';
		if( $args == 'flex_home_slide_height' ) return '340';
		if( $args == 'flex_home_slide_crop' ) return true;
		
		//blog entries
		if( $args == 'entry_width' ) return '370';
		if( $args == 'entry_height' ) return '330';
		if( $args == 'entry_crop' ) return true;
		
		//blog posts
		if( $args == 'post_width' ) return '590';
		if( $args == 'post_height' ) return '9999';
		if( $args == 'post_crop' ) return false;
		
	}
endif;