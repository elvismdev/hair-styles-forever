<?php
/**
 * Function used to control pagination on post type tax, archive and search pages
 * @package Impact WordPress Theme
 * @since 1.0
 * @author AJ Clarke : http://wpexplorer.com
 * @copyright Copyright (c) 2012, AJ Clarke
 * @link http://wpexplorer.com
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

//get posts per page
$wpex_option_posts_per_page = get_option( 'posts_per_page' );

//add posts per page filter
add_action( 'init', 'wpex_modify_posts_per_page', 0);
function wpex_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'wpex_option_posts_per_page' );
}

//modify posts per page
function wpex_option_posts_per_page( $value ) {

	global $wpex_option_posts_per_page;
	
	//tax pagination
    if( is_post_type_archive('deals') || is_tax('deals_category') || is_tax('deals_tag') ) {
		return of_get_option('deals_pagination', '-1');
	}
	else { return $wpex_option_posts_per_page; }
}

?>