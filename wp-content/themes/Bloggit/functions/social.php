<?php
/**
 * Setup an array of social sites
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
  
function wpex_get_social() {
	
	//define array of icons
	$social_icons = array( 
			'twitter' => 'twitter',
			'facebook' => 'facebook',
			'google' => 'google',
			'dribbble' => 'dribbble', 
			'pinterest' => 'pinterest',
			'myspace' => 'myspace',
			'tumblr' => 'tumblr',
			'vimeo' => 'vimeo',
			'youtube' => 'youtube',
			'behance' => 'behance',
			'github' => 'github',
			'flickr' => 'flickr',
			'forrst' => 'forrst',
			'rss' => 'rss',
			'mail' => 'mail'
		);
	
	//return array
	return apply_filters('wpex_get_social', $social_icons);
}