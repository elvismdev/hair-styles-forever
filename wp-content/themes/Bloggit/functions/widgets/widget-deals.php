<?php
/**
 * Deals Widget
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 * @author WPExplorer : http://www.wpexplorer.com
 */
 
class wpex_deals_widget extends WP_Widget {
    /** constructor */
    function wpex_deals_widget() {
        parent::WP_Widget(false, $name = __('Deals','wpex') );
    }

    /** @see WP_Widget::widget */
    function widget($args, $instance) {		
        extract( $args );
        $title = apply_filters('widget_title', $instance['title']);
        $number = apply_filters('widget_title', $instance['number']);
		$order = apply_filters('widget_title', $instance['order']); ?>
              <?php echo $before_widget; ?>
                  <?php if ( $title )
                        echo $before_title . $title . $after_title; ?>
							<div class="wpex-widget-deals">
							<?php
								global $post;
								$args = array(
									'post_type' => 'deals',
									'numberposts' => $number,
									'order' => 'post_date',
									'orderby' => $order
								);
								$myposts = get_posts( $args );
								foreach( $myposts as $post ) : setup_postdata($post);
									get_template_part( 'content', 'deals-widget' );
                              	endforeach;
							  	wp_reset_postdata(); ?>
							</div>
              <?php echo $after_widget; ?>
        <?php
    }

    /** @see WP_Widget::update */
    function update($new_instance, $old_instance) {				
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
	$instance['number'] = strip_tags($new_instance['number']);
	$instance['order'] = strip_tags($new_instance['order']);
        return $instance;
    }

    /** @see WP_Widget::form */
    function form($instance) {	
	    $instance = wp_parse_args( (array) $instance, array(
			'title' => __('Random Deal','wpex'),
			'number' => '1',
			'order' => 'rand'
		));					
        $title = esc_attr($instance['title']);
        $number = esc_attr($instance['number']);
		$order = esc_attr($instance['order']); ?>
         <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'wpex'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title','wpex'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        
		<p>
          <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number to Show:', 'wpex'); ?></label> 
          <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo $number; ?>" />
        </p>
        
        <p>
        <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Random or Recent?', 'wpex'); ?></label>
        <br />
        <select class='wpex-select' name="<?php echo $this->get_field_name('order'); ?>" id="<?php echo $this->get_field_id('order'); ?>">
        <option value="rand" <?php if($order == 'rand') { ?>selected="selected"<?php } ?>><?php _e('Random', 'wpex'); ?></option>
        <option value="ASC" <?php if($order == 'ASC') { ?>selected="selected"<?php } ?>><?php _e('Recent', 'wpex'); ?></option>
        </select>
        </p>
        <?php
    }


} // class wpex_deals_widget
// register Recent Posts widget
add_action('widgets_init', create_function('', 'return register_widget("wpex_deals_widget");'));	
?>