<?php
/**
 * Define sidebars for use in this theme
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 * @author WPExplorer : http://www.wpexplorer.com
 */


//SIDEBAR
register_sidebar(array(
	'name' => __( 'Sidebar','wpex'),
	'id' => 'sidebar',
	'description' => __( 'Widgets in this area are used on the main sidebar region.','wpex' ),
	'before_widget' => '<div class="sidebar-box %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));


// FOOTER MAIN
register_sidebar(array(
	'name' => __( 'Footer Main 1','wpex'),
	'id' => 'main_footer_one',
	'description' => __( 'Widgets in this area are used in the first footer region.','wpex' ),
	'before_widget' => '<div class="main-footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));
register_sidebar(array(
	'name' => __( 'Footer Main 2','wpex'),
	'id' => 'main_footer_two',
	'description' => __( 'Widgets in this area are used in the second footer region.','wpex' ),
	'before_widget' => '<div class="main-footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));
register_sidebar(array(
	'name' => __( 'Footer Main 3','wpex'),
	'id' => 'main_footer_three',
	'description' => __( 'Widgets in this area are used in the third footer region.','wpex' ),
	'before_widget' => '<div class="main-footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));


// FOOTER BOTTOM
register_sidebar(array(
	'name' => __( 'Footer Bottom 1','wpex'),
	'id' => 'footer_one',
	'description' => __( 'Widgets in this area are used in the first footer region.','wpex' ),
	'before_widget' => '<div class="footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));
register_sidebar(array(
	'name' => __( 'Footer Bottom 2','wpex'),
	'id' => 'footer_two',
	'description' => __( 'Widgets in this area are used in the second footer region.','wpex' ),
	'before_widget' => '<div class="footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));
register_sidebar(array(
	'name' => __( 'Footer Bottom 3','wpex'),
	'id' => 'footer_three',
	'description' => __( 'Widgets in this area are used in the third footer region.','wpex' ),
	'before_widget' => '<div class="footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));
register_sidebar(array(
	'name' => __( 'Footer Bottom 4','wpex'),
	'id' => 'footer_four',
	'description' => __( 'Widgets in this area are used in the fourth footer region.','wpex' ),
	'before_widget' => '<div class="footer-widget %2$s clearfix">',
	'after_widget' => '</div>',
	'before_title' => '<h6>',
	'after_title' => '</h6>',
));