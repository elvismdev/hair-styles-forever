<?php
/**
 * This file loads the CSS and Javascript used for the theme.
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */


add_action('wp_enqueue_scripts','wpex_load_scripts');
function wpex_load_scripts() {

	/*************
	* CSS
	* @since 1.0
	*************/

		// Main CSS
		wp_enqueue_style('style', get_stylesheet_uri() );


		// Fancybox
		wp_enqueue_style('fancybox', WPEX_CSS_DIR . '/fancybox.css');

		// Remove default contact 7 styling
		if( function_exists('wpcf7_enqueue_styles') ) {
			wp_dequeue_style('contact-form-7');
		}

		// Royal Slider
		wp_register_style('royalslider', WPEX_CSS_DIR . '/royalslider.css');
		wp_register_style('rs-default', WPEX_CSS_DIR . '/rs-default.css');

		// Responsive
		if( of_get_option('responsive','1') == '1' ) {
			wp_enqueue_style('wpex-responsive', WPEX_CSS_DIR . '/responsive.css');
		}


	/*************
	* jQuery
	* @since 1.0
	*************/

		// Main Scripts
		wp_enqueue_script('global', WPEX_JS_DIR .'/global.js', array('jquery'), '1.0', true);
		wp_enqueue_script('superfish', WPEX_JS_DIR .'/superfish.js', array('jquery'), '1.4.8', true);
		wp_enqueue_script('easing', WPEX_JS_DIR .'/easing.js', array('jquery'), '1.3', true);
		wp_enqueue_script('fancybox', WPEX_JS_DIR .'/fancybox.js', array('jquery'), '2.1.3', true);

		// Royal Slider
		wp_register_script('royalslider', WPEX_JS_DIR .'/royalslider.js', array('jquery'), '9.2.0', true);
		wp_register_script('wpex-home-slider-init', WPEX_JS_DIR .'/royalslider-home-init.js', array('jquery', 'royalslider'), '1.0', true);

		// FlexSlider
		wp_register_script('flexslider', WPEX_JS_DIR .'/flexslider.js', array('jquery'), '2.1', true);
		wp_register_script('wpex-flexslider-home-init', WPEX_JS_DIR .'/flexslider-home-init.js', array('jquery','flexslider'), '1.0', true);

		// BX Carousel
		wp_register_script('bxslider', WPEX_JS_DIR .'/bxslider.js', array('jquery'), '4.0', true);
		wp_register_script('wpex-footer-carousel-init', WPEX_JS_DIR .'/footer-carousel-init.js', array('jquery', 'bxslider', 'easing'), '1.0', true);

		// Responsive
		wp_enqueue_script('fitvids', WPEX_JS_DIR .'/fitvids.js', array('jquery'), 1.0, true);
		wp_enqueue_script('uniform', WPEX_JS_DIR .'/uniform.js', array('jquery'), '1.7.5', true);
		wp_enqueue_script('wpex-responsive', WPEX_JS_DIR .'/responsive.js', array('jquery', 'uniform', 'fitvids'), '', true);

		// Comment replies
		if(is_single() || is_page()) wp_enqueue_script('comment-reply');

		// Register
		wp_register_script('wpex-ajax-load', WPEX_JS_DIR .'/ajax-load.js', array('jquery'), 1.0, true);

		//localize ajax
		wp_localize_script( 'wpex-ajax-load', 'wpexvars', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
		wp_localize_script( 'wpex-responsive', 'wpexLocalizeNav', array( 'main' => __('Browse','wpex') ) );


} //end wpex_load_scripts()