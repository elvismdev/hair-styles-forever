<?php
/**
 * Output theme settings custom CSS into your header
 * @package Delta WordPress Theme
 * @since 1.0
 * @author AJ Clarke : http://wpexplorer.com
 * @copyright Copyright (c) 2012, AJ Clarke
 * @link http://wpexplorer.com
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */


add_action('wp_head', 'wpex_custom_css');
function wpex_custom_css() {

	$custom_css ='';

	// Background
	if( of_get_option('background','none') !== 'none' && of_get_option('background','none') !== 'noise-see-through' ) {
		$custom_css .= 'body{ background-image: url('. get_template_directory_uri() .'/images/bgs/'. of_get_option('background') .'.png); }';
	}

	//Box Shadow
	if( of_get_option('main_boxshadow','1') !== '1' ) {
	$custom_css .= '#main-content,
					#notification-bar,
					#footer-carousel-inner,
					#footer-banner img{ box-shadow: none; }';
	}


	// Color Scheme
	if( of_get_option('main_color','none') !== '' && of_get_option('main_color','none') !== '#0090ff' ) {
		$custom_css .= '#main-navigation .sf-menu > li.sfHover > a,
						#main-navigation .sf-menu > li > a:hover,
						#main-navigation .sf-menu > .current-menu-item > a,
						#main-navigation .sf-menu > .current-menu-item > a:hover,
						.videoGallery .rsThumb.rsNavSelected,
						.tagcloud a:hover, .flex-direction-nav a:hover,
						.entry input[type="button"]:hover,
						.entry input[type="submit"]:hover,
						#commentsbox input[type="submit"]:hover,
						.theme-button:hover,
						#post-tags a:hover,
						.tagcloud a:hover,
						#wp-calendar tbody td:hover,
						#wp-calendar tbody td:hover a,
						.related-post-thumbnail:hover,
						.wpex-widget-recent-posts img:hover,
						.page-pagination a:hover,
						.page-pagination span.current,
						#paginate-post a:hover span,
						#paginate-post span,
						#load-more a:hover{ background-color: '. of_get_option('main_color','none') .'; }';

		$custom_css .= '.home-title span,
						#related-heading span,
						.comments-title span,
						#reply-title span,
						.related-post-thumbnail:hover,
						.wpex-widget-recent-posts img:hover{ border-color: '. of_get_option('main_color','none') .' }';
	}

	//Breaking News
	if( of_get_option('header_breaking_news_title_color','#da3610') !== '#da3610' && of_get_option('header_breaking_news_title_color','#da3610') !== '' ) {
		$custom_css .= '#breaking-news div#title{ background-color: '. of_get_option('header_breaking_news_title_color') .'; }';
	}

	// Entry Overlays
	if( of_get_option('standard_overlay_color','#0090ff') !== '#0090ff' && of_get_option('standard_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay{ background-color: '. of_get_option('standard_overlay_color') .'; }';
	}
	if( of_get_option('video_overlay_color','#0090ff') !== '#0090ff' && of_get_option('video_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay.video{ background-color: '. of_get_option('video_overlay_color') .'; }';
	}
	if( of_get_option('image_overlay_color','#0090ff') !== '#0090ff' && of_get_option('image_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay.image{ background-color: '. of_get_option('image_overlay_color') .'; }';
	}
	if( of_get_option('gallery_overlay_color','#0090ff') !== '#0090ff' && of_get_option('gallery_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay.gallery{ background-color: '. of_get_option('gallery_overlay_color') .'; }';
	}
	if( of_get_option('link_overlay_color','#0090ff') !== '#0090ff' && of_get_option('link_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay.link{ background-color: '. of_get_option('link_overlay_color') .'; }';
	}
	if( of_get_option('audio_overlay_color','#0090ff') !== '#0090ff' && of_get_option('audio_overlay_color','#0090ff') !== '' ) {
		$custom_css .= '.overlay.audio{ background-color: '. of_get_option('audio_overlay_color') .'; }';
	}


	// Ads
	if( of_get_option('header_banner_margin','0px') !== '0px' && of_get_option('background','0px') !== '' ) {
		$custom_css .= '#header-banner{ margin-top: '. of_get_option('header_banner_margin') .'; }';
	}

	// trim white space for faster page loading
	$custom_css_trimmed =  preg_replace( '/\s+/', ' ', $custom_css );

	// output css on front end
	$css_output = "<!-- Custom CSS -->\n<style type=\"text/css\">\n" . $custom_css_trimmed . "\n</style>";
	if( !empty($custom_css) ) { echo $css_output;}

} //end wpex_custom_css()