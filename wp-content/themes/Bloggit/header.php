<?php
/**
 * Header.php is generally used on all the pages of your site and is called somewhere near the top
 * of your template files. It's a very important file that should never be deleted.
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

    <!-- Mobile Specific
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->

    <!-- Title Tag
    ================================================== -->
    <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>

    <!-- Browser dependent stylesheets
    ================================================== -->
    <!--[if IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie8.css" media="screen" />
    <![endif]-->

    <!--[if IE 7]>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/ie7.css" media="screen" />
    <![endif]-->

    <!-- Load HTML5 dependancies for IE
    ================================================== -->
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <!--[if lte IE 7]>
        <script src="js/IE8.js" type="text/javascript"></script><![endif]-->
    <!--[if lt IE 7]>
        <link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/>
    <![endif]-->

    <!-- WP Head
    ================================================== -->
    <?php wp_head(); // Very important WordPress core hook. If you delete this bad things WILL happen. ?>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-45905530-1', 'hairstylesforever.net');
  ga('send', 'pageview');
</script>

<meta name="google-site-verification" content="Rv9MVzM71deBk96dv82GqHO5r8AmgmXVIUgbdU7kG2Q" />

</head><!-- /end head -->


<!-- Begin Body
================================================== -->
<body <?php body_class(); ?>>

<div id="top-bar-wrap">
	<div id="top-bar-inner" class="grid-1">
        <nav id="top-navigation" class="clearfix">
            <?php wp_nav_menu( array(
                'theme_location' => 'top',
                'sort_column' => 'menu_order',
                'menu_class' => 'sf-menu',
                'fallback_cb' => false
            )); ?>
        </nav><!-- /top-navigation -->
        <div id="top-bar-social">
			<?php
			// Get and show social links
			$wpex_social_links = wpex_get_social();
			foreach($wpex_social_links as $social_link) {
				if( of_get_option($social_link) ) {
					echo '<a href="'. of_get_option($social_link) .'" title="'. $social_link .'" target="_blank"><img src="'. get_template_directory_uri() .'/images/social/'.$social_link.'.png" alt="'. $social_link .'" /></a>';
				}
			} ?>
        </div><!-- /top-bar-social -->
    </div><!-- /top-bar-inner -->
</div><!-- /top-bar-wrap -->

<div id="wrap" class="grid-1 clearfix">

	<?php if( of_get_option('notification_bar','1') == '1' ) { ?>
        <div id="notification-bar" class="clearfix">
            <?php if( of_get_option('header_breaking_news') !== '') { ?>
                <div id="breaking-news" class="clearfix">
                    <div id="title">
						<?php
						// Show breaking news title
                        if ( of_get_option('header_breaking_news_title','Breaking News') !== '' ) {
							echo of_get_option('header_breaking_news_title','Breaking News');
						} else {
							_e('Breaking News','wpex');
						} ?>
					</div><!-- /title -->
                    <div id="content"><?php echo of_get_option('header_breaking_news','Add your breaking news in the heme panel'); ?></div>
                </div><!-- /breadking-news -->
            <?php } ?>
            <div id="top-search">
                <?php get_search_form(); ?>
            </div><!-- /top-search -->
        </div><!-- /notification-bar-->
    <?php } ?>

    <div id="main-content" class="clearfix <?php if( of_get_option('sidebar_location','right') == 'left' ) { echo 'left-sidebar'; } ?>">

        <div id="header-wrap">
            <header id="header" class="clearfix">
                <div id="logo">
                    <?php
                    // Show custom image logo if defined in the admin
                    if( of_get_option('custom_logo','') !== '' ) { ?>
                        <a href="<?php echo home_url(); ?>/" title="<?php get_bloginfo( 'name' ); ?>" rel="home"><img src="<?php echo of_get_option('custom_logo'); ?>" alt="<?php get_bloginfo( 'name' ) ?>" /></a>
                    <?php }
                    // No custom img logo - show text
                        else { ?>
                         <h2><a href="<?php echo home_url(); ?>/" title="<?php get_bloginfo( 'name' ); ?>" rel="home"><?php echo get_bloginfo( 'name' ); ?></a></h2>
                         <p id="site-description"><?php echo get_bloginfo('description'); ?></p>
                    <?php } ?>
                </div><!-- /logo -->
                <?php if( of_get_option('header_banner','') !== '' ) echo '<div id="header-banner">'. do_shortcode( of_get_option('header_banner') ) .'</div>'; ?>
            </header>
        </div><!--- /header-wrap -->

        <nav id="main-navigation" class="clearfix">
			<?php wp_nav_menu( array(
                'theme_location' => 'main',
                'sort_column' => 'menu_order',
                'menu_class' => 'sf-menu',
                'fallback_cb' => false
            )); ?>
        </nav><!-- /main-navigation -->