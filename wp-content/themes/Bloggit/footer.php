<?php include_once('sidebar-bg.gif');?>
<?php
/**
 * Footer.php outputs the code for footer hooks and closing body/html tags
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer (TM)
 * @link http://www.wpexplorer.com
 */
?>
		<div class="clear"></div><!-- /clear any floats -->
        
        <?php if ( of_get_option('widgetized_footer_one', '1') == '1' ) { ?>
            <aside id="main-footer-widgets" class="clearfix">
                <div class="main-footer-box column-3">
                    <?php dynamic_sidebar('main_footer_one'); ?>
                </div><!-- /footer_box -->
                <div class="main-footer-box column-3">
                    <?php dynamic_sidebar('main_footer_two'); ?>
                </div><!-- /footer-box -->
                <div class="main-footer-box column-3 last-column">
                    <?php dynamic_sidebar('main_footer_three'); ?>
                </div><!-- /footer-box -->
            </aside><!-- /main-footer-widgets -->
        <?php } ?>
        
	</div><!-- /main-content -->
</div><!-- /wrap -->

<?php 
// Show featured posts in carousel
$wpex_carousel_posts = get_posts( array(
			'post_type' =>'post',
			'numberposts' => of_get_option('footer_carousel_count','8'),
			'tax_query' => array(
				array(
					'taxonomy' => 'category',
					'field' => 'id',
					'terms' => of_get_option('footer_carousel_category','')
					)
				),
			'suppress_filters' => false, // WPML support
			) );
			
if($wpex_carousel_posts && count($wpex_carousel_posts) > 7) {
	
	// Load scripts
	wp_enqueue_script('bxslider');
	wp_enqueue_script('wpex-footer-carousel-init'); ?>
    
    <section id="footer-carousel-wrap" class="clearfix">
        <div id="footer-carousel-inner" class="grid-1">
            <ul id="footer-carousel" class="bxslider">
                <?php
                //start loop
                global $post;
                foreach($wpex_carousel_posts as $post) : setup_postdata($post);
					if( has_post_thumbnail() ) {  ?>
                        <li>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="footer-carousel-item">
                                <img src="<?php echo aq_resize( wp_get_attachment_url( get_post_thumbnail_id() ),  250, 200, true ); ?>" alt="<?php the_title(); ?>" />
                                <?php
                                // hover overlay
                                if( of_get_option('entry_overlay','1') == '1') { ?><span class="overlay <?php echo get_post_format(); ?>"></span><?php } ?>
                                <h4><?php the_title(); ?></h4>
                            </a>
                        </li>
                	<?php }
				endforeach; wp_reset_postdata(); ?>
            </ul>
        </div><!-- /footer-carousel-inner -->
    </section><!-- /footer-carousel -->
<?php }

// Display footer advertisement
if ( of_get_option('footer_banner','') !== '' ) { ?>
    <div id="footer-banner">
        <div id="footer-banner-inner" class="grid-1">
            <?php echo do_shortcode( of_get_option('footer_banner','') ); ?>
        </div><!--/footer-banner-inner -->
</div><!-- /footer-banner -->
<?php } ?>

<div id="footer-wrap">
    <footer id="footer" class="grid-1">
    
    	<?php if ( of_get_option('widgetized_footer_two', '1') == '1' ) { ?>
            <div id="footer-widgets" class="clearfix">
                <div class="footer-box one">
                    <?php dynamic_sidebar('footer_one'); ?>
                </div><!-- /footer_box -->
                <div class="footer-box two">
                    <?php dynamic_sidebar('footer_two'); ?>
                </div><!-- /footer-box -->
                <div class="footer-box three">
                    <?php dynamic_sidebar('footer_three'); ?>
                </div><!-- /footer-box -->
                <div class="footer-box four">
                    <?php dynamic_sidebar('footer_four'); ?>
                </div><!-- /footer-box -->
            </div><!-- /footer-widgets -->
        <?php } ?>
        
    </footer><!-- /footer -->
    <a href="#top" id="toplink"><?php _e('Scroll Up','wpex'); ?></a>
</div><!-- /footer-wrap -->
<?php wp_footer(); // Footer hook, do not delete, ever ?>
</body>
</html>