<?php
/**
 * Used for the deals category archive
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author AJ Clarke : http://wpexplorer.com
 * @copyright Copyright (c) 2012, AJ Clarke
 * @link http://wpexplorer.com
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

get_header(); // Loads the header.php template
if(have_posts()) : //start loop ?>

<header id="page-heading">
	<h1><?php echo single_term_title(); ?></h1>
</header><!-- /page-heading -->

<div id="deals-archive-wrap">
    <div class="deals-wrap clearfix">
        <?php 
        // Loop through deals entries
		$count=0;
        while (have_posts()) : the_post();
		$count++;
            get_template_part( 'content', 'deals' );
        if ( $count== 3 ) { echo '<div class="clear"></div>'; $count=0; } endwhile; ?>    
    </div><!-- /deals-wrap -->
    <?php wpex_pagination(); // Paginate your posts ?>
</div><!-- /deals-archive-wrap -->

<?php
endif;  // end post check
get_footer(); //get template footer