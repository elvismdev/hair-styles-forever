jQuery(function($){
	$(document).ready(function(){
		
		/*back to top link*/
		$('a#toplink').on('click', function(){
			$('html, body').animate({scrollTop:0}, 'normal');
			return false;
		});
		
		/*fancybox*/
		$(".fancybox").fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
		});
		
		
		/*entry thumbnail hovers*/
		$('.post-entry-thumbnail a').hover(function(e) {
			$(this).children('.overlay').fadeIn('fast');
		}, function(){
			$(this).children('.overlay').fadeOut('fast');
		});
		
		
		/*footer carousel*/
		$('.footer-carousel-item').hover(function(e) {
			$(this).children('.overlay').stop().fadeIn('fast');
		}, function(){
			$(this).children('.overlay').stop().fadeOut('fast');
		});
		
		
		/*comment scroll*/
		$(".comment-scroll a").click(function(event){		
			event.preventDefault();
			$('html,body').animate({ scrollTop:$(this.hash).offset().top}, 'normal' );
		});
		
		
		/*superfish menu*/
		$("ul.sf-menu").superfish({
				delay: 200,
				autoArrows: true,
				dropShadows: false,
				animation: {opacity:'show', height:'show'},
				speed: 'fast'
		});
				
	});
});