jQuery(function($){
	$(window).load(function() {
		$('#featured-flexslider').flexslider({
			animation: 'fade',
			slideshow: true,
			smoothHeight: false,
			controlNav: false,
			prevText: 'Next',
			nextText: 'Prev'
		});
	});
});