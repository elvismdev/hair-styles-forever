jQuery(function($){
	$(document).ready(function(){
		
		/*responsive videos*/
		$(".fitvids").fitVids();

		/* Top Navigation */
		$("<select />").appendTo("#top-navigation");
		$("<option />", {
		   "selected": "selected",
		   "value" : "",
		   "text" : ''
		}).appendTo("#top-navigation select");

		$("#top-navigation a").each(function() {
			var el = $(this);
			if(el.parents('#top-navigation .sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr("href"),
				 'text' : '- ' + el.text()
				}).appendTo("#top-navigation select");
			}
			else if(el.parents('#top-navigation .sub-menu .sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : '-- ' + el.text()
				}).appendTo("#top-navigation select");
			}
			else {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : el.text()
				}).appendTo("#top-navigation select");
			}
		});
		
		
		/* Main Navigation */
		$("<select />").appendTo("#main-navigation");
		$("<option />", {
		   "selected": "selected",
		   "value" : "",
		   "text" : wpexLocalizeNav.main
		}).appendTo("#main-navigation select");
		
		$("#main-navigation a").each(function() {
			var el = $(this);
			if(el.parents('#main-navigation .sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr("href"),
				 'text' : '- ' + el.text()
				}).appendTo("#main-navigation select");
			}
			else if(el.parents('#main-navigation .sub-menu .sub-menu').length >= 1) {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : '-- ' + el.text()
				}).appendTo("#main-navigation select");
			}
			else {
				$('<option />', {
				 'value' : el.attr('href'),
				 'text' : el.text()
				}).appendTo("#main-navigation select");
			}
		});
		
		/*go to select url*/
		$("#top-navigation select, #main-navigation select").change(function() {
		  window.location = $(this).find("option:selected").val();
		});

		/* allow select skinning*/
		$("#top-navigation select, #main-navigation select").uniform();
	
	});
});