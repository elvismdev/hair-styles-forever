jQuery(function($){
	$(document).ready(function(){
		$('#footer-carousel-inner .bxslider').bxSlider({
		  minSlides: 3,
		  maxSlides: 4,
		  slideWidth: 170,
		  slideMargin: 10,
		  pager: false
		});
	});
});