<?php
/**
 * Default file for single regular posts.
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template
if ( have_posts( )) : // Check for posts
while (have_posts()) : the_post(); // Loop through post ?>

<div id="post" class="post-single clearfix">

	<header id="post-heading">
		<h1>
			<?php the_title(); ?>
		</h1>
		<ul class="meta clearfix">
			<li class="date"><span><?php _e('Posted On', 'wpex'); ?></span>: <?php the_time('jS F Y'); ?></li>
			<li class="author"><span><?php _e('By', 'wpex'); ?></span>: <?php the_author_posts_link(); ?></li>
			<?php if(comments_open() && !post_password_required() ) { ?><li class="comment-scroll"><span><?php _e('With', 'wpex'); ?></span> <?php comments_popup_link(__('0 Comments', 'wpex'), __('1 Comment', 'wpex'), __('% Comments', 'wpex'), 'comments-link' ); ?></li><?php } ?>
		</ul><!-- /meta -->
	</header><!-- /page-heading -->

	<!-- NEXT & PREVIOUS custom buttons -->

	<?php
        if (function_exists('drawnavbuttons')) {
            echo drawnavbuttons();
        }
	?>
			<!-- End NEXT & PREVIOUS custom buttons -->

	<?php
		// show media (image/video/gallery) if NOT a private post
	if ( !post_password_required() && of_get_option('single_post_media','1') == '1' && $page < 2 ) {
		get_template_part( 'content', get_post_format() );
	} ?>

	<!-- NEXT & PREVIOUS custom buttons -->

	<?php
        if (function_exists('drawnavbuttons')) {
            echo drawnavbuttons();
        }
	?>
			<!-- End NEXT & PREVIOUS custom buttons -->

			<article class="entry clearfix">
				<?php
			// Display post content

				the_content();

	// Paginate pages when <!--nextpage--> is used
				wp_link_pages( array(
					'before'		=>	'<div id="paginate-post" class="clearfix">',
					'after'			=>	'</div>',
					'link_before'	=>	'<span>',
					'link_after'	=>	'</span>',
					) );

			// Show button on link posts
				if ( get_post_format() == 'link' ) {
					echo '<a href="'. get_post_meta( get_the_ID(), 'wpex_post_url', true) .'" title="'. get_the_title() .'" target="_blank" class="theme-button" id="post-link-button">'. __('visit website','wpex') .'</a>';
				} ?>
			</article><!-- /entry -->

			<?php
		// Post Tags
			if ( of_get_option('single_post_tags','1' ) == '1' ) {
				the_tags( '<div id="post-tags" class="clearfix">', '', '</div>');
			}

        // Edit post link for admin only
			edit_post_link( __('Edit this post','wpex') .' &rarr;', '<p id="post-edit-link">', '</p>');

		// Display author bio unless disabled in the admin
			if( of_get_option('single_post_author','1') == '1' ) { ?>
			<div id="single-author" class="clearfix">
				<div id="author-image">
					<a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_avatar( get_the_author_meta('user_email'), '60', '' ); ?></a>
				</div><!-- author-image -->
				<div id="author-bio">
					<div id="single-author-title"><?php _e('Written by ','wpex') . the_author_meta('display_name'); ?></div>
					<p><?php the_author_meta('description'); ?></p>
					<p id="single-author-link"><?php _e('Read other posts by','wpex'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php the_author_meta('display_name'); ?> &rarr;</a></p>
				</div><!-- author-bio -->
			</div><!-- /single-author -->
			<?php }

		// Related Posts
			if ( of_get_option('single_post_related','1' ) == '1' ) {
				get_template_part( 'content', 'related-posts' );
			}

		// Get comments template located at comments.php
			comments_template(); ?>

		</div><!-- /post -->

		<?php
endwhile; // end posts loop
endif; // end have_posts check
get_sidebar(); // Get template sidebar ?>

<div class="clear"></div>
<div class="post-pagination clearfix">
	<div class="post-prev"><?php next_post_link('%link', '&larr; %title', false); ?></div>
	<div class="post-next"><?php previous_post_link('%link', '%title &rarr;', false); ?></div>
</div><!-- /post-pagination -->

<?php get_footer(); // Get template footer