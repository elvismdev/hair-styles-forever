<?php
/**
 * 404.php is used when your server reaches a 404 error page
 * @package Bloggit WordPress Theme
 * @since 1.0
 * @author WPExplorer : http://www.wpexplorer.com
 * @copyright Copyright (c) 2012, WPExplorer
 * @link http://www.wpexplorer.com
 */

get_header(); // Loads the header.php template ?>

<div id="post">

    <div id="page-heading">
        <h1><?php _e('404: Page Not Found','wpex'); ?></h1>
    </div><!-- /page-heading -->
    
    <div id="error-page">			
        <p id="error-page-text"><?php _e('Unfortunately, the page you tried accessing could not be retrieved as it may no longer exist or never existed. Why not have a look at our recent posts below?','wpex'); ?></p>
    </div><!-- /error-page -->

</div><!-- /post -->

<?php 
get_sidebar(); //get template sidebar
get_footer(); //get template footer